
import 'dart:io';

import 'package:path_provider/path_provider.dart';

/////////////////////////                    store data localy for Name of country

 Future<String> get localPathVersion async {
    final directory = await getApplicationDocumentsDirectory();
    // For your reference print the AppDoc directory
    return directory.path;
  }

  Future<File> get localFileVersion async {
    final path = await localPathVersion;
    return File('$path/Country.txt');
  }

  Future<File> writeVersion(String str) async {
    final file = await localFileVersion;
    // Write the file
    return file.writeAsString(str);
  }

  
Future<String> readVersion() async {
    try {
      final file = await localFileVersion;
      // Read the file
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If there is an error reading, return a default String
      return 'Error';
    }
  }