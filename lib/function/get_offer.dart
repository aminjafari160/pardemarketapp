/////////////////////////////      String To double         //////////////////////////////////

double String_to_int(String str) {
  if (str == "بدون قیمت") {
    return double.parse(
      "0",
    );
  } else {
    return double.parse(str.replaceAll("ریال", "").replaceAll("بدون قیمت", ""));
  }
}

//////////////////////////////        Get Offer        ///////////////////////////////////////////////

int offer(double num1, double num2) {
  return (((num1 - num2) / num1) * 100).round();
}
