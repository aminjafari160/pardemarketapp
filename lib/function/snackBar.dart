import 'package:flutter/material.dart';

loading_SnackBar() {
  return SnackBar(
    behavior: SnackBarBehavior.floating,
    duration: Duration(seconds: 90),
    content: Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            backgroundColor: Colors.red,
          ),
          Text(
            "لطفا صبور باشید",
            style: TextStyle(
                color: Color(0xffffffff),
                fontFamily: "iranYekan",
                fontSize: 16),
          ),
        ],
      ),
    ),
    backgroundColor: Color(0xffc80058),
  );
}


loadingSnackBarUploadImage() {
  return SnackBar(
    behavior: SnackBarBehavior.floating,
    duration: Duration(seconds: 90),
    content: Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            backgroundColor: Colors.red,
          ),
          Text(
            "لطفا صبور باشید، درحال آپلود عکس",
            style: TextStyle(
                color: Color(0xffffffff),
                fontFamily: "iranYekan",
                fontSize: 14),
          ),
        ],
      ),
    ),
    backgroundColor: Color(0xffc80058),
  );
}

showErrorDialog(String str, GlobalKey<ScaffoldState> _scaffoldKey) {
  _scaffoldKey.currentState.showSnackBar(
    new SnackBar(
      content: Text(
        str,
        style: TextStyle(
          fontFamily: "IransansLight",
          fontSize: 16,
          color: Color(0xffffffff),
        ),
        textDirection: TextDirection.rtl,
      ),
      backgroundColor: Color(0xffc80058),
    ),
  );
}

dialog(String str_content, String str_button, BuildContext context,
    Widget widget) {
  return showDialog(
    context: context,
    
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(32.0))),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(
                str_content,
                textDirection: TextDirection.rtl,
                style: TextStyle(fontFamily: "IranSansMobile"),
              ),
            ),
            widget
          ],
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text(
              str_button,
              style: TextStyle(
                color: Color(0xffc80058),
                fontFamily: "IranSansMobile",
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}
