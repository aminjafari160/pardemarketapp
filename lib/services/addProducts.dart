import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';

String replaceEnglishNumber(String input) {
  const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

  for (int i = 0; i < english.length; i++) {
    input = input.replaceAll(farsi[i], english[i]);
  }

  return input;
}

/////////////////////////////    ADD PRODUCT IN WOOCOMMERCE   ///////////////////////////////////////
Future<List> addProduct(var body,
    String token) async {

  String url = 'https://pardemarket.com/wp-json/dokan/v1/products';

  
  http.Response response = await http.post((url),
      
     headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
        'Content-type': 'application/json',
      },
      body: json.encode(body));

}

Future<List> editProduct(var body,
    String token,[String id]) async {

  String url = 'https://pardemarket.com/wp-json/dokan/v1/products/$id';

  
  http.Response response = await http.post((url),
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer $token",
        'Content-type': 'application/json',
      },
      body: json.encode(body));

}

Future<List> editProductOrder(var body,
    String id) async {

  String url = 'https://pardemarket.com/wp-json/wc/v3/products/$id?consumer_key=ck_79788848af39dd8c025955af50f3284b1e7a8b2f&consumer_secret=cs_573910910f89ecd1b24cf406364e0be49baf49cd';

  
  http.Response response = await http.post((url),
      
      headers: {
        "Content-Type": "application/json",
      },
      body: json.encode(body));

}
