import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';


List productsMyStoreList;

Future productsMyStore(String id) async {
  http.Response response = await http.get(
    ("http://http://pardemarket.com/wp-json/dokan/v1/stores/$id/products"),
    headers: {
      "Accept": "application/json",
    },

  );
  return json.decode(response.body);

}
