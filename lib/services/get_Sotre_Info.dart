import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import './get_Token.dart';

Map store_Info = new Map();


Future<String> get_Store_Info(String id) async {

  String url1 = 'https://pardemarket.com/wp-json/dokan/v1/stores/$id';
  http.Response response1 = await http.get(
    (url1),
    headers: {
      "Accept": "application/json",
      'Content-type': 'application/json',
      "Authorization": "Bearer $token"
    },
  );
  store_Info = json.decode(response1.body);
}


String storName(Map storInfo){

  return storInfo["store_name"];
}

String storFirstName(Map storInfo){

  return storInfo["first_name"];
}

String storId(Map storInfo){

  return storInfo["Id"];
}

String storImage(Map storInfo){

  return storInfo["gravatar"];
}

String storPhone(Map storInfo){

  return storInfo["phone"];
}

String storStreet(Map storInfo){

  return storInfo["address"]["street_1"];
}

String storCity(Map storInfo){

  return storInfo["address"]["city"];
}

String storeAcName(Map storInfo){

  return storInfo["payment"]["bank"]["ac_name"];
}

String storePayment(Map storInfo){

  return storInfo["payment"].toString();
}

String storAcNumber(Map storInfo){

  return storInfo["payment"]["bank"]["ac_number"].toString();
}
