import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import './get_Token.dart';

String replaceEnglishNumber(String input) {
  const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

  for (int i = 0; i < english.length; i++) {
    input = input.replaceAll(farsi[i], english[i]);
  }

  return input;
}

/////////////////////////////    payment_send   ///////////////////////////////////////

Map payment_send_response ={};

String redirect = "https://pardemarket.com/success-payment/";
String apiKye = "ae3c3e44-5ccd-4d59-83d1-63dbbcfd7e61";

Future payment_send(int amount) async {
  String url = "https://api.idpay.ir/v1.1/payment";

  http.Response response = await http.post(
    url,
    headers: {
      'X-SANDBOX': '0',
      "Accept": "application/json",
      'Content-type': 'application/json',
      'X-API-KEY': apiKye,
    },
    body: json.encode(
      {
        "order_id": 2022,
        "amount": amount,
        "callback": "https://pardemarket.com/success-payment/"
      },
    ),
  );
  payment_send_response = json.decode(response.body);
  return json.decode(response.body);
}

/////////////////////////////    move to payment page   ///////////////////////////////////////

Map verify_transaction_response ={};
Future payment_verify(String id) async {
  String url = 'https://api.idpay.ir/v1.1/payment/verify';

  http.Response response = await http.post(
    (url),
    headers: {
      'X-SANDBOX': '0',
      "Accept": "application/json",
      'Content-type': 'application/json',
      'X-API-KEY': apiKye,
    },
    body: json.encode(
      {
        "order_id": 2022,
        "id": id.toString(),
      },
    ),
  );
  print(response.body);
  verify_transaction_response = json.decode(response.body);
  return json.decode(response.body);
}

/////////////////////////////    Verifaya of transection   ///////////////////////////////////////

// Future varify_transaction(String token) async {
//   String api = "cc3c49731e9c949990301db9c3a16c83";

//   String url = 'https://pay.ir/pg/verify?api=$api&token=$token';

//   http.Response response = await http.post(
//     (url),
//     headers: {
//       "Accept": "application/json",
//       'Content-type': 'application/json',
//     },
//   );

//   verify_transaction_response = json.decode(response.body);
//   return json.decode(response.body);
// }
