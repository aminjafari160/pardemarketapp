import 'package:http/http.dart' as http;
import 'dart:async';


String token ;
String strProductFeatured ;
String strProductBestSeller ;
Future<String> get_token()async{

  http.Response response = await http.post(("https://pardemarket.com/wp-content/uploads/2019/09/token.txt"),
      headers: {
        "Accept": "application/json",
      });
      token = response.body;
      print("\n\n $token\n\n");
}
Future<String> getUrl(String url,String object)async{

  http.Response response = await http.post((url),
      headers: {
        "Accept": "application/json",
      });
      strProductFeatured = response.body;
      return strProductFeatured;
}
Future<String> getUrlBestseller(String url,String object)async{

  http.Response response = await http.post((url),
      headers: {
        "Accept": "application/json",
      });
      strProductBestSeller = response.body;
      return strProductBestSeller;
}
