import 'dart:convert';
import './get_Token.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

Future<List> becomeAVendor(
    int id,
    int idOfImage,
    String storeName,
    String phoneOfStore,
    String firstName,
    String city,
    String address,
    String acName,
    String acNumber) async {
  int idOfStore;

  String url = 'https://pardemarket.com/wp-json/wp/v2/users/$id';

  String url1 = 'https://pardemarket.com/wp-json/dokan/v1/stores/$id';

  http.Response response = await http
      .post(
    (url),
    headers: {
      "Accept": "application/json",
      "Authorization": "Bearer $token",
      'Content-type': 'application/json',
    },
    body: json.encode(
      {
        "roles": ["seller"],
        "first_name": firstName,
      },
    ),
  )
      .whenComplete(() async {
    Map forId;
    http.Response response1 = await http.post((url1),
        headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token",
          'Content-type': 'application/json',
        },
        body: json.encode({
          "store_name": storeName,
          "gravatar_id": "$idOfImage",
          "phone": phoneOfStore,
          "address": {
            "street_1": address,
            "city": city,
          },
          "payment": {
            "bank": {"ac_name": "$acName", "ac_number": "$acNumber"}
          }
        }));
    forId = json.decode(response1.body);
    idOfStore = forId["id"];
  });
}

_launchURL() async {
  const url = 'https://flutter.io';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
