import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

List urlsBrand;
Future<List> getImageBrand() async {
  http.Response response = await http.post(
      ("https://pardemarket.com/wp-content/uploads/brands.txt"),
      headers: {
        "Accept": "application/json",
      });

  urlsBrand = response.body.split(" ");
  print("\n\n ${urlsBrand.length}\n\n");
  return urlsBrand;
}
