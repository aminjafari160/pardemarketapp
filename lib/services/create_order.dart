// import 'dart:_http';
import 'dart:convert';
import 'dart:async';

import 'dart:io';

String replaceEnglishNumber(String input) {
  const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

  for (int i = 0; i < english.length; i++) {
    input = input.replaceAll(farsi[i], english[i]);
  }

  // FIXME: amin jafari

  return input;
}

/////////////////////////////    Creat_Order   ///////////////////////////////////////

String url =
    'https://pardemarket.com/wp-json/wc/v3/orders?consumer_key=ck_79788848af39dd8c025955af50f3284b1e7a8b2f&consumer_secret=cs_573910910f89ecd1b24cf406364e0be49baf49cd';
Future create_order_pendding(
  String first_name,
  String last_name,
  String address,
  String phone_number,
  String customer_id,
  List list_of_product,
) async {

  Map body = {
    // "payment_method": "payir",
    "payment_method_title": "پرداخت آنلاین",
    // "set_paid": true,
    "customer_id": "$customer_id",
    "billing": {
      "first_name": "$first_name",
      "last_name": "$last_name",
      "address_1": "$address",
      "phone": "00989133265739"
    },
    "line_items": list_of_product
  };

  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('content-type', 'application/json');
  request.add(utf8.encode(json.encode(body)));
  HttpClientResponse response = await request.close();
  // todo - you should check the response.statusCode
  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  
 Map<String,dynamic> amin= json.decode(reply);

 idOfOrder = amin["id"];

  return reply;
}
int idOfOrder;
Future create_order(
  String first_name,
  String last_name,
  String address,
  String phone_number,
  String customer_id,
  List list_of_product,
  String id

) async {

String url =
    'https://pardemarket.com/wp-json/wc/v3/orders/$id?consumer_key=ck_79788848af39dd8c025955af50f3284b1e7a8b2f&consumer_secret=cs_573910910f89ecd1b24cf406364e0be49baf49cd';

  Map body = {
    "payment_method": "payir",
    "payment_method_title": "پرداخت آنلاین",
    "status":"processing",
    "set_paid": true,
    "customer_id": "$customer_id",
    "billing": {
      "first_name": "$first_name",
      "last_name": "$last_name",
      "address_1": "$address",
      "phone": "$phone_number"
    },
    "line_items": list_of_product
  };

  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('content-type', 'application/json');
  request.add(utf8.encode(json.encode(body)));
  HttpClientResponse response = await request.close();
  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  return reply;

}
