import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import './get_Token.dart';

Future<List> createATicket(
    String title, String content, String phonenumber, int id) async {
  String url = 'https://pardemarket.com/wp-json/wpas-api/v1/tickets';

  http.Response response = await http.post((url), headers: {
    "Accept": "application/json",
    "Authorization": "Bearer $token",
  }, body: {
    "title": title,
    "content": "$phonenumber \n $content",
    "author": "$id"
  });
}
