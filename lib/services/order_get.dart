String dateCreatedOrderUser(List nameOfProducts, int index) {
  return nameOfProducts[index]['date_created'].toString().substring(0, 10);
}

String timeCreatedOrderUser(List nameOfProducts, int index) {
  return nameOfProducts[index]['date_created'].toString().substring(11, 16);
}

/////////////////////////////////////////     List of single product in order ///////////////////////////////////////////////////



List listOfProductInOrder(List nameOfProducts, int index) {
  return nameOfProducts[index]['line_items'];
}

String nameOfSingleProductInList(List nameOfProducts, int index) {
  return nameOfProducts[index]['name'];
}

String statusOfOrder(List nameOfProducts, int index) {
  return nameOfProducts[index]['status'];
}

String quntityOfSingleProductInList(List nameOfProducts, int index) {
  return (nameOfProducts[index]['quantity'] ).toStringAsFixed(2);
}
 quntityOfSingleProductInList_int(List nameOfProducts, int index) {
  return (nameOfProducts[index]['quantity'] );
}

 priceOfSingleProductInList(List nameOfProducts, int index) {
  return (nameOfProducts[index]['price'] ).toString()   ;
}
 priceOfSingleProductInList_int(List nameOfProducts, int index) {
  return (nameOfProducts[index]['price'] )   ;
}
String totalpriceOfSingleProductInList(List nameOfProducts, int index) {
  return nameOfProducts[index]['total'].toString() ;
}

String orderKeyOfSingleProductInList(List nameOfProducts, int index) {
  return nameOfProducts[index]['id'].toString();
}
