import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';

Future ChangeShipping(String nameOfStore, String phoneOfStore, String sheba,
    String city, String address, int id) async {
  http.Response response = await http.post(
      ("https://pardemarket.com/wp-json/wc/v3/customers/$id?consumer_key=ck_79788848af39dd8c025955af50f3284b1e7a8b2f&consumer_secret=cs_573910910f89ecd1b24cf406364e0be49baf49cd"),
      headers: {
        "Accept": "application/json"
      },
      body: {
        "shipping": {
          "company": nameOfStore,
          "address_1": address,
          "city": city,
          "postcode": phoneOfStore,
          "country": sheba,
        }
      });

  return json.encode(response.body);
}
