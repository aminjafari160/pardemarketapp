// import 'dart:convert';
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'dart:async';

// List Products = new List();

// Future<List> getProducts(List NameOfProducts, String url) async {
//   "get products");
//   http.Response response = await http.get(
//     (url),
//     headers: {
//       "Accept": "application/json",
//     },
//   );

//   NameOfProducts = json.decode(response.body);

//   "lenght of product (getProduct)       "+NameOfProducts.length.toString());
//   // "${json.decode(response.body)}");
//   return NameOfProducts = json.decode(response.body);
// }

import 'package:pardemarketapp/function/monyString.dart';

String getImage(List NameOfProducts, int index) {
  return NameOfProducts[index]['images'].toString().contains('src')
      ? NameOfProducts[index]['images'][0]['src']
      : "http://pardemarket.com/wp-content/uploads/2019/06/curtain.png";
}

String getPrice(List NameOfProducts, int index) {
  return NameOfProducts[index]['price'] == ''
      ? "بدون قیمت"
      : (double.parse((NameOfProducts[index]['price'])) )
              .round()
              .toString() +
          '  ریال';
}

String getShowPrice(List NameOfProducts, int index) {
  return NameOfProducts[index]['price'] == ''
      ? "بدون قیمت"
      : toMony((double.parse((NameOfProducts[index]['price'])) )
              .round()
              .toString()) +
          '  ریال';
}

String get_date_created(List NameOfProducts, int index) {
  return NameOfProducts[index]['date_created'];
}

String getId(List NameOfProducts, int index) {
  return NameOfProducts[index]['id'].toString();
}

String getRegularPrice(List NameOfProducts, int index) {
  return NameOfProducts[index]['regular_price'] == ''
      ? "بدون قیمت"
      : (double.parse((NameOfProducts[index]['regular_price'])) )
              .round()
              .toString() +
          '  ریال';
}

String showRegularPrice(List NameOfProducts, int index) {
  return NameOfProducts[index]['regular_price'] == ''
      ? "بدون قیمت"
      : toMony((double.parse((NameOfProducts[index]['regular_price'])) )
              .round()
              .toString()) +
          '  ریال';
}

int showRegularPrice_int(List NameOfProducts, int index) {
  return NameOfProducts[index]['regular_price'] == ''
      ? 0
      : int.parse(
         (double.parse((NameOfProducts[index]['regular_price'])) )
              .round()
              .toString(),
        );
}

String getdesdescription(List NameOfProducts, int index) {
  return NameOfProducts[index]['description'] == ''
      ? "توضیحاتی داده نشده است"
      : NameOfProducts[index]['description'];
}

String getCategorie(List NameOfProducts, int index) {
  return NameOfProducts[index]['categories'][0]["name"] == ''
      ? "بدون دسته بندی"
      : NameOfProducts[index]['categories'][0]["name"];
}

int getIdCategorie(List NameOfProducts, int index) {
  return NameOfProducts[index]['categories'][1]["id"] == ''
      ? "بدون دسته بندی"
      : NameOfProducts[index]['categories'][1]["id"];
}

int getIdSubCategorie(List NameOfProducts, int index) {
  return NameOfProducts[index]['categories'][0]["id"] == ''
      ? "بدون دسته بندی"
      : NameOfProducts[index]['categories'][0]["id"];
}

// String getAtribute(List NameOfProducts, int index) {
//   return !NameOfProducts[index]['attributes'].toString().contains('options')
//       ? "تعریف نشده است"
//       : NameOfProducts[index]['attributes'][0]["options"][0];
// }

String getName(List NameOfProducts, int index) {
  return NameOfProducts[index]['name'];
}

String idStore(List NameOfProducts, int index) {
  return NameOfProducts[index]['store']['id'].toString();
}

String storePhone(List NameOfProducts, int index) {
  return NameOfProducts[index]['store']['name'].toString();
}

String getStatus(List NameOfProducts, int index) {
  return NameOfProducts[index]['status'] == "pending"
      ? "در حال بررسی"
      : NameOfProducts[index]['status'] == "publish"
          ? "منتشر شد"
          : "محصول شما تایید نشد";
}

List getAttribute(List NameOfProducts, int index) {
  return NameOfProducts[index]['attributes'];
}

String getNoeErae(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 3) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}

String getTedadAdl(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 10) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}

String getSizeAdl(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 9) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}

String getTedadTedadi(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 15) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}

String getTedadTaghe(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 12) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}

String getSizeTaghe(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 6) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}

String getTedadMetraj(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 8) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}

String getSizeMetraj(List NameOfProducts, int index) {
  for (int i = 0; i <= getAttribute(NameOfProducts, index).length; i++) {
    if (NameOfProducts[index]["attributes"][i]["id"] == 13) {
      return NameOfProducts[index]["attributes"][i]["options"]
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "");
    }
  }
}
