import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:pardemarketapp/pages/IntroductionPage.dart';
import 'pages/department_store/BottomNavBar.dart';
import 'package:shared_preferences/shared_preferences.dart';

String url = 'https://www.pardemarket.com/wp-json/wp/v2/users/register';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MaterialApp(
    title: 'PardeMarket',
    theme: ThemeData(
      primaryColor: new Color(0xffc80058),
      accentColor: Colors.orange,
    ),
    debugShowCheckedModeBanner: false,
    home: HomePage(),
  ),);
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seen') ?? false);

    if (_seen) {
      Navigator.of(context).pushReplacement(
        new MaterialPageRoute(
          builder: (context) => new BottomNavyBarMain(),
        ),
      );
    } else {
      prefs.setBool('seen', true);
      Navigator.of(context).pushReplacement(
        new MaterialPageRoute(
          builder: (context) => new IntroductionPage(),
        ),
      );
    }
  }

  @override
  void initState() {
    checkFirstSeen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
