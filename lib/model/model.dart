class MyList {
  int _id;
  String _nameOfUser;
  String _phoneOfUser;
  String _boolBecomeAVendor;
  String _boolCustomer;
  String _userId;

  MyList(this._nameOfUser, this._boolBecomeAVendor, this._boolCustomer,
      this._phoneOfUser, this._userId);


  int get id => _id;
  String get nameOfUser => _nameOfUser;
  String get phoneOfUser => _phoneOfUser;
  String get boolBecomeAVendor => _boolBecomeAVendor;
  String get boolCustomer => _boolCustomer;
  String get userId => _userId;
  

  set nameOfUser(String value) {
    if (value.length > 2) {
      _nameOfUser = value;
    }
  }

  set phoneOfUser(String value) {
    if (value.length > 0) {
      _phoneOfUser = value;
    }
  }

  set boolBecomeAVendor(String value) {
    if (value.length > 2) {
      _boolBecomeAVendor = value;
    }
  }

  set boolCustomer(String value) {
    if (value.length > 2) {
      _boolCustomer = value;
    }
  }

  set userId(String value) {
    if (value.length != null) {
      _userId = value;
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["nameOfUser"] = _nameOfUser;
    map["phoneOfUser"] = _phoneOfUser;
    map["boolBecomeAVendor"] = _boolBecomeAVendor;
    map["boolcustomer"] = _boolCustomer;
    map["userId"] = _userId;
    if (_id != null) {
      map["id"] = _id;
    }
    return map;
  }

  MyList.fromObject(dynamic o) {
    this._id = o["Id"];
    this._nameOfUser = o["NameOfUser"];
    this._phoneOfUser = o["PhoneOfUser"];
    this._boolBecomeAVendor = o["BoolBecomeAVendor"];
    this._boolCustomer = o["BoolCustomer"];
    this._userId = o["UserId"];
  }
}
