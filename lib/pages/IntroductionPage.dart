import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/model/model.dart';

import 'department_store/BottomNavBar.dart';

Color RedColorFont = Color(0xffc80058);
Color darkColorFont = Color(0xff212121);
Color LightColorFont = Color(0xff424242);

String assetWeb = 'assets/imgs/Asset 1.png';

String assetPrice = 'assets/imgs/Asset 2.jpg';

String assetCard = 'assets/imgs/Asset 3.png';

class IntroductionPage extends StatefulWidget {
  @override
  _IntroductionPageState createState() => _IntroductionPageState();
}

class _IntroductionPageState extends State<IntroductionPage> {
  List<Slide> slides = new List();
  int count = 0;
  DbHelper dbHelper = new DbHelper();

  List<MyList> myList;

  MyList mylist;
  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "کالاها را ببینید",
        styleTitle: TextStyle(
            color: Color(0xff212121),
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'iransansD'),
        description: "در دسته بندی های مختلف بگردید" +
            "\n" +
            " و محصولات موردنظر خود را انتخاب کنید",
        styleDescription: TextStyle(
            color: Color(0xff424242), fontSize: 16.0, fontFamily: 'iranYekan'),
        pathImage: assetPrice,
        backgroundColor: Color(0xFFf5f5f5),
      ),
    );
    slides.add(
      new Slide(
        title: "آنلاین بخرید",
        styleTitle: TextStyle(
            color: Color(0xff212121),
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'iransansD'),
        description: "آنلاین بخرید و آنلاین پرداخت کنید و در محل تحویل بگیرید",
        styleDescription: TextStyle(
            color: Color(0xff424242), fontSize: 16.0, fontFamily: 'iranYekan'),
        pathImage: assetWeb,
        backgroundColor: Color(0xFFf5f5f5),
      ),
    );
    slides.add(
      new Slide(
        title: "!تخفیف بگیرید",
        styleTitle: TextStyle(
            color: Color(0xff212121),
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'iransansD'),
        description: "از تخفیف های شگفت انگیز اپلیکیشن ما بهره مند شوید",
        styleDescription: TextStyle(
            color: Color(0xff424242), fontSize: 16.0, fontFamily: 'iranYekan'),
        pathImage: assetCard,
        backgroundColor: Color(0xFFf5f5f5),
      ),
    );
  }

  void onDonePress() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => BottomNavyBarMain()),
    );
    dbHelper.insert(MyList("false", "false", "false", "false", "false"));
  }

  void onSkipPress() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => BottomNavyBarMain()),
    );
    dbHelper.insert(MyList("false", "false", "false", "false", "false"));
  }

  Widget renderNextBtn() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          'بعدی',
          style: TextStyle(
              fontSize: 14.0,
              fontFamily: 'iranYekan',
              fontWeight: FontWeight.bold,
              color: RedColorFont),
        ),
        Icon(
          Icons.navigate_next,
          color: RedColorFont,
          size: 20.0,
        ),
      ],
    );
  }

  Widget renderDoneBtn() {
    return Text(
      "!شروع",
      style: TextStyle(
          fontSize: 16.0,
          fontFamily: 'iranYekan',
          fontWeight: FontWeight.bold,
          color: RedColorFont),
    );
  }

  Widget renderSkipBtn() {
    return Text(
      "رد کردن",
      style: TextStyle(
          color: Color(0xff424242),
          fontWeight: FontWeight.bold,
          fontFamily: 'iranYekan',
          fontSize: 16.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }
    return new IntroSlider(
      // List slides
      slides: this.slides,

      // Skip button
      renderSkipBtn: this.renderSkipBtn(),
      onSkipPress: this.onSkipPress,
      colorSkipBtn: Color(0xFFf0f0f0),
      highlightColorSkipBtn: Color(0xff000000),

      // Next, Done button
      onDonePress: this.onDonePress,
      renderNextBtn: this.renderNextBtn(),
      renderDoneBtn: this.renderDoneBtn(),
      colorDoneBtn: Color(0xFFf0f0f0),
      highlightColorDoneBtn: Color(0xff000000),

      // Dot indicator
      colorDot: Color(0xffdddddd),
      colorActiveDot: Color(0xffc80058),
      sizeDot: 8.0,
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
        });
      });
    });
  }
}
