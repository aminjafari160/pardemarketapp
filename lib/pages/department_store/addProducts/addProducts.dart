import 'dart:io';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/CategoryPage/selectForProducts.dart';
import 'package:pardemarketapp/pages/department_store/MyStore/myStore.dart';
import 'package:pardemarketapp/services/getProducts.dart' as prefix0;
import 'package:pardemarketapp/services/getProducts.dart';
import 'package:pardemarketapp/services/get_Token_forUser.dart';
import 'package:path/path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pardemarketapp/services/addProducts.dart';
import '../../../services/get_Token.dart';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'package:async/async.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class addProducts extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _addProductsState createState() => _addProductsState();
}

class _addProductsState extends State<addProducts> {
  @override
  void dispose() {
    // TODO: implement dispose
    indexOfProductOfMyStore = null;
    subCategorie = null;
    idSubCategorie = null;
    idCategorie = null;

    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController _nameController = new TextEditingController();

  TextEditingController _tedadTagheController = new TextEditingController();

  TextEditingController _sizeTagheController = new TextEditingController();
  TextEditingController _tedadAdlController = new TextEditingController();
  TextEditingController _sizeAdlController = new TextEditingController();
  TextEditingController _metrajController = new TextEditingController();
  TextEditingController _tedadController = new TextEditingController();
  // TextEditingController _sizeMetrajController = new TextEditingController();
  // TextEditingController _tedadMetrajController = new TextEditingController();

  var _regularPriceController = new MaskedTextController(mask: '00000000000');
  TextEditingController _salePriceController = new TextEditingController();
  TextEditingController _decriptionController = new TextEditingController();
  DbHelper dbHelper = new DbHelper();

  List<MyList> myList;

  MyList mylist;

  int count = 0;
  bool taghe = false;
  bool tedadi = false;
  bool adl = false;
  bool metraj = false;

  @override
  void initState() {
    if (indexOfProductOfMyStore != null) {
      idCategorie = getIdCategorie(ProductOfMyStore, indexOfProductOfMyStore);
      idSubCategorie =
          getIdSubCategorie(ProductOfMyStore, indexOfProductOfMyStore);

      subCategorie = getCategorie(ProductOfMyStore, indexOfProductOfMyStore);
      _nameController.text = getName(ProductOfMyStore, indexOfProductOfMyStore);
      int _price = int.parse(getPrice(ProductOfMyStore, indexOfProductOfMyStore)
          .replaceAll(new RegExp(r'ریال'), ''));
      int _regularPrice = int.parse(
          getRegularPrice(ProductOfMyStore, indexOfProductOfMyStore)
              .replaceAll(new RegExp(r'ریال'), ''));

      _salePriceController.text =
          (100 - (_price * 100 / _regularPrice)).round().toString();

      _decriptionController.text =
          getdesdescription(ProductOfMyStore, indexOfProductOfMyStore)
              .replaceAll(new RegExp(r'<p>'), '')
              .replaceAll(new RegExp(r'</p>'), '')
              .replaceAll(new RegExp(r'<br />'), '');

      _regularPriceController.text =
          getRegularPrice(ProductOfMyStore, indexOfProductOfMyStore)
              .replaceAll(new RegExp(r'ریال'), '');

      if (getNoeErae(ProductOfMyStore, indexOfProductOfMyStore)
          .contains("عدل")) {
        typeOfSell.add("عدل");
        adl = true;
        adlString = getSizeAdl(ProductOfMyStore, indexOfProductOfMyStore);
        adlStringQuantity =
            getTedadAdl(ProductOfMyStore, indexOfProductOfMyStore);
        _sizeAdlController.text = adlString;
        _tedadAdlController.text = adlStringQuantity;
      }
      if (getNoeErae(ProductOfMyStore, indexOfProductOfMyStore)
          .contains("طاقه")) {
        typeOfSell.add("طاقه");
        taghe = true;
        tagheString = getSizeTaghe(ProductOfMyStore, indexOfProductOfMyStore);
        tagheStringQuantity =
            getTedadTaghe(ProductOfMyStore, indexOfProductOfMyStore);
        _sizeTagheController.text = tagheString;
        _tedadTagheController.text = tagheStringQuantity;
      }
      if (getNoeErae(ProductOfMyStore, indexOfProductOfMyStore)
          .contains("تعدادی")) {
        typeOfSell.add("تعدادی");
        tedadi = true;
        tedadString = getTedadTedadi(ProductOfMyStore, indexOfProductOfMyStore);
        _tedadController.text = tedadString;
      }
      if (getNoeErae(ProductOfMyStore, indexOfProductOfMyStore)
          .contains("متراژی")) {
        print(
            "\n\n${prefix0.getTedadMetraj(ProductOfMyStore, indexOfProductOfMyStore)}\n\n\n");
        typeOfSell.add("متراژی");
        metraj = true;
        metrajString =
            getTedadMetraj(ProductOfMyStore, indexOfProductOfMyStore);
        _metrajController.text = metrajString;
        // sizeMetrajString =
        //     getSizeMetraj(ProductOfMyStore, indexOfProductOfMyStore);
        // _sizeMetrajController.text = sizeMetrajString;
        // metrajStringQuantity = getTedadTaghe(ProductOfMyStore, indexOfProductOfMyStore);
      }
    }

    super.initState();
    getData();
  }

  File _image;
  String tagheString,
      metrajString,
      tedadString,
      sizeMetrajString,
      adlString,
      tagheStringQuantity,
      adlStringQuantity,
      name,
      regularPrice,
      salePrice = "0",
      description,
      srcOfImage;
  List<String> typeOfSell = new List();
  int _countForFirstTimeClick = 0;
  // Future<File> compressImage(File file, String targetPath) async {
  //   var result = await FlutterImageCompress.compressAndGetFile(
  //     file.absolute.path,
  //     targetPath,

  //     quality: 88,
  //     rotate: 180,
  //   );

  //   print(file.lengthSync());
  //   print(result.lengthSync());

  //   return result;
  // }

  Future Upload(File imageFile) async {
    print("upload Image");
    var stream =
        new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    var length = await imageFile.length();
    String apiUrl = "https://pardemarket.com/wp-json/wp/v2/media";

    var uri = Uri.parse(apiUrl);
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request = new http.MultipartRequest("POST", uri);
    request.headers.addAll(headers);
    var multipartFile = new http.MultipartFile('file', stream, length,
        contentType: new MediaType('application', 'x-tar'),
        filename: basename(imageFile.path));
    //contentType: new MediaType('image', 'png'));

    request.files.add(multipartFile);
    var response = await request.send();
    String respStr = await response.stream.bytesToString();
    Map info_Of_Image = json.decode(respStr);
    print("code" + "${response.contentLength}");
    print("body" + "$respStr");
    print("fields" + "${request.method}");
    print("${info_Of_Image["guid"]["rendered"]}");
    srcOfImage = info_Of_Image["guid"]["rendered"];
    return info_Of_Image;
    // response.stream.transform(utf8.decoder).listen((value) {
    //   print(json.decode(value));
    // }
    // );
  }

  Future getImageFromGalery() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 1800, maxWidth: 1200);

    print("================!!!================${image.path}");
    // compressImage(image, image.path);
    setState(() {
      _image = image;
      isImageLoaded = true;
    });

    if (image != null) {
      _scaffoldKey.currentState.showSnackBar(loadingSnackBarUploadImage());
    }

    Upload(_image).then((value) {
      print("\n\n         value of upload     $value     \n\n\n");
      if (value != null) {
        _scaffoldKey.currentState.hideCurrentSnackBar();
        showErrorDialog("تصویر آپلود شد", _scaffoldKey);
      }
    });
  }

  bool isImageLoaded = false;

  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }
    double withOfScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xffd5d5d5),
      body: Container(
        margin: EdgeInsets.fromLTRB(20.0, 45.0, 20.0, 25.0),
        decoration: BoxDecoration(
          color: Color(0xfff5f5f5),
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
          boxShadow: [
            new BoxShadow(
              color: Color(0xffA8A8A8),
              blurRadius: 21.0,
              offset: Offset(0, 3),
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width - 30,
        height: MediaQuery.of(context).size.height - 60,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  getImageFromGalery();
                },
                child: Container(
                  padding: isImageLoaded && _image != null ||
                          indexOfProductOfMyStore != null
                      ? EdgeInsets.all(0.0)
                      : EdgeInsets.all(50.0),
                  width: MediaQuery.of(context).size.width - 30,
                  height: 180,
                  decoration: BoxDecoration(
                    color: Color(0xffe0e0e0),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0)),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    ),
                    child: indexOfProductOfMyStore != null
                        ? Image.network(
                            getImage(ProductOfMyStore, indexOfProductOfMyStore),
                            fit: BoxFit.fill,
                          )
                        : isImageLoaded && _image != null
                            ? Image.file(
                                _image,
                                fit: BoxFit.cover,
                              )
                            : SvgPicture.asset(
                                "assets/imgs/add_image.svg",
                              ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                child: Material(
                  borderRadius: BorderRadius.all(Radius.circular(35.0)),
                  elevation: 2.0,
                  child: TextField(
                    controller: _nameController,
                    onChanged: (value) {
                      name = value;
                    },
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                    ),
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                      border: InputBorder.none,
                      hintText: "عنوان محصول  (اجباری)",
                      hintStyle: TextStyle(
                        fontFamily: 'iranSansMobile',
                        fontSize: 15.0,
                        color: Color(0xff999999),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SelectCategories()));
                },
                child: Container(
                  padding: EdgeInsets.only(right: 20.0),
                  alignment: Alignment.centerRight,
                  width: withOfScreen,
                  height: 40.0,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    boxShadow: [
                      new BoxShadow(
                        color: Colors.grey[400],
                        // blurRadius: 3.0,
                        offset: Offset(0, 1),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Text(
                    subCategorie == null ? "دسته بندی  (اجباری)" : subCategorie,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                      fontSize: 15.0,
                      color: subCategorie == null
                          ? Color(0xff999999)
                          : Colors.black,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  cupertino(context);
                },
                child: Container(
                  padding: EdgeInsets.only(right: 20.0),
                  alignment: Alignment.centerRight,
                  width: withOfScreen,
                  height: 40.0,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    boxShadow: [
                      new BoxShadow(
                        color: Colors.grey[400],
                        // blurRadius: 3.0,
                        offset: Offset(0, 1),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Text(
                    typeOfSell.length == 0
                        ? "نوع ارائه (اجباری)"
                        : typeOfSell
                            .toString()
                            .replaceAll(r'[', "")
                            .replaceAll(r']', ""),
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                      fontSize: 15.0,
                      color: typeOfSell.length == 0
                          ? Color(0xff999999)
                          : Colors.black,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
              ),
              SizedBox(
                height: adl ? 10 : 0,
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    height: adl ? 10 : 0,
                  ),
                  Expanded(
                    child: adl
                        ? Padding(
                            padding: EdgeInsets.only(right: 8.0, left: 13.0),
                            child: Material(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(35.0)),
                              elevation: 2.0,
                              child: TextField(
                                controller: _tedadAdlController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  adlStringQuantity = value;
                                },
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontFamily: 'iranSansMobile',
                                ),
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      0.0, 10.0, 18.0, 10.0),
                                  border: InputBorder.none,
                                  hintText: "تعداد عدل",
                                  hintStyle: TextStyle(
                                    fontFamily: 'iranSansMobile',
                                    fontSize: 15.0,
                                    color: Color(0xff999999),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.all(0),
                          ),
                  ),
                  Expanded(
                    child: adl
                        ? Padding(
                            padding:
                                const EdgeInsets.only(right: 13.0, left: 8.0),
                            child: Material(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(35.0)),
                              elevation: 2.0,
                              child: TextField(
                                controller: _sizeAdlController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  adlString = value;
                                },
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontFamily: 'iranSansMobile',
                                ),
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      0.0, 10.0, 18.0, 10.0),
                                  border: InputBorder.none,
                                  hintText: "متراژ هر عدل",
                                  hintStyle: TextStyle(
                                    fontFamily: 'iranSansMobile',
                                    fontSize: 15.0,
                                    color: Color(0xff999999),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.all(0),
                          ),
                  ),
                ],
              ),
              SizedBox(
                height: taghe ? 10 : 0,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: taghe
                        ? Padding(
                            padding: EdgeInsets.only(right: 8.0, left: 13.0),
                            child: Material(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(35.0)),
                              elevation: 2.0,
                              child: TextField(
                                controller: _tedadTagheController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  tagheStringQuantity = value;
                                },
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontFamily: 'iranSansMobile',
                                ),
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      0.0, 10.0, 18.0, 10.0),
                                  border: InputBorder.none,
                                  hintText: "تعداد طاقه",
                                  hintStyle: TextStyle(
                                    fontFamily: 'iranSansMobile',
                                    fontSize: 15.0,
                                    color: Color(0xff999999),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.all(0),
                          ),
                  ),
                  Expanded(
                    child: taghe
                        ? Padding(
                            padding:
                                const EdgeInsets.only(right: 13.0, left: 8.0),
                            child: Material(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(35.0)),
                              elevation: 2.0,
                              child: TextField(
                                controller: _sizeTagheController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  tagheString = value;
                                },
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontFamily: 'iranSansMobile',
                                ),
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      0.0, 10.0, 18.0, 10.0),
                                  border: InputBorder.none,
                                  hintText: "متراژ هر طاقه",
                                  hintStyle: TextStyle(
                                    fontFamily: 'iranSansMobile',
                                    fontSize: 15.0,
                                    color: Color(0xff999999),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: EdgeInsets.all(0),
                          ),
                  ),
                ],
              ),
              SizedBox(
                height: metraj ? 10 : 0,
              ),
              metraj
                  ? Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding:
                                const EdgeInsets.only(right: 13.0, left: 13.0),
                            child: Material(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(35.0)),
                              elevation: 2.0,
                              child: TextField(
                                controller: _metrajController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  metrajString = value;
                                },
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontFamily: 'iranSansMobile',
                                ),
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      0.0, 10.0, 18.0, 10.0),
                                  border: InputBorder.none,
                                  hintText: "متراژ موجود",
                                  hintStyle: TextStyle(
                                    fontFamily: 'iranSansMobile',
                                    fontSize: 15.0,
                                    color: Color(0xff999999),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // Expanded(
                        //     child: Padding(
                        //   padding:
                        //       const EdgeInsets.only(right: 13.0, left: 13.0),
                        //   child: Material(
                        //     borderRadius:
                        //         BorderRadius.all(Radius.circular(35.0)),
                        //     elevation: 2.0,
                        //     child: TextField(
                        //       controller: _sizeMetrajController,
                        //       keyboardType: TextInputType.number,
                        //       onChanged: (value) {
                        //         sizeMetrajString = value;
                        //       },
                        //       textDirection: TextDirection.rtl,
                        //       style: TextStyle(
                        //         fontFamily: 'iranSansMobile',
                        //       ),
                        //       textAlign: TextAlign.right,
                        //       decoration: InputDecoration(
                        //         contentPadding:
                        //             EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        //         border: InputBorder.none,
                        //         hintText: "معیار متراژی",
                        //         hintStyle: TextStyle(
                        //           fontFamily: 'iranSansMobile',
                        //           fontSize: 15.0,
                        //           color: Color(0xff999999),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // )),
                      ],
                    )
                  : Padding(
                      padding: EdgeInsets.all(0),
                    ),
              SizedBox(
                height: tedadi ? 10 : 0,
              ),
              tedadi
                  ? Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding:
                                const EdgeInsets.only(right: 13.0, left: 13.0),
                            child: Material(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(35.0)),
                              elevation: 2.0,
                              child: TextField(
                                controller: _tedadController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  tedadString = value;
                                },
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontFamily: 'iranSansMobile',
                                ),
                                textAlign: TextAlign.right,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(
                                      0.0, 10.0, 18.0, 10.0),
                                  border: InputBorder.none,
                                  hintText: "تعداد موجود",
                                  hintStyle: TextStyle(
                                    fontFamily: 'iranSansMobile',
                                    fontSize: 15.0,
                                    color: Color(0xff999999),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // Expanded(
                        //     child: Padding(
                        //   padding:
                        //       const EdgeInsets.only(right: 13.0, left: 13.0),
                        //   child: Material(
                        //     borderRadius:
                        //         BorderRadius.all(Radius.circular(35.0)),
                        //     elevation: 2.0,
                        //     child: TextField(
                        //       controller: _sizeMetrajController,
                        //       keyboardType: TextInputType.number,
                        //       onChanged: (value) {
                        //         sizeMetrajString = value;
                        //       },
                        //       textDirection: TextDirection.rtl,
                        //       style: TextStyle(
                        //         fontFamily: 'iranSansMobile',
                        //       ),
                        //       textAlign: TextAlign.right,
                        //       decoration: InputDecoration(
                        //         contentPadding:
                        //             EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        //         border: InputBorder.none,
                        //         hintText: "معیار متراژی",
                        //         hintStyle: TextStyle(
                        //           fontFamily: 'iranSansMobile',
                        //           fontSize: 15.0,
                        //           color: Color(0xff999999),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // )),
                      ],
                    )
                  : Padding(
                      padding: EdgeInsets.all(0),
                    ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                child: Material(
                  borderRadius: BorderRadius.all(Radius.circular(35.0)),
                  elevation: 2.0,
                  child: TextField(
                    controller: _regularPriceController,
                    keyboardType: TextInputType.number,

                    onChanged: (value) {
                      // print(value.length);
                      print(_regularPriceController.text.length);
                      if (_regularPriceController.text.length == 5) {
print("object");
                        _regularPriceController.updateMask("0,000000",
                            moveCursorToEnd: false);

                      } else if (_regularPriceController.text.length == 6) {

                        _regularPriceController.updateMask("00,000000000",
                            moveCursorToEnd: false);

                      }else if (_regularPriceController.text.length == 7) {

                        _regularPriceController.updateMask("000,000000000",
                            moveCursorToEnd: false);

                      }else if (_regularPriceController.text.length == 8) {

                        _regularPriceController.updateMask("0,000,000000000",
                            moveCursorToEnd: false);

                      }else if (_regularPriceController.text.length == 10) {

                        _regularPriceController.updateMask("00,000,000000000",
                            moveCursorToEnd: false);

                      }else if (_regularPriceController.text.length == 11) {

                        _regularPriceController.updateMask("000,000,000000000",
                            moveCursorToEnd: false);

                      }else {
                        
                        _regularPriceController.updateMask("000,000,000,000,000",
                            moveCursorToEnd: false);
                      }
                      // else if(_regularPriceController.text.length == 6){
                      //   print(7);

                      // _regularPriceController.updateMask("00,000");
                      // }else if(_regularPriceController.text.length == 7){
                      //   print(9);

                      // _regularPriceController.updateMask("000,000");
                      // }else if(_regularPriceController.text.length == 8){
                      //   print(8);

                      // _regularPriceController.updateMask("0,000,000");
                      // }else if(_regularPriceController.text.length == 9){
                      //   print(11);

                      // _regularPriceController.updateMask("00,000,000");
                      // }else if(_regularPriceController.text.length == 10){
                      //   print(12);

                      // _regularPriceController.updateMask("000,000,000");
                      // }else{

                      // _regularPriceController.updateMask("000000000");
                      // }

                      regularPrice = value.replaceAll(",", "");
                    },
                    // textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                    ),
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                        prefix: Padding(
                          padding: const EdgeInsets.only(left: 18.0),
                          child: Text("ریال"),
                        ),
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        border: InputBorder.none,
                        hintText: tedadi
                            ? "قیمت هر دانه (ریال)"
                            : "قیمت هر متر (ریال)",
                        hintStyle: TextStyle(
                          fontFamily: 'iranSansMobile',
                          color: Color(0xff999999),
                          fontSize: 15.0,
                        )),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                child: Material(
                  borderRadius: BorderRadius.all(Radius.circular(35.0)),
                  elevation: 2.0,
                  child: TextField(
                    controller: _salePriceController,
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      salePrice = value;
                    },
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                    ),
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        border: InputBorder.none,
                        hintText: "درصد تخفیف",
                        hintStyle: TextStyle(
                          fontFamily: 'iranSansMobile',
                          color: Color(0xff999999),
                          fontSize: 15.0,
                        )),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    right: 13.0, left: 13.0, bottom: 20.0),
                child: Material(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  elevation: 2.0,
                  child: TextField(
                    controller: _decriptionController,
                    onChanged: (value) {
                      description = value;
                    },
                    maxLines: 4,
                    minLines: 4,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                    ),
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        border: InputBorder.none,
                        hintText: "توضیحات",
                        hintStyle: TextStyle(
                          fontFamily: 'iranSansMobile',
                          fontSize: 15.0,
                          color: Color(0xff999999),
                        )),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (_countForFirstTimeClick == 0) {
                    if (indexOfProductOfMyStore != null) {
                      print(
                          "\n\n               index  $indexOfProductOfMyStore                  \n\n\n");

                      String _typeOfSell = typeOfSell
                          .toString()
                          .replaceAll("[", "")
                          .replaceAll("]", "");

                      if (double.parse(_salePriceController.text).round() >
                          99) {
                        print("\n\n                 1                  \n\n\n");
                        _showErrorDialog("درصد تخفیف باید کوچکتر از 99% باشد");
                      } else if (getImage(
                              ProductOfMyStore, indexOfProductOfMyStore) ==
                          null) {
                        print("\n\n                 2                  \n\n\n");
                        _showErrorDialog("عکسی انتخاب نشده است!");
                      } else if (_nameController.text == null) {
                        print("\n\n                 3                  \n\n\n");
                        _showErrorDialog("عنوانی انتخاب نشده است");
                      } else if (typeOfSell.length <= 0 || typeOfSell == null) {
                        print("\n\n                 4                  \n\n\n");
                        _showErrorDialog("نوع ارائه ای مشخص نکرده اید");
                      } else if (taghe &&
                          (_sizeTagheController.text == null ||
                              _tedadTagheController.text == null ||
                              _sizeTagheController.text.length < 1 ||
                              _tedadTagheController.text.length < 1)) {
                        print("\n\n                 5                  \n\n\n");
                        _showErrorDialog("مقادیر طاقه را به درستی وارد کنید");
                      } else if (adl &&
                          (_sizeAdlController.text == null ||
                              _tedadAdlController.text == null ||
                              _sizeAdlController.text.length < 1 ||
                              _tedadAdlController.text.length < 1)) {
                        _showErrorDialog("مقادیر عدل را به درستی وارد کنید");
                      } else if (metraj &&
                          (_metrajController.text == null ||
                              _metrajController.text.length < 1)) {
                        print("\n\n                 7                  \n\n\n");
                        _showErrorDialog("مقادیر متراژ را به درستی وارد کنید");
                      } else if (_regularPriceController.text
                              .replaceAll(",", "") ==
                          null) {
                        print("\n\n                 8                  \n\n\n");
                        _showErrorDialog("قیمت وارد نشده است");
                      } else if (idCategorie == null) {
                        print("\n\n                 9                  \n\n\n");
                        _showErrorDialog(
                            "دسته ای برای این محصول انتخاب نشده است");
                      } else {
                        print("\n  metraj   $metraj \n");
                        print("\n  size metraj   $sizeMetrajString \n");
                        print("\n  andazi metraj   $metrajString \n");
                        print(
                            "\n\n                 10                  \n\n\n");
                        _countForFirstTimeClick++;
                        _scaffoldKey.currentState
                            .showSnackBar(loading_SnackBar());
                        int _regularPricedouble = double.parse(
                                _regularPriceController.text
                                    .replaceAll(",", ""))
                            .toInt();
                        int _salePriceInt =
                            double.parse(_salePriceController.text).toInt();

                        String _salePrice = (_regularPricedouble -
                                ((_regularPricedouble * _salePriceInt) / 100))
                            .round()
                            .toString();
                        /////////////////////////////     ezafi     //////////////
                        String reg = (double.parse(
                                  replaceEnglishNumber(_regularPriceController
                                      .text
                                      .replaceAll(",", "")),
                                ) )
                            .toString();

                        print(
                          "\n\n\n    total quan" +
                              totalQuantity(
                                  metraj ? _metrajController.text : "0",
                                  adl ? _sizeAdlController.text : "0",
                                  adl ? _tedadAdlController.text : "0",
                                  taghe ? _sizeTagheController.text : '0',
                                  taghe ? _tedadTagheController.text : "0",
                                  tedadi ? _tedadController.text : '0'),
                        );
                        String sal = _salePriceController.text == null
                            ? (double.parse(replaceEnglishNumber(
                                        _regularPriceController.text
                                            .replaceAll(",", ""))) )
                                .toString()
                            : (double.parse(replaceEnglishNumber(_salePrice)) )
                                .toString();
                        Map body = {
                          "name": _nameController.text,
                          "regular_price": reg,
                          "sale_price": sal,
                          "stock_quantity": totalQuantity(
                              metraj ? _metrajController.text : "0",
                              adl ? _sizeAdlController.text : "0",
                              adl ? _tedadAdlController.text : "0",
                              taghe ? _sizeTagheController.text : '0',
                              taghe ? _tedadTagheController.text : "0",
                              tedadi ? _tedadController.text : '0'),
                          "manage_stock": "true",
                          "description": _decriptionController.text,
                          "categories": [
                            {
                              "id": "$idCategorie",
                            },
                            {
                              "id": "$idSubCategorie",
                            }
                          ],
                          srcOfImage == null ? "" : "images": [
                            {
                              "position": 0,
                              "src": srcOfImage,
                            }
                          ],
                          typeOfSell == null ? "" : "attributes": [
                            {
                              "id": 3,
                              "name": "نوع ارائه",
                              "position": 0,
                              "visible": true,
                              "variation": false,
                              "options": "$_typeOfSell"
                            },
                            adl
                                ? {
                                    "id": 9,
                                    "name": "اندازه عدل",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$adlString"
                                  }
                                : {},
                            adl
                                ? {
                                    "id": 10,
                                    "name": "تعداد عدل",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$adlStringQuantity"
                                  }
                                : {},
                            taghe
                                ? {
                                    "id": 6,
                                    "name": "اندازه طاقه",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$tagheString"
                                  }
                                : {},
                            taghe
                                ? {
                                    "id": 12,
                                    "name": "تعداد طاقه",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$tagheStringQuantity"
                                  }
                                : {},
                            metraj
                                ? {
                                    "id": 13,
                                    "name": "معیار متراژ",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$sizeMetrajString"
                                  }
                                : {},
                            metraj
                                ? {
                                    "id": 8,
                                    "name": "اندازه متراژی",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$metrajString"
                                  }
                                : {},
                            tedadi
                                ? {
                                    "id": 15,
                                    "name": "تعداد تعدادی",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$tedadString"
                                  }
                                : {},
                          ]
                        };
                        String tokenForUser;
                        get_token_for_user(this.myList[0].phoneOfUser)
                            .then((onValue) {
                          print("        99999999999        $onValue");
                          if (onValue.toString().contains("[jwt_auth]") ==
                              false) {
                            tokenForUser = onValue.toString();
                            dbHelper.update(mylist);
                          } else {
                            _showErrorDialog("مشگلی رخ داده است");
                          }
                        }).whenComplete(() {
                          dbHelper.update(mylist);

                          print(
                            totalQuantity(
                                metraj ? _metrajController.text : "0",
                                adl ? _sizeAdlController.text : "0",
                                adl ? _tedadAdlController.text : "0",
                                taghe ? _sizeTagheController.text : '0',
                                taghe ? _tedadTagheController.text : "0",
                                tedadi ? _tedadController.text : '0'),
                          );
                          editProduct(
                                  body,
                                  tokenForUser,
                                  getId(ProductOfMyStore,
                                      indexOfProductOfMyStore))
                              .whenComplete(() {
                            Navigator.pop(context);
                            subCategorie = null;
                            srcOfImage = null;
                            name = null;
                            regularPrice = null;
                          });
                        });
                      }
                    } else {
                      String _typeOfSell = typeOfSell
                          .toString()
                          .replaceAll("[", "")
                          .replaceAll("]", "");

                      if (int.parse(salePrice) > 99) {
                        _showErrorDialog("درصد تخفیف باید کوچکتر از 99% باشد");
                      } else if (srcOfImage == null) {
                        _showErrorDialog("عکسی انتخاب نشده است!");
                      } else if (name == null) {
                        _showErrorDialog("عنوانی انتخاب نشده است");
                      } else if (typeOfSell.length <= 0 || typeOfSell == null) {
                        _showErrorDialog("نوع ارائه ای مشخص نکرده اید");
                      } else if (taghe &&
                          (tagheString == null ||
                              tagheStringQuantity == null ||
                              tagheString.length < 1 ||
                              tagheStringQuantity.length < 1)) {
                        _showErrorDialog("مقادیر طاقه را به درستی وارد کنید");
                      } else if (adl &&
                          (adlString == null ||
                              adlStringQuantity == null ||
                              adlString.length < 1 ||
                              adlStringQuantity.length < 1)) {
                        _showErrorDialog("مقادیر عدل را به درستی وارد کنید");
                      } else if (metraj &&
                          (metrajString == null || metrajString.length < 1)) {
                        _showErrorDialog("مقادیر متراژ را به درستی وارد کنید");
                      } else if (regularPrice == null) {
                        _showErrorDialog("قیمت وارد نشده است");
                      } else if (idCategorie == null) {
                        _showErrorDialog(
                            "دسته ای برای این محصول انتخاب نشده است");
                      } else {
                        _countForFirstTimeClick++;
                        _scaffoldKey.currentState
                            .showSnackBar(loading_SnackBar());
                        int _regularPriceInt = int.parse(regularPrice);
                        int _salePriceInt = int.parse(salePrice);

                        String _salePrice = (_regularPriceInt -
                                ((_regularPriceInt * _salePriceInt) / 100))
                            .round()
                            .toString();

                        /////////////////////////////     ezafi     //////////////
                        String reg = (double.parse(
                                  replaceEnglishNumber(_regularPriceController
                                      .text
                                      .replaceAll(",", "")),
                                ) )
                            .toString();

                        String sal = _salePriceController.text == null
                            ? (double.parse(replaceEnglishNumber(
                                        _regularPriceController.text
                                            .replaceAll(",", ""))) )
                                .toString()
                            : (double.parse(replaceEnglishNumber(_salePrice)) )
                                .toString();

                        print(
                          "\n\n\n    total quan" +
                              totalQuantity(
                                  metraj ? _metrajController.text : "0",
                                  adl ? _sizeAdlController.text : "0",
                                  adl ? _tedadAdlController.text : "0",
                                  taghe ? _sizeTagheController.text : '0',
                                  taghe ? _tedadTagheController.text : "0",
                                  tedadi ? _tedadController.text : '0'),
                        );
                        Map body = {
                          "name": name,
                          "regular_price": reg,
                          "sale_price": sal,
                          "description": description,
                          "status": "pending",
                          "manage_stock": "true",
                          "stock_quantity": totalQuantity(
                              metraj ? _metrajController.text : "0",
                              adl ? _sizeAdlController.text : "0",
                              adl ? _tedadAdlController.text : "0",
                              taghe ? _sizeTagheController.text : '0',
                              taghe ? _tedadTagheController.text : "0",
                              tedadi ? _tedadController.text : '0'),
                          "categories": [
                            {
                              "id": "$idCategorie",
                            },
                            {
                              "id": "$idSubCategorie",
                            }
                          ],
                          srcOfImage == null ? "" : "images": [
                            {
                              "position": 0,
                              "src": srcOfImage,
                            }
                          ],
                          typeOfSell == null ? "" : "attributes": [
                            {
                              "id": 3,
                              "name": "نوع ارائه",
                              "position": 0,
                              "visible": true,
                              "variation": false,
                              "options": "$_typeOfSell"
                            },
                            adl
                                ? {
                                    "id": 9,
                                    "name": "اندازه عدل",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$adlString"
                                  }
                                : {},
                            adl
                                ? {
                                    "id": 10,
                                    "name": "تعداد عدل",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$adlStringQuantity"
                                  }
                                : {},
                            taghe
                                ? {
                                    "id": 6,
                                    "name": "اندازه طاقه",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$tagheString"
                                  }
                                : {},
                            taghe
                                ? {
                                    "id": 12,
                                    "name": "تعداد طاقه",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$tagheStringQuantity"
                                  }
                                : {},
                            metraj
                                ? {
                                    "id": 8,
                                    "name": "اندازه متراژی",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$metrajString"
                                  }
                                : {},
                            metraj
                                ? {
                                    "id": 13,
                                    "name": "معیار متراژ",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$sizeMetrajString"
                                  }
                                : {},
                            tedadi
                                ? {
                                    "id": 15,
                                    "name": "تعداد تعدادی",
                                    "position": 0,
                                    "visible": true,
                                    "variation": false,
                                    "options": "$tedadString"
                                  }
                                : {},
                          ]
                        };
                        String tokenForUser;
                        get_token_for_user(this.myList[0].phoneOfUser)
                            .then((onValue) {
                          print("        99999999999        $onValue");
                          if (onValue.toString().contains("[jwt_auth]") ==
                              false) {
                            tokenForUser = onValue.toString();
                            dbHelper.update(mylist);
                          } else {
                            _showErrorDialog("مشگلی رخ داده است");
                          }
                        }).whenComplete(() {
                          dbHelper.update(mylist);

                          addProduct(
                            body,
                            tokenForUser,
                          ).whenComplete(() {
                            Navigator.pop(context);
                            subCategorie = null;
                            srcOfImage = null;
                            name = null;
                            regularPrice = null;
                          });
                        });
                      }
                    }
                  }
                },
                child: Container(
                  width: 800.0,
                  height: 60.0,
                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20.0),
                        bottomRight: Radius.circular(20.0)),
                    color: Color(0xffc80058),
                  ),
                  child: Center(
                    child: Text(
                      "ثبت",
                      style: TextStyle(
                        fontSize: 21.0,
                        color: Color(0xfffafafa),
                        fontFamily: "iranSansMobile",
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showErrorDialog(String str) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(
          str,
          style: TextStyle(
            fontFamily: "IransansLight",
            fontSize: 16,
            color: Color(0xffffffff),
          ),
          textDirection: TextDirection.rtl,
        ),
        backgroundColor: Color(0xffc80058),
      ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;

          mylist = myList[0];
        });
      });
    });
  }

  cupertino(BuildContext context) {
    return showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: const Text(
          'یکی از انواع ارائه ها را انتخاب کنید',
          style: TextStyle(
              fontFamily: "iranSansD", color: Colors.black, fontSize: 16.0),
        ),
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: Text(
              'عدل',
              style: TextStyle(
                color: adl ? Color(0xffc80058) : Colors.grey,
                fontFamily: "iranSansD",
                fontSize: 14.0,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
              setState(() {});
              adl ? typeOfSell.remove("عدل") : typeOfSell.add("عدل");
              adl = !adl;
            },
          ),
          CupertinoActionSheetAction(
            child: Text(
              'طاقه',
              style: TextStyle(
                color: taghe ? Color(0xffc80058) : Colors.grey,
                fontFamily: "iranSansD",
                fontSize: 14.0,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
              setState(() {});

              taghe ? typeOfSell.remove("طاقه") : typeOfSell.add("طاقه");
              taghe = !taghe;
            },
          ),
          CupertinoActionSheetAction(
            child: Text(
              'متری (کاتینگ)',
              style: TextStyle(
                color: metraj ? Color(0xffc80058) : Colors.grey,
                fontFamily: "iranSansD",
                fontSize: 14.0,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
              setState(() {
                metraj ? typeOfSell.remove("متراژی") : typeOfSell.add("متراژی");
                metraj = !metraj;
              });
            },
          ),
          CupertinoActionSheetAction(
            child: Text(
              '   (لوازم) تعدادی ',
              style: TextStyle(
                color: tedadi ? Color(0xffc80058) : Colors.grey,
                fontFamily: "iranSansD",
                fontSize: 14.0,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
              setState(() {});

              tedadi ? typeOfSell.remove("تعدادی") : typeOfSell.add("تعدادی");
              tedadi = !tedadi;
            },
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: const Text(
            'هیچ کدام',
            style: TextStyle(
                fontFamily: "iranSansD", color: Colors.black, fontSize: 16.0),
          ),
          isDefaultAction: true,
          onPressed: () {
            setState(() {});
            Navigator.pop(context);
            typeOfSell = null;
          },
        ),
      ),
    );
  }
}

// loading_SnackBar() {
//   return SnackBar(
//     behavior: SnackBarBehavior.floating,
//     duration: Duration(seconds: 20),
//     content: Container(
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceAround,
//         children: <Widget>[
//           CircularProgressIndicator(
//             valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
//             backgroundColor: Colors.red,
//           ),
//           Text(
//             "لطفا صبور باشید",
//             style: TextStyle(
//                 color: Color(0xffffffff),
//                 fontFamily: "iranYekan",
//                 fontSize: 14),
//           ),
//         ],
//       ),
//     ),
//     backgroundColor: Color(0xffc80058),
//   );
// }

String totalQuantity(String metraj, String sizeAdl, String tedadAdl,
    String sizeTaghe, String tedadTaghe, String tedadi) {
  double _metraj = double.parse(metraj);
  double _sizeAdl = double.parse(sizeAdl);
  double _tedadAdl = double.parse(tedadAdl);
  double _sizeTaghe = double.parse(sizeTaghe);
  double _tedadTaghe = double.parse(tedadTaghe);

  print("\n\n\n\n" +
      "$_metraj\n$_sizeAdl\n$_tedadAdl\n$_sizeTaghe\n$_tedadTaghe");

  String total = (_metraj + (_sizeAdl * _tedadAdl) + (_sizeTaghe * _tedadTaghe))
      .toStringAsFixed(0);

  print("\n\n\n\n" +
      "$metraj\n$sizeAdl\n$tedadAdl\n$sizeTaghe\n$tedadTaghe\n$tedadi\ntotal:$total");

  return tedadi == "0" ? total : tedadi;
}
