import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/pages/IntroductionPage.dart';
import 'package:pardemarketapp/pages/department_store/CategoryPage/CategoryAppbar.dart';
import 'package:pardemarketapp/pages/department_store/CategoryPage/CategoryPage.dart';
import 'package:pardemarketapp/pages/department_store/StorePage/StoreAppBar.dart';
import 'package:pardemarketapp/pages/department_store/StorePage/storePage.dart';
import 'package:pardemarketapp/pages/department_store/UserPanelPage/UserPanelAppBar.dart';
import 'package:pardemarketapp/pages/department_store/UserPanelPage/UserPanelPage.dart';


class BottomNavyBarMain extends StatefulWidget {
  @override
  _BottomNavyBarMainState createState() => _BottomNavyBarMainState();
}

class _BottomNavyBarMainState extends State<BottomNavyBarMain> {
  int selectedIndex = 2;
  Color backgroundColor = Colors.white;

  List<NavigationItem> items = [
    NavigationItem(
        Icon(Icons.person_outline), Text('حساب من'), Color(0xffc80058)),
    NavigationItem(Icon(Icons.list), Text('دسته ها'), Color(0xffc80058)),
    NavigationItem(Icon(Icons.store), Text('فروشگاه'), Color(0xffc80058)),
  ];

  StorePage one;
  StoreAppBar oneAppbar;

  UserPanelPage two;
  UserPanelAppBar twoAppbar;

  CategoryPage three;
  CategoryAppbar threeAppbar;

  List<Widget> pages;
  List<Widget> Appbars;

  Widget currentPage;
  Widget currentAppbar;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    one = StorePage();
    two = UserPanelPage();
    three = CategoryPage();

    oneAppbar = StoreAppBar();
    twoAppbar = UserPanelAppBar();
    threeAppbar = CategoryAppbar();

    pages = [two, three, one];
    currentPage = one;

    Appbars = [twoAppbar, threeAppbar, oneAppbar];
    currentAppbar = oneAppbar;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    double customHeight = height / 8;

    switch (selectedIndex) {
      case 0:
        customHeight = 210;
        break;
      case 1:
        customHeight = 50.0;
        break;
      case 2:
        customHeight = 70.0;
        break;
    }
    DateTime currentBackPressTime;
    Future<bool> _onBackPressed() {
      DateTime currentTime = DateTime.now();

      DateTime now = DateTime.now();
      if (currentBackPressTime == null ||
          now.difference(currentBackPressTime) > Duration(seconds: 2)) {
        currentBackPressTime = now;
        // Fluttertoast.showToast(msg: exit_warning);
        showErrorDialog("برای خروج دوبار ضربه بزنید", _scaffoldKey);
        return Future.value(false);
      }
      return Future.value(true);
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: PreferredSize(
        preferredSize: Size(width, customHeight),
        child: currentAppbar,
      ),
      body: WillPopScope(
        child: currentPage,
        onWillPop: _onBackPressed,
      ),
      bottomNavigationBar: Container(
        height: height / 12,
        decoration: BoxDecoration(
            color: backgroundColor,
            boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)]),
        padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 4, bottom: 4),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: items.map((item) {
            var itemIndex = items.indexOf(item);
            return GestureDetector(
              onTap: () {
                setState(() {
                  selectedIndex = itemIndex;
                  currentPage = pages[itemIndex];
                  currentAppbar = Appbars[itemIndex];
                });
              },
              child: _buildItem(item, selectedIndex == itemIndex),
            );
          }).toList(),
        ),
      ),
    );
  }

  Widget _buildItem(NavigationItem item, bool isSelected) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 270),
      height: double.maxFinite,
      width: isSelected ? 120 : 50,
      padding: isSelected ? EdgeInsets.only(left: 16, right: 16) : null,
      decoration: isSelected
          ? BoxDecoration(
              color: item.color,
              borderRadius: BorderRadius.all(Radius.circular(40)))
          : null,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              IconTheme(
                data: IconThemeData(
                    size: 24.0,
                    color: isSelected ? backgroundColor : Colors.black),
                child: item.icon,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: isSelected
                    ? DefaultTextStyle.merge(
                        style: TextStyle(
                            color: backgroundColor,
                            fontFamily: 'iranYekan',
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                        child: item.title)
                    : Container(),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class NavigationItem {
  final Icon icon;
  final Text title;
  final Color color;

  NavigationItem(this.icon, this.title, this.color);
}
