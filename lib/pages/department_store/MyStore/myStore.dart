import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/monyString.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/StorePage/storePage.dart';
import 'package:pardemarketapp/pages/department_store/URLS/urlS.dart';
import 'package:pardemarketapp/pages/department_store/addProducts/addProducts.dart';
import 'package:pardemarketapp/pages/department_store/become_A_Vendor/becomeAVendor.dart';
import 'package:pardemarketapp/pages/department_store/show_a_single_picture/pictureOfMyStorePage.dart';
import 'package:pardemarketapp/pages/department_store/singleProducts/singleProducts.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pardemarketapp/services/addProducts.dart';
import 'package:pardemarketapp/services/deletProduct.dart';
import 'package:pardemarketapp/services/get_Sotre_Info.dart';
import '../../../services/getProducts.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class MyStore extends StatefulWidget {
  Function setstate;

  MyStore([this.setstate]);
  @override
  _MyStoreState createState() => _MyStoreState(setstate);
}

Color backgroundPages = Color(0xfffafafa);
int indexOfProductOfMyStore;
List ProductOfMyStore = new List();

class _MyStoreState extends State<MyStore> with TickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  Function setstate;
  _MyStoreState(this.setstate);

  AnimationController animateAppbar;
  Animation changeSize;

  DbHelper dbHelper = new DbHelper();

  List<MyList> myList;

  MyList mylist;

  int count = 0;

  void initState() {
    getData();
    animateAppbar = new AnimationController(
        duration: Duration(milliseconds: 2500), vsync: this);
    changeSize = Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(parent: animateAppbar, curve: Curves.elasticOut));
    animateAppbar.forward();

    Future.delayed(Duration(seconds: 1)).whenComplete(() {
      get_Store_Info(this.myList[0].userId).whenComplete(() {
        this.mylist.nameOfUser = storFirstName(store_Info);

        dbHelper.update(mylist);

        setState(() {});
      });
    });
    super.initState();
  }

  Future<List> getProducts_forUser(String id) async {
    print("get getProducts_forUser");
    print("id   $id");
    http.Response response = await http.get(
      ("https://pardemarket.com/wp-json/dokan/v1/stores/$id/products?status=any&per_page=100"),
      headers: {
        "Accept": "application/json",
      },
    );

    ProductOfMyStore = json.decode(response.body);

    // print("lenght of product (getProduct)       " +
    //     ProductOfMyStore.length.toString());
    print("${json.decode(response.body)}");
    return ProductOfMyStore = json.decode(response.body);
  }

  reload() {
    print("reload");
    return setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    print("${toMony("123456789")}");

    if (myList == null) {
      print("\n\n aaaaaaaaaaaaa\n\n");
      myList = new List<MyList>();
      getData();
    }

    if (countVendorMyStor == 1) {
      showErrorDialog("در حال تغییر اطلاعات فروشگاه", _scaffoldKey);

      get_Store_Info(this.myList[0].userId).whenComplete(() {
        _scaffoldKey.currentState.hideCurrentSnackBar();
        setState(() {
          countVendorMyStor = 0;
        });
      });
    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffc80058),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => addProducts()));
        },
        child: Icon(
          Icons.add,
          size: 30.0,
          color: Color(0xfffafafa),
        ),
      ),
      body: mylist == null
          ? Text("")
          : Stack(
              children: <Widget>[
                Padding(
                  padding: ProductOfMyStore.length == 0
                      ? EdgeInsets.only(top: 5.0)
                      : EdgeInsets.only(top: 5.0),
                  child:
                      // ProductOfMyStore.length == 0
                      //     ? Center(
                      //         child: Text("شما هنوز محصولی وارد نکرده اید",style: TextStyle(
                      //           color: Color(0xffc80058),
                      //           fontFamily: "iranYekan"
                      //         ),),
                      //       )
                      //     :
                      FutureBuilder(
                          future: getProducts_forUser(this.mylist.userId),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.data == null) {
                              return Center(
                                child: SpinKitCubeGrid(
                                  color: Color(0xffc80058),
                                ),
                              );
                            } else if (snapshot.data.toString().length <= 2) {
                              return Container(
                                padding: EdgeInsets.only(top: 120),
                                alignment: Alignment.center,
                                child: Text(
                                  "شما هنوز محصولی وارد نکرده اید",
                                  style: TextStyle(
                                      color: Color(0xffc80058),
                                      fontFamily: "iranYekan"),
                                ),
                              );
                            } else {
                              return GridView.builder(
                                padding: EdgeInsets.fromLTRB(20, 200, 20, 20),
                                semanticChildCount: 2,
                                cacheExtent: 20.0,
                                itemCount: ProductOfMyStore.length,
                                gridDelegate:
                                    new SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 20,
                                  childAspectRatio: .7,
                                  mainAxisSpacing: 20.0,
                                ),
                                itemBuilder:
                                    (BuildContext context, int index) {
                                  return InkWell(
                                    onTap: () {
                                      itemIndex = index;
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              SingleProduct(ProductOfMyStore),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      width: 150.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(19.0)),
                                          color: Color(0xfffafafa),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Color(0xffD4D4D4),
                                              blurRadius: 10.0,
                                            ),
                                          ]),
                                      child: Column(
                                        children: <Widget>[
                                          Stack(
                                            children: <Widget>[
                                              ClipRRect(
                                                child:
                                                    FadeInImage.assetNetwork(
                                                  placeholder:
                                                      'assets/imgs/zero_to_infinity_by_volorf.gif',
                                                  image: getImage(
                                                      ProductOfMyStore,
                                                      index),
                                                  width: 300,
                                                  height: 135,
                                                  fit: BoxFit.fill,
                                                ),
                                                borderRadius:
                                                    BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(19.0),
                                                  topRight:
                                                      Radius.circular(19.0),
                                                ),
                                              ),
                                              Positioned(
                                                top: 0,
                                                left: 0,
                                                child: InkWell(
                                                  onTap: () {
                                                    indexOfProductOfMyStore =
                                                        index;
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder:
                                                            (BuildContext) =>
                                                                addProducts(),
                                                      ),
                                                    );
                                                  },
                                                  child: Container(
                                                    width: 35.0,
                                                    height: 35.0,
                                                    decoration: BoxDecoration(
                                                        color:
                                                            Color(0xfffafafa),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                bottomRight:
                                                                    Radius.circular(
                                                                        19.0),
                                                                topLeft: Radius
                                                                    .circular(
                                                                        19.0))),
                                                    child: Icon(
                                                      Icons.mode_edit,
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                top: 0,
                                                right: 0,
                                                child: InkWell(
                                                  onTap: () {
                                                    cupertinoForDeleteProduct(
                                                        context,
                                                        getId(
                                                            ProductOfMyStore,
                                                            index),
                                                        reload,
                                                        _scaffoldKey);
                                                  },
                                                  child: Container(
                                                    width: 35.0,
                                                    height: 35.0,
                                                    decoration: BoxDecoration(
                                                        color:
                                                            Color(0xfffafafa),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                bottomLeft:
                                                                    Radius.circular(
                                                                        19.0),
                                                                topRight: Radius
                                                                    .circular(
                                                                        19.0))),
                                                    child: Icon(
                                                      Icons.close,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                15.0, 0.0, 15.0, 0.0),
                                            child: AutoSizeText(
                                              getName(
                                                  ProductOfMyStore, index),
                                              maxLines: 1,
                                              textDirection:
                                                  TextDirection.rtl,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontFamily: 'iranYekan',
                                                  fontSize: 15.0),
                                            ),
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.fromLTRB(
                                                    15.0, 0.0, 15.0, 0.0),
                                            child: Text(
                                              getShowPrice(
                                                  ProductOfMyStore, index),
                                              textDirection:
                                                  TextDirection.rtl,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontFamily: 'iranSansMobile',
                                                fontSize: 15.0,
                                                color: Color(0xffc80058),
                                              ),
                                            ),
                                          ),
                                          Text(getStatus(
                                              ProductOfMyStore, index))
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              );
                            }
                          }),
                ),
                Container(
                  width: width,
                  height: 200.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        // bottomRight: Radius.circular(60.0),
                        bottomLeft: Radius.circular(60.0),
                      ),
                      gradient: LinearGradient(
                          colors: [
                            Color(0xff530050),
                            Color(0xffc80058),
                          ],
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft)),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            SizedBox(
                              height: 20.0,
                            ),
                            store_Info["store_name"] == null
                                ? Container(
                                    child: SpinKitThreeBounce(
                                      color: Color(0xfffafafa),
                                      size: 20.0,
                                    ),
                                  )
                                : Text(
                                    store_Info["store_name"],
                                    style: TextStyle(
                                        color: backgroundPages,
                                        fontFamily: 'iranYekan',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0),
                                  ),
                            SizedBox(
                              height: 20.0,
                            ),
                            store_Info["first_name"] == null
                                ? Container(
                                    child: SpinKitThreeBounce(
                                      color: Color(0xfffafafa),
                                      size: 20.0,
                                    ),
                                  )
                                : Text(
                                    store_Info["first_name"],
                                    style: TextStyle(
                                        color: backgroundPages,
                                        fontFamily: 'iranYekan',
                                        fontSize: 14.0),
                                  ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Container(
                              padding:
                                  EdgeInsets.only(left: 28.0, right: 28.0),
                              width: width / 2.5,
                              height: height / 17,
                              decoration: BoxDecoration(
                                color: Color(0xfffafafa).withOpacity(0.4),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(23.0),
                                ),
                              ),
                              child: InkWell(
                                onTap: () {
                                  if (store_Info.isEmpty) {
                                    get_Store_Info("${this.myList[0].userId}")
                                        .whenComplete(() {
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              BecomeAVendor(),
                                        ),
                                      );
                                    });
                                  } else {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => BecomeAVendor(),
                                      ),
                                    );
                                  }
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text(
                                      'ویرایش',
                                      style: TextStyle(
                                          color: backgroundPages,
                                          fontFamily: 'iranYekan',
                                          fontSize: 12.0),
                                    ),
                                    Icon(
                                      Icons.edit,
                                      color: backgroundPages,
                                      size: 16.0,
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xffc80058),
                          ),
                          width: 90.0,
                          height: 90.0,
                          child: InkWell(
                            onTap: () {
                              imageAssets = store_Info["gravatar"];
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      ShowASinglePictureOfMyStorePage(),
                                ),
                              );
                            },
                            child: ClipRRect(
                                child: Hero(
                                  tag: "imageOfAvatar",
                                  child: FadeInImage.assetNetwork(
                                    placeholder:
                                        'assets/imgs/zero_to_infinity_by_volorf.gif',
                                    image: "${store_Info["gravatar"]}",
                                    width: 150,
                                    height: 150,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50.0))),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
          mylist = myList[0];
        });
      });
    });
  }

  Future<bool> _onBackPressed() async{
    print("\n\n on back pressed\n\n");
    print(databasereload);

    databasereload = true;
    print(databasereload);
    // DateTime currentTime = DateTime.now();

    // DateTime now = DateTime.now();
    // if (currentBackPressTime == null ||
    //     now.difference(currentBackPressTime) > Duration(seconds: 2)) {
    //   currentBackPressTime = now;
    //   // Fluttertoast.showToast(msg: exit_warning);
    //   showErrorDialog("برای خروج دوبار ضربه بزنید",_scaffoldKey);
    //   return Future.value(false);
    // }
    this.setstate();
    return true;
  }
}

cupertinoForDeleteProduct(BuildContext context, String index, Function reload,
    GlobalKey<ScaffoldState> _scaffoldKey) {
  return showCupertinoModalPopup(
    context: context,
    builder: (BuildContext context) => CupertinoActionSheet(
      title: Text(
        "آیا میخواهید محصول خود را حذف کنید؟",
        style: TextStyle(
            fontFamily: "iranSansD", color: Color(0xffc80058), fontSize: 16.0),
      ),
      cancelButton: CupertinoActionSheetAction(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text(
          "خیر",
          style: TextStyle(
              fontFamily: "iranSansD", color: Colors.black, fontSize: 16.0),
        ),
      ),
      actions: <Widget>[
        CupertinoActionSheetAction(
          onPressed: () {
            Navigator.pop(context);
            _scaffoldKey.currentState.showSnackBar(loading_SnackBar());
            deletProduct(index).whenComplete(() {
              _scaffoldKey.currentState.hideCurrentSnackBar();
              reload();
            });
          },
          child: Text(
            "بلی",
            style: TextStyle(
                fontFamily: "iranSansD", color: Colors.black, fontSize: 16.0),
          ),
        ),
      ],
    ),
  );
}

bool databasereload = false;
