import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pardemarketapp/pages/department_store/show_all_factors/show_all_factors.dart';
import 'package:pardemarketapp/pages/help/helpPage.dart';
import '../setting/setting.dart';
import '../support/support.dart';

Color backgroundPages = Color(0xfffafafa);

class UserPanelPage extends StatefulWidget {
  @override
  _UserPanelPageState createState() => _UserPanelPageState();
}

class _UserPanelPageState extends State<UserPanelPage> {
  List<String> vectors = [
    'assets/imgs/alert.svg',
    'assets/imgs/shopping-bag.svg',
    'assets/imgs/settings.svg',
    'assets/imgs/user.svg'
  ];

@override
  void initState() {
    
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    double iconSize = width * 0.38;

    return Container(
      padding: EdgeInsets.only(left: 15.0, right: 15.0),
      color: backgroundPages,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 28.0,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    setState(() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Setting()),
                      );
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xffD4D4D4),
                              blurRadius: 10,
                              offset: Offset(0.0, 3.0))
                        ]),
                    width: iconSize,
                    height: iconSize,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SvgPicture.asset(
                          vectors[2],
                          width: 50,
                          height: 50,
                        ),
                        Text(
                          'تنظیمات',
                          style: TextStyle(
                              color: Color(0xffaa0050),
                              fontSize: 15,
                              fontFamily: 'IranYekan'),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Show_All_Factors()));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xffD4D4D4),
                              blurRadius: 10,
                              offset: Offset(0.0, 3.0))
                        ]),
                    width: iconSize,
                    height: iconSize,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Stack(
                          alignment: AlignmentDirectional.center,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 3.0, right: 4.0),
                              child: SvgPicture.asset(
                                vectors[1],
                                width: 54.0,
                                height: 54.0,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          'فاکتور ها',
                          style: TextStyle(
                              color: Color(0xffaa0050),
                              fontSize: 15,
                              fontFamily: 'IranYekan'),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 28.0,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    setState(() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HelpPage()),
                      );
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xffD4D4D4),
                              blurRadius: 10,
                              offset: Offset(0.0, 3.0))
                        ]),
                    width: iconSize,
                    height: iconSize,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SvgPicture.asset(
                          vectors[0],
                          width: 54,
                          height: 54,
                        ),
                        Text(
                          'راهنما',
                          style: TextStyle(
                              color: Color(0xffaa0050),
                              fontSize: 15,
                              fontFamily: 'IranYekan'),
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Support()),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xffD4D4D4),
                              blurRadius: 10,
                              offset: Offset(0.0, 3.0))
                        ]),
                    width: iconSize,
                    height: iconSize,
                    // padding: EdgeInsets.all(15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SvgPicture.asset(
                          vectors[3],
                          width: 54,
                          height: 54,
                        ),
                        Text(
                          'پشتیبانی',
                          style: TextStyle(
                              color: Color(0xffaa0050),

                              fontSize: 15,
                              fontFamily: 'IranYekan'),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Text(""),
          ],
        ),
      ),
    );
  }
}
