import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/MyStore/myStore.dart';
import 'package:pardemarketapp/pages/department_store/term/term.dart';
import 'package:pardemarketapp/pages/login/loginPage.dart';

Color backgroundPages = Color(0xfffafafa);

class UserPanelAppBar extends StatefulWidget {
  @override
  UserPanelAppBarState createState() {
    return new UserPanelAppBarState();
  }
}

int count = 0;
class UserPanelAppBarState extends State<UserPanelAppBar> {

  
    void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
    mylist = this.myList[0];

        });
      });
    });
  }


reload(){
 return getData();
  // return setState((){});
}



  // AnimationController animateAppbar;
  // Animation changeSize;

  DbHelper dbHelper = new DbHelper();
  List<MyList> myList;
  MyList mylist;
  String username = 'کاربر مجازی';

  String phoneNumber = '09130000000';

  @override
  void initState() {
setState(() {
  countainer++;
});
    super.initState();
  }
int countainer =0;
  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {



getData();

    if (myList == null || databasereload == true) {
      myList = new List<MyList>();
      databasereload = false;
      getData();
    }
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return
    mylist == null?
    Text(""):
     Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(60.0),
          ),
          gradient: LinearGradient(colors: [
            Color(0xff530050),
            Color(0xffc80058),
          ], begin: Alignment.topRight, end: Alignment.bottomLeft)),
      child: Padding(
        padding: const EdgeInsets.only(top: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(
              height: myList[0].boolBecomeAVendor != "true" ? 5.0 : 0.0,
            ),
            Text(
              myList[0].boolBecomeAVendor != "true"
                  ? "شما هنوز نام نکرده اید"
                  : this.myList[0].nameOfUser,
              style: TextStyle(
                  color: backgroundPages,
                  fontFamily: 'iranYekan',
                  fontWeight: FontWeight.bold,
                  fontSize: myList[0].boolBecomeAVendor != null ? 18 : 26.0),
            ),
            myList[0].boolCustomer != "true"
                ? SizedBox(
                    height: 0.0,
                  )
                : InkWell(
                    onTap: () {},
                    child: Text(
                      myList[0].phoneOfUser == null
                          ? phoneNumber
                          : myList[0].phoneOfUser + ' : شماره شما ',
                      style: TextStyle(
                          color: backgroundPages,
                          fontFamily: 'iranSansMobile',
                          fontSize: 20.0,),
                    ),
                  ),
            Container(
              // padding: EdgeInsets.only(left: 28.0, right: 28.0),
              width: width / 2.5,
              height: height / 17,
              decoration: BoxDecoration(
                  color: Color(0xfffafafa).withOpacity(0.4),
                  borderRadius: BorderRadius.all(Radius.circular(23.0))),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    
                    MaterialPageRoute(
                        builder: (context) =>
                            myList[0].boolBecomeAVendor == "true"
                                ? MyStore(reload)
                                : myList[0].boolCustomer == "true"
                                    ? Term()
                                    : LogInAndRegister(),
                                    ),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      myList[0].boolBecomeAVendor == "true"
                          ? 'پنل فروشندگی'
                          : "ورود یا ثبت نام",
                      style: TextStyle(
                          color: backgroundPages,
                          fontFamily: 'iranYekan',
                          fontSize: 12.0),
                    ),
                    // SizedBox(
                    //   width: 10.0,
                    // ),
                    Icon(
                      myList[0].boolBecomeAVendor == "true"
                          ? Icons.store
                          : Icons.supervisor_account,
                      color: backgroundPages,
                      size: myList[0].boolBecomeAVendor == "true" ? 18.0 : 19.0,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }


}
