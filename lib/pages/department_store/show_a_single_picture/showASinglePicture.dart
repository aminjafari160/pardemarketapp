import 'package:flutter/material.dart';

String imageAssets;

class ShowASinglePicture extends StatelessWidget {
  @override
  Widget build(BuildContext context) { 
    return Hero(
      tag: "amin",
      child: Image.network(
        imageAssets,
        // fit: BoxFit.fill,
      ),
    );
  }
}
