import 'package:flutter/material.dart';

String imageAssets;

class ShowASinglePictureOfMyStorePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) { 
    return Hero(
      tag: "imageOfAvatar",
      child: Image.network(
        imageAssets,
        // fit: BoxFit.fill,
      ),
    );
  }
}