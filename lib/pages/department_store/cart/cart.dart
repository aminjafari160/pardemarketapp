import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/pages/department_store/cart/cartBody.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffeeeeee),
      body: Container(
        decoration: BoxDecoration(
          color: Color(0xffeeeeee),
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          boxShadow: [
            BoxShadow(
              color: Color(0xffd1d1d1),
              blurRadius: 16,
              offset: Offset(0, 3),
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(30.0, 60.0, 30.0, 60.0),
        child: Scaffold(
          backgroundColor: Color(0xffe2e2e2),
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16.0),
                  topRight: Radius.circular(16.0),
                  
                ),
                color: Colors.white
              ),
              alignment: Alignment.topCenter,
              width: 60.0,
              height: 60.0,
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25.0),
                  ),
                  color: Color(0xffc80058),
                ),
                width: 156.0,
                height: 54,
                child: Text(
                  "سبد خرید",
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'iranSansD',
                    fontSize: 20.0,
                  ),
                ),
              ),
            ),
          ),
          body: CartBody(),
        ),
      ),
    );
  }


}
