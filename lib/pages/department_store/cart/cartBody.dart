import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/function/monyString.dart';
import 'package:pardemarketapp/pages/department_store/Preview_Factor/preview_factor.dart';
import 'package:pardemarketapp/pages/department_store/StorePage/storePage.dart';
import 'package:pardemarketapp/pages/department_store/get_iformation_of_buyer/get_iformation_of_buyer.dart';
import 'package:pardemarketapp/services/Payment.dart';
import 'package:pardemarketapp/services/getProducts.dart';
import 'package:pardemarketapp/services/get_Sotre_Info.dart';
import 'package:pardemarketapp/services/order_get.dart';
import '../../../function/get_offer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/widgets.dart';
import '../../../function/snackBar.dart';

class CartBody extends StatefulWidget {
  @override
  _CartBodyState createState() => _CartBodyState();
}

List<int> quntityTextMetraj = new List();
List<String> quntityTextCenti = new List();
List<int> quntityTextTaghe = new List();
List<int> quntityTextAdl = new List();
List<int> quntityTextTedadi = new List();
int amin;

List ProductOfCartPage = new List();
List list_of_product = new List();

int indexAdl = 0;
int indexTaghe = 0;

class _CartBodyState extends State<CartBody> with WidgetsBindingObserver {
  bool _tagheOfCupertino = false;
  bool _adlOfCupertino = false;
  bool _metrajOfCupertino = false;
  bool _tedadOfCupertino = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  reload() {
    return setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(bottom: 51.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(16.0),
              bottomRight: Radius.circular(16.0),
            ),
          ),
          child: ProductOfCartPage.toString().length <= 10
              ? Center(
                  child: Text("هیچ محصولی را اضافه نکرده اید",
                      style: TextStyle(
                          color: Color(0xffc80058),
                          fontFamily: "iranSansMobile",
                          fontSize: 14.0)),
                )
              : ListView.builder(
                  itemCount: ProductOfCartPage.length,
                  itemBuilder: (BuildContext context, int index) {
                    Widget quntityOfProduct(String noe) {
                      return Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 20.0),
                            color: Color(0xffe0e0e0),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              mainAxisAlignment: _metrajOfCupertino
                                  ? MainAxisAlignment.center
                                  : MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Text(
                                  _tedadOfCupertino
                                      ? "تعداد کل"
                                      : _tagheOfCupertino
                                          ? "متراژ هر طاقه"
                                          : _metrajOfCupertino
                                              ? noe == "metraj"
                                                  ? "متراژ"
                                                  : "سانتی متر"
                                              : "متراژ هر عدل",
                                  style: TextStyle(
                                    fontFamily: "iranSansMobile",
                                    color: Color(0xffc80058),
                                  ),
                                ),
                                _metrajOfCupertino
                                    ? SizedBox(
                                        width: 0,
                                      )
                                    : Text(
                                        _tedadOfCupertino
                                            ? getTedadTedadi(
                                                    ProductOfCartPage, index)
                                                .toString()
                                            : _tagheOfCupertino
                                                ? getSizeTaghe(
                                                    ProductOfCartPage, index)
                                                : getSizeAdl(
                                                    ProductOfCartPage, index),
                                        style: TextStyle(
                                          color: Color(0xffc80058),
                                          fontFamily: "iranSansMobile",
                                        ),
                                      ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: CupertinoPicker.builder(
                              backgroundColor: Color(0xffe0e0e0),
                              itemBuilder: (BuildContext context, index) {
                                return Text(
                                  "$index ",
                                  style: TextStyle(
                                    color: Color(0xff333333),
                                    // fontFamily: "iranSansMobile",
                                    fontSize: 16.0,
                                  ),
                                );
                              },
                              childCount: noe == "tedadi"
                                  ? int.parse(getTedadTedadi(
                                          ProductOfCartPage, index)) +
                                      1
                                  : noe == "taghe"
                                      ? int.parse(
                                            getTedadTaghe(
                                                ProductOfCartPage, index),
                                          ) +
                                          1
                                      : noe == "metraj"
                                          ? int.parse(
                                              getTedadMetraj(
                                                  ProductOfCartPage, index),
                                            )
                                          : noe == "centi"
                                              ? 100
                                              : int.parse(
                                                    getTedadAdl(
                                                        ProductOfCartPage,
                                                        index),
                                                  ) +
                                                  1,
                              diameterRatio: 1.0,
                              scrollController:
                                  FixedExtentScrollController(initialItem: 0),
                              magnification: 1.7,
                              useMagnifier: true,
                              itemExtent: 21,
                              onSelectedItemChanged: (int value) {
                                String stringCenti;
                                setState(() {});
                                noe == "tedadi"
                                    ? quntityTextTedadi[amin] = value
                                    : noe == "taghe"
                                        ? quntityTextTaghe[amin] = value
                                        : noe == "metraj"
                                            ? quntityTextMetraj[amin] = value
                                            : noe == "centi"
                                                ? stringCenti = value.toString()
                                                : quntityTextAdl[amin] = value;
                                if (stringCenti != null) {
                                  if (stringCenti.length <= 1) {
                                    quntityTextCenti[amin] = "0$stringCenti";
                                  } else {
                                    quntityTextCenti[amin] = "$stringCenti";
                                  }
                                }
                              },
                            ),
                          ),
                        ],
                      );
                    }

                    return Column(
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  // textDirection: TextDirection.rtl,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(getName(ProductOfCartPage, index),
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontFamily: "iranSansMobile",
                                            fontSize: 14.0)),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 0.0),
                                          child: Text(
                                            getShowPrice(ProductOfCartPage, index),
                                            textDirection: TextDirection.rtl,
                                            style: TextStyle(
                                              fontFamily: "iranSansMobile",
                                              color: Color(0xffc80058),
                                              fontSize: 13.0,
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "  قیمت",
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                            color: Color(0xffc80058),
                                            fontFamily: "iranYekan",
                                            fontSize: 13.0,
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 0.0),
                                          child: Text(
                                            getNoeErae(ProductOfCartPage, index)
                                                    .contains("تعدادی")
                                                ? getTedadTedadi(
                                                    ProductOfCartPage, index)
                                                : totalQuantity(
                                                        ProductOfCartPage,
                                                        index)
                                                    .toString(),
                                            textDirection: TextDirection.rtl,
                                            style: TextStyle(
                                              fontFamily: "iranSansMobile",
                                              // color: Color(0xffc80058),
                                              fontSize: 13.0,
                                            ),
                                          ),
                                        ),
                                        Text(
                                          getNoeErae(ProductOfCartPage, index)
                                                  .contains("تعدادی")
                                              ? "تعداد کل"
                                              : "  متراژکل",
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                            // color: Color(0xffc80058),
                                            fontFamily: "iranYekan",
                                            fontSize: 13.0,
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    getNoeErae(ProductOfCartPage, index)
                                            .contains("طاقه")
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              InkWell(
                                                onTap: () {
                                                  reload();
                                                  quntityTextTaghe[index] = 0;
                                                  indexTaghe = index;
                                                  _tagheOfCupertino = true;
                                                  _adlOfCupertino = false;
                                                  _metrajOfCupertino = false;
                                                  _tedadOfCupertino = false;
                                                  amin = index;
                                                  showModalBottomSheet(
                                                    context: context,
                                                    builder:
                                                        (BuildContext builder) {
                                                      return Container(
                                                        height: MediaQuery.of(
                                                                    context)
                                                                .copyWith()
                                                                .size
                                                                .height /
                                                            3,
                                                        child: quntityOfProduct(
                                                            "taghe"),
                                                      );
                                                    },
                                                  );
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      left: 0.0),
                                                  alignment: Alignment.center,
                                                  width: 40.0,
                                                  height: 23.0,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(4),
                                                      ),
                                                      border: Border.all(
                                                        color:
                                                            Color(0xff707070),
                                                      )),
                                                  child: Text(
                                                    "${quntityTextTaghe[index]}",
                                                    style: TextStyle(
                                                      color: Color(0xff585858),
                                                      fontFamily:
                                                          "iranSansMobile",
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                "تعداد طاقه",
                                                style: TextStyle(
                                                  color: Color(0xff585858),
                                                  fontFamily: "iranYekan",
                                                  fontSize: 13.0,
                                                ),
                                              )
                                            ],
                                          )
                                        : SizedBox(
                                            height: 0,
                                          ),
                                    getNoeErae(ProductOfCartPage, index)
                                            .contains("عدل")
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8.0),
                                                child: InkWell(
                                                  onTap: () {
                                                    reload();
                                                    quntityTextAdl[index] = 0;
                                                    indexAdl = index;
                                                    amin = index;
                                                    _tagheOfCupertino = false;
                                                    _adlOfCupertino = true;
                                                    _metrajOfCupertino = false;
                                                    _tedadOfCupertino = false;
                                                    showModalBottomSheet(
                                                      context: context,
                                                      builder: (BuildContext
                                                          builder) {
                                                        return Container(
                                                          height: MediaQuery.of(
                                                                      context)
                                                                  .copyWith()
                                                                  .size
                                                                  .height /
                                                              3,
                                                          child:
                                                              quntityOfProduct(
                                                                  "adl"),
                                                        );
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 0.0),
                                                    alignment: Alignment.center,
                                                    width: 40.0,
                                                    height: 23.0,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(4),
                                                        ),
                                                        border: Border.all(
                                                          color:
                                                              Color(0xff707070),
                                                        )),
                                                    child: Text(
                                                      "${quntityTextAdl[index]}",
                                                      style: TextStyle(
                                                        color:
                                                            Color(0xff585858),
                                                        fontFamily:
                                                            "iranSansMobile",
                                                        fontSize: 16.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                "تعداد عدل",
                                                style: TextStyle(
                                                  color: Color(0xff585858),
                                                  fontFamily: "iranYekan",
                                                  fontSize: 13.0,
                                                ),
                                              )
                                            ],
                                          )
                                        : SizedBox(
                                            height: 0,
                                          ),
                                    getNoeErae(ProductOfCartPage, index)
                                            .contains("متراژی")
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8.0),
                                                child: InkWell(
                                                  onTap: () {
                                                    quntityTextMetraj[index] =
                                                        0;
                                                    quntityTextCenti[index] =
                                                        "0";
                                                    reload();

                                                    _metrajOfCupertino = true;
                                                    _tagheOfCupertino = false;
                                                    _adlOfCupertino = false;
                                                    _tedadOfCupertino = false;
                                                    amin = index;
                                                    showModalBottomSheet(
                                                      context: context,
                                                      builder: (BuildContext
                                                          builder) {
                                                        return Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                height: MediaQuery.of(
                                                                            context)
                                                                        .copyWith()
                                                                        .size
                                                                        .height /
                                                                    3,
                                                                child:
                                                                    quntityOfProduct(
                                                                        "metraj"),
                                                              ),
                                                            ),
                                                            Expanded(
                                                              child: Container(
                                                                height: MediaQuery.of(
                                                                            context)
                                                                        .copyWith()
                                                                        .size
                                                                        .height /
                                                                    3,
                                                                child:
                                                                    quntityOfProduct(
                                                                        "centi"),
                                                              ),
                                                            ),
                                                          ],
                                                        );
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 0.0),
                                                    alignment: Alignment.center,
                                                    width: 45.0,
                                                    height: 23.0,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(4),
                                                        ),
                                                        border: Border.all(
                                                          color:
                                                              Color(0xff707070),
                                                        )),
                                                    child: Text(
                                                      "${quntityTextMetraj[index]}" +
                                                          "." +
                                                          "${quntityTextCenti[index]}",
                                                      style: TextStyle(
                                                        color:
                                                            Color(0xff585858),
                                                        fontFamily:
                                                            "iranSansMobile",
                                                        fontSize: 16.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                "متراژ",
                                                style: TextStyle(
                                                  color: Color(0xff585858),
                                                  fontFamily: "iranYekan",
                                                  fontSize: 13.0,
                                                ),
                                              )
                                            ],
                                          )
                                        : SizedBox(
                                            height: 0,
                                          ),
                                    getNoeErae(ProductOfCartPage, index)
                                            .contains("تعدادی")
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8.0),
                                                child: InkWell(
                                                  onTap: () {
                                                    quntityTextTedadi[index] =
                                                        0;
                                                    reload();

                                                    _metrajOfCupertino = false;
                                                    _tagheOfCupertino = false;
                                                    _adlOfCupertino = false;
                                                    _tedadOfCupertino = true;
                                                    amin = index;
                                                    showModalBottomSheet(
                                                      context: context,
                                                      builder: (BuildContext
                                                          builder) {
                                                        return Row(
                                                          children: <Widget>[
                                                            Expanded(
                                                              child: Container(
                                                                height: MediaQuery.of(
                                                                            context)
                                                                        .copyWith()
                                                                        .size
                                                                        .height /
                                                                    3,
                                                                child:
                                                                    quntityOfProduct(
                                                                        "tedadi"),
                                                              ),
                                                            ),
                                                          ],
                                                        );
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 0.0),
                                                    alignment: Alignment.center,
                                                    width: 45.0,
                                                    height: 23.0,
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(4),
                                                        ),
                                                        border: Border.all(
                                                          color:
                                                              Color(0xff707070),
                                                        )),
                                                    child: Text(
                                                      "${quntityTextTedadi[index]}",
                                                      style: TextStyle(
                                                        color:
                                                            Color(0xff585858),
                                                        fontFamily:
                                                            "iranSansMobile",
                                                        fontSize: 16.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                "تعداد",
                                                style: TextStyle(
                                                  color: Color(0xff585858),
                                                  fontFamily: "iranYekan",
                                                  fontSize: 13.0,
                                                ),
                                              )
                                            ],
                                          )
                                        : SizedBox(
                                            height: 0,
                                          ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Stack(
                                  alignment: Alignment.centerRight,
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        // color: Colors.red,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(19.0)),
                                      ),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(9.0),
                                        ),
                                        child: FadeInImage(
                                          width: 90,
                                          height: 90.0,
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                            getImage(ProductOfCartPage, index),
                                          ),
                                          placeholder: AssetImage(
                                              'assets/imgs/zero_to_infinity_by_volorf.gif'),
                                        ),
                                      ),
                                      width: 90.0,
                                      height: 90.0,
                                    ),
                                    String_to_int(getRegularPrice(
                                                ProductOfCartPage, index)) >
                                            String_to_int(getPrice(
                                                ProductOfCartPage, index))
                                        ? Positioned(
                                            top: 0,
                                            right: 0,
                                            child: Container(
                                              alignment: Alignment.center,
                                              width: 27.0,
                                              height: 27.0,
                                              child: Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 0.0),
                                                  child: Text(
                                                    "%" +
                                                        offer(
                                                                String_to_int(
                                                                    getRegularPrice(
                                                                        ProductOfCartPage,
                                                                        index)),
                                                                String_to_int(
                                                                    getPrice(
                                                                        ProductOfCartPage,
                                                                        index)))
                                                            .toString(),
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xfffafafa),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 10.0,
                                                        fontFamily:
                                                            'IransansBold'),
                                                  ),
                                                ),
                                              ),
                                              decoration: BoxDecoration(
                                                color: Color(0xffc80058),
                                                borderRadius: BorderRadius.only(
                                                  topRight:
                                                      Radius.circular(9.0),
                                                  bottomLeft:
                                                      Radius.circular(9.0),
                                                ),
                                              ),
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    ProductOfCartPage.removeAt(index);
                                    quntityTextAdl.remove(index);
                                    quntityTextTaghe.remove(index);
                                    quntityTextMetraj.remove(index);
                                    quntityTextTedadi.remove(index);
                                    quntityTextCenti.remove(index);
                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.close,
                                    color: Color(0xff707070),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          color: Colors.grey,
                          endIndent: 30.0,
                          indent: 30.0,
                        )
                      ],
                    );
                  },
                ),
        ),
        Positioned(
          bottom: 0.0,
          right: 20.0,
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(),
            height: 51.0,
            child: Row(
              children: <Widget>[
                Text(
                  ProductOfCartPage.length != 0
                      ?toMony(
                         get_price_of_cart(
                              ProductOfCartPage, indexTaghe, indexAdl)
                          .toString()
                      )
                      : "0",
                  style: TextStyle(
                    color: Color(0xff666666),
                    fontFamily: 'iranSansMobile',
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  "جمع کل :",
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    color: Color(0xff666666),
                    fontFamily: 'iranSansD',
                    fontSize: 16.0,
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 0.0,
          left: 0.0,
          child: InkWell(
            onTap: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => PreviewFactor()));
            },
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xffc80058),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16.0),
                    topRight: Radius.circular(16.0)),
              ),
              width: 131.0,
              height: 51.0,
              child: Text(
                "ادامه خرید",
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'iranSansD',
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

// _launchURL(String url) async {
//   if (await canLaunch(url)) {
//     await launch(url);
//   } else {
//     throw 'Could not launch $url';
//   }
// }

int get_price_of_cart(List ProductOfCartPage, [int taghe = 0, int adl = 0]) {
  double sum_of_price_metraj = 0;
  double sum_of_price_taghe = 0;
  double sum_of_price_tedadi = 0;
  double sum_of_price_adl = 0;

  double sizeTaghe = 0;
  double sizeAdl = 0;
  // double sizeMetraj = 0;

  for (int i = 0; i < ProductOfCartPage.length; i++) {
    getNoeErae(ProductOfCartPage, i).contains("عدل")
        ? sizeAdl = double.parse(
            getSizeAdl(ProductOfCartPage, i),
          )
        : sizeAdl;

    getNoeErae(ProductOfCartPage, i).contains("طاقه")
        ? sizeTaghe = double.parse(
            getSizeTaghe(ProductOfCartPage, i),
          )
        : sizeTaghe;

    // getNoeErae(ProductOfCartPage, i).contains("متراژی")
    //     ? sizeMetraj = double.parse(
    //         getSizeMetraj(ProductOfCartPage, i),
    //       )
    //     : sizeMetraj;

    int price = int.parse(
      getPrice(ProductOfCartPage, i)
          .replaceAll("ریال", "")
          .replaceAll(".", "")
          .replaceAll("بدون قیمت", ""),
    );

    if (getNoeErae(ProductOfCartPage, i).contains("تعدادی")) {
      sum_of_price_tedadi += price * quntityTextTedadi[i];
    }

    sum_of_price_metraj +=
        price * (quntityTextMetraj[i] + (int.parse(quntityTextCenti[i]) / 100));
    sum_of_price_taghe += price * quntityTextTaghe[i] * sizeTaghe;
    sum_of_price_adl += price * quntityTextAdl[i] * sizeAdl;
  }

  return sum_of_price_metraj.round() +
      sum_of_price_taghe.round() +
      sum_of_price_adl.round() +
      sum_of_price_tedadi.round();
}

int get_offer_price_of_cart(List ProductOfCartPage,
    [int taghe = 0, int adl = 0]) {
  double sum_of_price_metraj = 0;
  double sum_of_price_taghe = 0;
  double sum_of_price_adl = 0;
  double sum_of_price_tedadi = 0;

  double sizeTaghe = 0;
  double sizeAdl = 0;
  int tedadtedadi = 0;
  // double sizeMetraj = 0;
  for (int i = 0; i < ProductOfCartPage.length; i++) {
    getNoeErae(ProductOfCartPage, i).contains("عدل")
        ? sizeAdl = double.parse(
            getSizeAdl(ProductOfCartPage, i),
          )
        : sizeAdl;
    getNoeErae(ProductOfCartPage, i).contains("تعدادی")
        ? tedadtedadi = quntityTextTedadi[i]
        : tedadtedadi;
    // getNoeErae(ProductOfCartPage, i).contains("متراژی")
    //     ? sizeMetraj = double.parse(
    //         getSizeMetraj(ProductOfCartPage, i),
    //       )
    //     : sizeMetraj;

    getNoeErae(ProductOfCartPage, i).contains("طاقه")
        ? sizeTaghe = double.parse(
            getSizeTaghe(ProductOfCartPage, i),
          )
        : sizeTaghe;

    int price = int.parse(
          getRegularPrice(ProductOfCartPage, i)
              .replaceAll("ریال", "")
              .replaceAll(".", "")
              .replaceAll("بدون قیمت", ""),
        ) -
        int.parse(
          getPrice(ProductOfCartPage, i)
              .replaceAll("ریال", "")
              .replaceAll(".", "")
              .replaceAll("بدون قیمت", ""),
        );

    sum_of_price_metraj += price * quntityTextMetraj[i] +
        (double.parse(quntityTextCenti[i]) / 100);
    sum_of_price_tedadi += price * quntityTextTedadi[i];
    sum_of_price_taghe += price * quntityTextTaghe[i] * sizeTaghe;
    sum_of_price_adl += price * quntityTextAdl[i] * sizeAdl;
  }

  return sum_of_price_metraj.round() +
      sum_of_price_taghe.round() +
      sum_of_price_adl.round() +
      sum_of_price_tedadi.round();
}

double totalSizeOfCartProduct(List _products, int index) {
  double sizeTaghe = 0;
  double sizeAdl = 0;
  // double sizeMetraj = 0;

  double totalQuantityOfAdl;
  double totalQuantityOfTaghe;
  double totalQuantityOfMetraj;

  getNoeErae(_products, index).contains("عدل")
      ? sizeAdl = double.parse(
          getSizeAdl(_products, index),
        )
      : sizeAdl;

  // getNoeErae(_products, index).contains("متراژی")
  //     ? sizeMetraj = double.parse(
  //         getSizeMetraj(_products, index),
  //       )
  //     : sizeMetraj;

  getNoeErae(_products, index).contains("طاقه")
      ? sizeTaghe = double.parse(
          getSizeTaghe(_products, index),
        )
      : sizeTaghe;

  totalQuantityOfAdl = sizeAdl * quntityTextAdl[index];
  totalQuantityOfMetraj =
      (double.parse(quntityTextCenti[index]) / 100) + quntityTextMetraj[index];
  totalQuantityOfTaghe = sizeTaghe * quntityTextTaghe[index];

  return totalQuantityOfMetraj +
      totalQuantityOfTaghe +
      totalQuantityOfAdl +
      quntityTextTedadi[index];
}

totalQuantity(List product, int index) {
  double _sizeAdl = getNoeErae(product, index).contains("عدل")
      ? double.parse(getSizeAdl(product, index)) *
          double.parse(getTedadAdl(product, index))
      : 0;
  double _sizeTaghe = getNoeErae(product, index).contains("طاقه")
      ? double.parse(getSizeTaghe(product, index)) *
          double.parse(getTedadTaghe(product, index))
      : 0;
  double _sizeMetraj = getNoeErae(product, index).contains("متراژ")
      ? double.parse(getTedadMetraj(product, index))
      : 0;

  return (_sizeAdl + _sizeMetraj + _sizeTaghe).toString();
}
