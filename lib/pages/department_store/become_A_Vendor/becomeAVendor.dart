import 'dart:convert';
import 'dart:io';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/setting/setting.dart';
import 'package:pardemarketapp/services/becomeAVendor.dart';
import 'package:pardemarketapp/services/get_Sotre_Info.dart';
import 'package:pardemarketapp/services/get_Token_forUser.dart';
import 'package:pardemarketapp/services/login_services.dart';
import 'package:path/path.dart';
import 'package:http_parser/http_parser.dart';
import 'package:async/async.dart';
import '../../../services/get_Sotre_Info.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pardemarketapp/pages/department_store/MyStore/myStore.dart';
import '../../../services/get_Token.dart';
import '../../../function/snackBar.dart';
import '../BottomNavBar.dart';

int countVendorMyStor = 0;

class BecomeAVendor extends StatefulWidget {
  @override
  _BecomeAVendorState createState() => _BecomeAVendorState();
}

class _BecomeAVendorState extends State<BecomeAVendor> {
  TextEditingController namOfStore = new TextEditingController();
  TextEditingController phonOfStore = new TextEditingController();
  TextEditingController firsNameShopper = new TextEditingController();
  TextEditingController cityOfStore = new TextEditingController();
  TextEditingController streetOfStore = new TextEditingController();
  TextEditingController acNumber = new TextEditingController();
  TextEditingController acName = new TextEditingController();

  File _image;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DbHelper dbHelper = new DbHelper();

  List<MyList> myList;

  MyList mylist;
  int count = 0;
  int _countForFirstClick = 0;

  @override
  void initState() {
    if (removehesab) {
      namOfStore.text = "";
      phonOfStore.text = "";
      firsNameShopper.text = "";
      cityOfStore.text = "";
      streetOfStore.text = "";
      acName.text = "";
      acNumber.text = "";
    }
    countVendorMyStor = 0;
    print("\n\n     ${namOfStore.text} \n\n\n");
    print("amin");
    print("\n\n     ${cityOfStore.text} \n\n\n");
    super.initState();
    getData();

    if (!store_Info.isEmpty) {
      namOfStore.text = storName(store_Info);
      phonOfStore.text = storPhone(store_Info);
      firsNameShopper.text = storFirstName(store_Info);
      cityOfStore.text = storCity(store_Info);
      streetOfStore.text = storStreet(store_Info);
      if (storePayment(store_Info).contains("ac_number")) {
        acName.text = storeAcName(store_Info);
        acNumber.text = storAcNumber(store_Info);
      }
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxHeight: 1800, maxWidth: 1200);
    setState(() {
      _image = image;
      isImageLoaded = true;
    });
    if (image != null) {
      _scaffoldKey.currentState.showSnackBar(loadingSnackBarUploadImage());
    }

    Upload(_image).then((value) {
      print("\n\n         value of upload     $value     \n\n\n");
      if (value != null) {
        _scaffoldKey.currentState.hideCurrentSnackBar();
        showErrorDialog("تصویر آپلود شد", _scaffoldKey);
      }
    });
  }

  Future Upload(File imageFile) async {
    var stream =
        new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    var length = await imageFile.length();
    String apiUrl = "https://pardemarket.com/wp-json/wp/v2/media";

    var uri = Uri.parse(apiUrl);
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request = new http.MultipartRequest("POST", uri);
    request.headers.addAll(headers);
    var multipartFile = new http.MultipartFile('file', stream, length,
        contentType: new MediaType('application', 'x-tar'),
        filename: basename(imageFile.path));

    request.files.add(multipartFile);
    var response = await request.send();
    String respStr = await response.stream.bytesToString();

    Map aaa = json.decode(respStr);
    idOfImage = aaa["id"];
    return aaa;
  }

  int idOfImage;

  bool isImageLoaded = false;

  String nameOfStore,
      phoneOfStore,
      firstName,
      city,
      address,
      acNamestr,
      acNumberstr;
  int id;
  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xffd5d5d5),
      body: Center(
        child: Container(
          margin: EdgeInsets.fromLTRB(20.0, 45.0, 20.0, 19.0),
          decoration: BoxDecoration(
            color: Color(0xfff5f5f5),
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
            boxShadow: [
              new BoxShadow(
                color: Color(0xffA8A8A8),
                blurRadius: 21.0,
                offset: Offset(0, 3),
              ),
            ],
          ),
          width: MediaQuery.of(context).size.width - 30,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                GestureDetector(
                  onTap: () {
                    return getImage();
                  },
                  child: Container(
                    padding: _image != null || storImage(store_Info) != null
                        ? EdgeInsets.all(0.0)
                        : EdgeInsets.all(40.0),
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      color: Color(0xffe0e0e0),
                      border: Border.all(
                        width: 2.0,
                        color: Color(0xaac80058),
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: _image != null || storImage(store_Info) != null
                        ? ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(500.0)),
                            child: _image == null
                                ? Image.network(
                                    storImage(store_Info),
                                    fit: BoxFit.fill,
                                  )
                                : Image.file(
                                    _image,
                                    fit: BoxFit.fill,
                                  ),
                          )
                        : SvgPicture.asset(
                            "assets/imgs/add_image.svg",
                          ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(35.0)),
                    elevation: 2.0,
                    child: TextField(
                      controller: namOfStore,
                      onChanged: (value) {
                        nameOfStore = value;
                        print(namOfStore.text);
                      },
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'iranSansMobile',
                      ),
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        border: InputBorder.none,
                        hintText: "اسم مغازه",
                        hintStyle: TextStyle(
                          fontFamily: 'iranSansMobile',
                          fontSize: 15.0,
                          color: Color(0xff999999),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(35.0)),
                    elevation: 2.0,
                    child: TextField(
                      controller: phonOfStore,
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        phoneOfStore = value;
                      },
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'iranSansMobile',
                      ),
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        border: InputBorder.none,
                        hintText: "تلفن مغازه",
                        hintStyle: TextStyle(
                          fontFamily: 'iranSansMobile',
                          color: Color(0xff999999),
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(35.0)),
                    elevation: 2.0,
                    child: TextField(
                      controller: firsNameShopper,
                      onChanged: (value) {
                        firstName = value;
                      },
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'iranSansMobile',
                      ),
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        border: InputBorder.none,
                        hintText: "نام و نام خانوادگی فروشنده",
                        hintStyle: TextStyle(
                          fontFamily: 'iranSansMobile',
                          color: Color(0xff999999),
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(35.0)),
                    elevation: 2.0,
                    child: TextField(
                      controller: cityOfStore,
                      onChanged: (value) {
                        city = value;
                      },
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'iranSansMobile',
                      ),
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                          border: InputBorder.none,
                          hintText: "شهر",
                          hintStyle: TextStyle(
                            fontFamily: 'iranSansMobile',
                            color: Color(0xff999999),
                            fontSize: 15.0,
                          )),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(35.0)),
                    elevation: 2.0,
                    child: TextField(
                      controller: acNumber,
                      onChanged: (value) {
                        acNumberstr = value;
                      },
                      // textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'iranSansMobile',
                      ),
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                        border: InputBorder.none,
                        hintText: "شماره شبا",
                        hintStyle: TextStyle(
                          fontFamily: 'iranSansMobile',
                          color: Color(0xff999999),
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(35.0)),
                    elevation: 2.0,
                    child: TextField(
                      controller: acName,
                      onChanged: (value) {
                        acNamestr = value;
                        print(acNamestr);
                        print(acName.text);
                      },
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'iranSansMobile',
                      ),
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                          border: InputBorder.none,
                          hintText: "نام دارنده حساب",
                          hintStyle: TextStyle(
                            fontFamily: 'iranSansMobile',
                            color: Color(0xff999999),
                            fontSize: 15.0,
                          )),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                  child: Material(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                    elevation: 2.0,
                    child: TextField(
                      controller: streetOfStore,
                      onChanged: (value) {
                        address = value;
                      },
                      maxLines: 4,
                      minLines: 3,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'iranSansMobile',
                      ),
                      textAlign: TextAlign.right,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                          border: InputBorder.none,
                          hintText: "ادرس (حداقل 15 کاراکتر)",
                          hintStyle: TextStyle(
                            fontFamily: 'iranSansMobile',
                            fontSize: 15.0,
                            color: Color(0xff999999),
                          )),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: InkWell(
                    onTap: () {
                      if (store_Info.isEmpty) {
                        if (nameOfStore == null) {
                          _showErrorDialog(
                              "فیلد اسم مغازه را پر نکرده اید", _scaffoldKey);
                        } else if (phoneOfStore == null) {
                          _showErrorDialog(
                              "فیلد تلفن مغازه را پر نکرده اید", _scaffoldKey);
                        } else if (address == null) {
                          _showErrorDialog(
                              "فیلد آدرس را پر نکرده اید", _scaffoldKey);
                        } else if (city == null) {
                          _showErrorDialog(
                              "فیلد شهر را پر نکرده اید", _scaffoldKey);
                        } else if (nameOfStore.length >= 3 &&
                            phoneOfStore.length >= 8 &&
                            address.length >= 15 &&
                            city.length >= 2) {
                          // if (_countForFirstClick == 0) {
                          print("${this.myList[0].userId}");
                          _countForFirstClick++;
                          _scaffoldKey.currentState
                              .showSnackBar(loading_SnackBar());
                          becomeAVendor(

                                  id_User == null
                                      ? int.parse(this.myList[0].userId)
                                      : id_User,
                                  idOfImage,
                                  nameOfStore,
                                  phoneOfStore,
                                  firstName,
                                  city,
                                  address,
                                  acNamestr,
                                  acNumberstr)
                              .then((onValue) {
                            print("121212122       $onValue");
                            get_token_for_user(this.myList[0].phoneOfUser)
                                .then((onValue) {
                              print("        99999999999        $onValue");
                              if (onValue.toString().contains("[jwt_auth]") ==
                                  false) {
                                print(
                                    "09350934859084390859034890589034859890349580934850934059839058903485903890583945");
                                this.mylist.boolBecomeAVendor = "true";
                                this.mylist.nameOfUser = firstName;
                                // this.mylist.notification30 = firstName;
                                dbHelper.update(mylist);
                                _image = null;

                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MyStore(),
                                  ),
                                );
                                _countForFirstClick--;
                              } else {
                                _showErrorDialog(
                                    "مشگلی رخ داده است", _scaffoldKey);
                              }
                            });
                          }).catchError((e) {
                            _showErrorDialog("مقادیر به درستی وارد نشده اند. ",
                                _scaffoldKey);
                            print("$e");
                          });
                          // }
                        } else {
                          if (nameOfStore.length < 3) {
                            _showErrorDialog(
                                "اسم مغازه باید بزرگتر از 2 کاراکتر باشد",
                                _scaffoldKey);
                          } else if (phoneOfStore.length < 8) {
                            _showErrorDialog(
                                "شماره مغازه درست وارد نشده است", _scaffoldKey);
                          } else if (address.length < 15) {
                            _showErrorDialog(
                                "آدرس درست وارد نشده است", _scaffoldKey);
                          } else if (city.length <= 2) {
                            _showErrorDialog(
                                "شهر درست وارد نشده است", _scaffoldKey);
                          }
                        }
                      } else {
                        print("\ndddddd\ndddd\nddddd\n$_countForFirstClick");
                        if (namOfStore.text == null ||
                            phonOfStore.text == null ||
                            streetOfStore.text == null ||
                            cityOfStore.text == null) {
                          _showErrorDialog(
                              "تمام مقادیر را وارد کنید", _scaffoldKey);
                          print("1111");
                        } else if (namOfStore.text.length >= 3 &&
                            phonOfStore.text.length >= 8 &&
                            streetOfStore.text.length >= 15 &&
                            cityOfStore.text.length >= 3) {
                          if (_countForFirstClick == 0) {
                            countVendorMyStor = 1;
                            _countForFirstClick++;
                            _scaffoldKey.currentState
                                .showSnackBar(loading_SnackBar());
                            becomeAVendor(
                                    id_User == null
                                        ? int.parse(this.myList[0].userId)
                                        : id_User,
                                    idOfImage,
                                    namOfStore.text,
                                    phonOfStore.text,
                                    firsNameShopper.text,
                                    cityOfStore.text,
                                    streetOfStore.text,
                                    acName.text,
                                    acNumber.text)
                                .then((onValue) {
                              print("121212122       $onValue");
                              get_token_for_user(this.myList[0].phoneOfUser)
                                  .then((onValue) {
                                print("        99999999999        $onValue");
                                if (onValue.toString().contains("[jwt_auth]") ==
                                    false) {
                                  print(
                                      "09350934859084390859034890589034859890349580934850934059839058903485903890583945\n\n\n     ${firsNameShopper.text}");
                                  this.mylist.boolBecomeAVendor = "true";
                                  this.mylist.nameOfUser = firsNameShopper.text;
                                  // this.mylist.notification30 =
                                  //     firsNameOfStore.text;
                                  dbHelper.update(mylist);
                                  _image = null;

                                  Navigator.pop(
                                    context,
                                  );
                                  _countForFirstClick--;
                                } else {
                                  _showErrorDialog(
                                      "مشگلی رخ داده است", _scaffoldKey);
                                }
                              });
                            }).catchError((e) {
                              _showErrorDialog(
                                  "مقادیر به درستی وارد نشده اند. ",
                                  _scaffoldKey);
                              print("$e");
                            });
                          }
                        } else {
                          _showErrorDialog(
                              "مقادیر به درستی وارد نشده اند. ", _scaffoldKey);
                        }
                      }
                    },
                    child: Container(
                      margin: const EdgeInsets.only(right: 13.0, left: 13.0),
                      width: 800.0,
                      height: 50.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        color: Color(0xffc80058),
                      ),
                      child: Center(
                        child: Text(
                          "ثبت",
                          style: TextStyle(
                            fontSize: 21.0,
                            color: Color(0xfffafafa),
                            fontFamily: "iranSansMobile",
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
          mylist = myList[0];
        });
      });
    });
  }
}

_showErrorDialog(String str, GlobalKey<ScaffoldState> _scaffoldKey) {
  _scaffoldKey.currentState.showSnackBar(
    new SnackBar(
      content: Text(
        str,
        style: TextStyle(
          fontFamily: "IransansLight",
          fontSize: 16,
          color: Color(0xffffffff),
        ),
        textDirection: TextDirection.rtl,
      ),
      backgroundColor: Color(0xffc80058),
    ),
  );
}
