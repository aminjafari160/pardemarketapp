import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/term/termInSetting.dart';
import 'package:pardemarketapp/services/delet_user.dart';

class Setting extends StatelessWidget {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Setting1(this._scaffoldKey),
    );
  }
}

class Setting1 extends StatefulWidget {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Setting1(this._scaffoldKey);
  @override
  _SettingState createState() => _SettingState(_scaffoldKey);
}

String city = "تهران";

class _SettingState extends State<Setting1> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _SettingState(this._scaffoldKey);
  int count = 0;
  DbHelper dbHelper = new DbHelper();
  List<MyList> myList;
  MyList mylist;
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  setStat() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }
    double width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 42.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: mylist == null
                    ? Text("")
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            this.myList[0].boolBecomeAVendor == "false"
                                ? "شما هنوز ثبت نام نکرده اید "
                                : this.mylist.nameOfUser,
                            style: TextStyle(
                                color: Color(0xfff5f5f5),
                                fontSize: 19.0,
                                fontFamily: 'iranSansMobile'),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            this.myList[0].phoneOfUser == "false"
                                ? ""
                                : this.myList[0].phoneOfUser,
                            style: TextStyle(
                                color: Color(0xfff5f5f5),
                                fontSize: 19.0,
                                fontFamily: 'iranSansMobile'),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                height: 98.0,
                width: width / 1.1,
                decoration: BoxDecoration(
                  color: Color(0xffc80058),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  gradient: LinearGradient(colors: [
                    Color(0xff530050),
                    Color(0xffc80058),
                  ], begin: Alignment.centerRight, end: Alignment.centerLeft),
                ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: 44.0,
              ),
              // Container(
              //   child: Padding(
              //     padding: const EdgeInsets.all(8.0),
              //     child: Row(
              //       mainAxisAlignment: MainAxisAlignment.end,
              //       children: <Widget>[
              //         Text(
              //           'حالت شب',
              //           style: TextStyle(
              //               color: Color(0xff000000),
              //               fontSize: 17.0,
              //               fontFamily: 'iransans'),
              //           textAlign: TextAlign.right,
              //         ),
              //         SizedBox(
              //           width: 10,
              //         ),
              //         Image.asset(
              //           "assets/imgs/night-mode.png",
              //           height: 30,
              //           width: 30,
              //         ),
              //       ],
              //     ),
              //   ),
              //   height: 60.0,
              //   width: width / 1.1,
              //   decoration: BoxDecoration(
              //       boxShadow: [
              //         BoxShadow(
              //           blurRadius: 16.0,
              //           color: Color(0xffd1d1d1),
              //           offset: Offset(0.0, 3.0),
              //         ),
              //       ],
              //       color: Color(0xfff5f5f5),
              //       borderRadius: BorderRadius.all(Radius.circular(20.0))),
              // ),
              // SizedBox(
              //   height: 19.0,
              // ),
              InkWell(
                onTap: () {
                  select_City(setStat);
                },
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          city,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17.0,
                              fontFamily: 'iransansmobile'),
                          textAlign: TextAlign.right,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          ' : شهر',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 17.0,
                              fontFamily: 'iransansmobile'),
                          textAlign: TextAlign.right,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Image.asset(
                          "assets/imgs/cityscape.png",
                          height: 30,
                          width: 30,
                        ),
                      ],
                    ),
                  ),
                  height: 60.0,
                  width: width / 1.1,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 16.0,
                          color: Color(0xffd1d1d1),
                          offset: Offset(0.0, 3.0),
                        ),
                      ],
                      color: Color(0xfff5f5f5),
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                ),
              ),
              SizedBox(height: 15.0),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TermInSetting(),
                    ),
                  );
                },
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'قوانین و توضیحات',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 17.0,
                              fontFamily: 'iransansmobile'),
                          textAlign: TextAlign.right,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(Icons.view_headline)
                        // Image.asset(
                        //   "assets/imgs/cityscape.png",
                        //   height: 30,
                        //   width: 30,
                        // ),
                      ],
                    ),
                  ),
                  height: 60.0,
                  width: width / 1.1,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 16.0,
                          color: Color(0xffd1d1d1),
                          offset: Offset(0.0, 3.0),
                        ),
                      ],
                      color: Color(0xfff5f5f5),
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                ),
              ),
              SizedBox(
                height: 19.0,
              ),
              InkWell(
                onTap: () {
                  if (this.myList[0].userId != "false") {
                    showCupertinoModalPopup(
                      context: context,
                      builder: (BuildContext context) => CupertinoActionSheet(
                        title: const Text(
                          'آیا میخواهید از حسابتان خارج شوید؟',
                          style: TextStyle(
                              fontFamily: "iranSansD",
                              color: Color(0xffc80058),
                              fontSize: 16.0),
                        ),
                        actions: <Widget>[
                          CupertinoActionSheetAction(
                            child: const Text(
                              'بلی',
                              style: TextStyle(
                                  fontFamily: "iranSansD",
                                  color: Colors.black,
                                  fontSize: 16.0),
                            ),
                            isDefaultAction: true,
                            onPressed: () {
                              removehesab = true;
                              Navigator.pop(context);
                              _scaffoldKey.currentState
                                  .showSnackBar(loading_SnackBar());
                              delete_User(this.myList[0].userId)
                                  .whenComplete(() {
                                mylist.boolBecomeAVendor = "false";
                                mylist.nameOfUser = "false";
                                mylist.boolCustomer = "false";
                                mylist.phoneOfUser = "false";
                                mylist.userId = "false";
                                dbHelper.update(mylist);

                                _scaffoldKey.currentState.hideCurrentSnackBar();
                                setState(() {});
                                // Navigator.pop(context);
                              });
                            },
                          ),
                        ],
                        cancelButton: CupertinoActionSheetAction(
                          child: const Text(
                            'انصراف',
                            style: TextStyle(
                                fontFamily: "iranSansD",
                                color: Colors.black,
                                fontSize: 16.0),
                          ),
                          isDefaultAction: true,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    );
                  } else {
                    return showCupertinoModalPopup(
                      context: context,
                      builder: (BuildContext context) => CupertinoActionSheet(
                          title: const Text(
                            'شما هنوز ثبت نام نکرده اید',
                            style: TextStyle(
                                fontFamily: "iranSansD",
                                color: Color(0xffc80058),
                                fontSize: 16.0),
                          ),
                          actions: <Widget>[],
                          cancelButton: CupertinoActionSheetAction(
                            child: const Text(
                              'انصراف',
                              style: TextStyle(
                                  fontFamily: "iranSansD",
                                  color: Colors.black,
                                  fontSize: 16.0),
                            ),
                            isDefaultAction: true,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          )),
                    );
                  }
                },
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'حذف حساب کاربری',
                          style: TextStyle(
                            color: Color(0xffc80050),
                            fontSize: 17.0,
                            fontFamily: 'iransansmobile',
                          ),
                          textAlign: TextAlign.right,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Image.asset(
                          "assets/imgs/logout.png",
                          height: 30,
                          width: 30,
                        ),
                      ],
                    ),
                  ),
                  height: 60.0,
                  width: width / 1.1,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 16.0,
                          color: Color(0xffd1d1d1),
                          offset: Offset(0.0, 3.0),
                        ),
                      ],
                      color: Color(0xfff5f5f5),
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                ),
              ),
            ],
          ),
          Text(''),
        ],
      ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          mylist = myList[0];
          count = count;
        });
      });
    });
  }

  bool tehran = true, mashhad = false, esfahan = false;
  select_City(Function setstate) {
    return showModalBottomSheet(
      context: context,
      builder: (BuildContext builder) {
        return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            child: listOfCity(setstate));
      },
    );
  }
}

int indexOfCityInCupertino;
Widget listOfCity(Function setstate) {
  return Container(
    color: Color(0xffe0e0e0),
    child: CupertinoPicker.builder(
      scrollController: FixedExtentScrollController(
          initialItem:
              indexOfCityInCupertino == null ? 7 : indexOfCityInCupertino),
      childCount: nameOfCity.length,
      backgroundColor: Color(0xffe0e0e0),
      itemExtent: 25,
      itemBuilder: (BuildContext context, index) {
        return Center(
          child: Text(
            nameOfCity[index],
            style: TextStyle(
              color: Color(0xff333333),
              fontFamily: "iranSansMobile",
              fontSize: 16.0,
            ),
          ),
        );
      },
      onSelectedItemChanged: (int value) {
        city = nameOfCity[value];
        indexOfCityInCupertino = value;
        setstate();
      },
    ),
  );
}

List<String> nameOfCity = [
  "آذربایجان شرقی",
  'آذربایجان غربی',
  "اردبیل",
  "اصفهان",
  "البرز",
  "ایلام",
  "بوشهر",
  "تهران",
  "چهارمحال و بختیاری",
  "خراسان جنوبی",
  'خراسان رضوی',
  "خراسان شمالی",
  "خوزستان",
  "زنجان",
  "سمنان",
  "سیستان و بلوچستان",
  "فارس",
  "قزوین",
  "قم",
  "کردستان",
  'کرمان',
  "کرمانشاه",
  "کهگیلویه وبویراحمد",
  'گلستان',
  'گیلان',
  "لرستان",
  'مازندران',
  "مرکزی",
  "هرمزگان",
  "همدان",
  "یزد"
];
bool removehesab = false;
