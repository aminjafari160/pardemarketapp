import 'package:flutter/material.dart';
import 'package:pardemarketapp/pages/department_store/become_A_Vendor/becomeAVendor.dart';

class Term extends StatefulWidget {
  @override
  _TermState createState() => _TermState();
}

class _TermState extends State<Term> {
  @override
  Widget build(BuildContext context) {
    TextStyle title = TextStyle(
      fontFamily: "IranSansMobile",
      fontSize: 18.0,
    );

    TextStyle subtitle = TextStyle(
      fontFamily: "IranSansMobile",
      fontSize: 15.0,
    );

    SizedBox sizebox = SizedBox(
      height: 40.0,
    );

    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: 
        Column(
          children: <Widget>[
            sizebox,
            Text(
              "قوانین و مقررات",
              style: title,
            ),
            sizebox,
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                "شرایط استفاده از خدمات",
                style: title,
                textDirection: TextDirection.rtl,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              """متن پیش رو توضیحی دربارهٔ شرایط استفاده از برنامه پرده مارکت و تمام خدمات و سرویس‌های ارائه شده از طریق آن است.
استفاده از خدمات ما، به این معنی است که با این شرایط موافقت کرده‌اید. لطفاً برای آشنایی با شرایط استفاده از پرده مارکت این سند را مطالعه کنید و چنانچه سؤالی برایتان پیش آمد خوشحال می‌شویم از طریق بخش پیام به پشتیبانی ، با ما در میان بگذارید.""",
              style: subtitle,
              textDirection: TextDirection.rtl,
            ),
            sizebox,
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                "شرایط بسته‌بندی و ارسال محصولات",
                style: title,
                textDirection: TextDirection.rtl,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              """محصولات فروخته شده از طریق اپلیکیشن پرده مارکت ، می‌بایست در قالب یک بسته‌بندی متعارف به انبار پرده مارکت به نشانیِ خیابان مولوی ، پاساژ روحی ، طبقه اول ، پلاک ۲۳۷۴
ارسال گردد. اطلاعات ارسال به مشتری فقط قید شماره‌ فاکتور مربوط ومشخصات  فروشنده که بر روی بسته‌بندی درج شده باشد.
هزینه‌ی حمل محصولات تا انبار پرده مارکت بعهده فروشنده می‌باشد.""",
              style: subtitle,
              textDirection: TextDirection.rtl,
            ),
            sizebox,
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                "شرایط کارمزد اپلیکیشن",
                style: title,
                textDirection: TextDirection.rtl,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              """در حال حاضر کارمزد در نظر گرفته شده جهت خدمات وپشتیبانی اپلیکیشن ۲ درصد می باشد""",
              style: subtitle,
              textDirection: TextDirection.rtl,
            ),
            Text(
              """(درصورت تغییر کارمزد خدمات اپلیکیشن ، اطلاع رسانی از طریق پیامک به فروشندگان محترم انجام خواهد شد)""",
              style: subtitle,
              textDirection: TextDirection.rtl,
            ),
            InkWell(
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BecomeAVendor(),
                  ),
                );
              },
              child: Container(
                padding: EdgeInsets.all(10.0),
                margin: EdgeInsets.all(30.0),
                decoration: BoxDecoration(
                  color: Color(0xffc80058),
                  borderRadius: BorderRadius.all(
                    Radius.circular(30.0),
                  ),
                ),
                child: Text(
                  "با قوانین بالا موافقم",
                  style: TextStyle(
                    fontFamily: "IranSansMobile",
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
