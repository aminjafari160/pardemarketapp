import 'package:flutter/material.dart';
import 'package:pardemarketapp/resource/strings.dart';

class TermInSetting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: SingleChildScrollView(
        child: Column(
          textDirection: TextDirection.rtl,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),

            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  "قوانین کلی",
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  SizedBox(
                    height: 35.0,
                  ),
                  Text(
                    "توضیحات تکمیلی",
                    style: hedder,
                  ),
                  freeSpace,
                  Text(
                    "متن پیش رو توضیحی درباره‌ی شرایط استفاده از برنامه پرده مارکت و تمام خدمات و سرویس‌های ارائه شده از طریق آن است.استفاده از خدمات ما ، به این معنی است که با این شرایط موافقت کرده‌اید. لطفا برای آشنایی با شرایط استفاده از پرده مارکت این سند را مطالعه کنید و چنانچه سوالی برایتان پیش آمد خوشحال می‌شویم از طریق بخش پیام به پشتیبانی ، با ما در میان بگذارید",
                    textDirection: TextDirection.rtl,
                    style: text,
                  ),
                  freeSpace,
                  Text(
                    "ثبت، پردازش و ارسال سفارش",
                    textDirection: TextDirection.rtl,
                    style: hedder,
                  ),
                  freeSpace,
                  Text(
                    "پرده مارکت به عنوان بزرگترین مرکز خرید و فروش آنلاین پرده در ایران ، مفتخر است تا خدمات خرید و فروش پرده را بصورت ۲۴ ساعته و ۷ روز هفته ، به شما مشتریان گرامی ارائه دهد.لذا روند سفارش گیری بصورت شبانه روزی و بدون تعطیلی از طریق اپلیکیشن صورت می‌پذیرد. محصولات درخواستی حداکثر ظرف مدت ۵ روز کاری به دست مشتریان خواهد رسید.",
                    textDirection: TextDirection.rtl,
                    style: text,
                  ),
                  freeSpace,
                  Text(
                    "روش پرداخت",
                    textDirection: TextDirection.rtl,
                    style: hedder,
                  ),
                  freeSpace,
                  Text(
                    "روش پرداخت در پرده مارکت ، در حال حاضر ، فقط به صورت پرداخت آنلاین می‌باشد.هزینه‌ی حمل و نقل محصولات به عهده‌ی مشتری می‌باشد و پس از دریافت محصول و فاکتور رسمی پرده مارکت می‌بایست که صورت پذیرد",
                    textDirection: TextDirection.rtl,
                    style: text,
                  ),
                  freeSpace,
                  Text(
                    "ارتباط با ما",
                    style: hedder,
                  ),
                  freeSpace,
                  Text(
                    "مشتریان می‌توانند سوالات خود را در هر مرحله از سفارش با شماره‌ی پشتیبانی \n ۰۹۳۵۵۱۱۱۹۳۳\n۰۹۳۵۵۱۱۱۹۴۴ \nدر میان بگذارند",
                    textDirection: TextDirection.rtl,
                    style: text,
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.publicT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.publicC,
                      textDirection: TextDirection.rtl,
                      
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.connectionT,
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.connectionC,
                    textAlign: TextAlign.justify,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.sellerT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.sellerC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.connectionT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.connectionC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.privacyT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.privacyC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.deliveryT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.deliveryC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.hurtT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.hurtC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.contentT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.contentC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.pricingT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.pricingC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.representationT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.representationC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
          
            Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                    )
                  ]),
              child: ExpansionTile(
                title: Text(
                  Strings.ghahrieT,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                     Strings.ghahrieC,
                      textDirection: TextDirection.rtl,
                      style: text,
                    ),
                  ),
                ],
              ),
            ),
          
          ],
        ),
      ),
    );
  }
}

TextStyle text = TextStyle(
  fontFamily: "iranSansMobile",
  fontSize: 16,
);
TextStyle hedder = TextStyle(
  fontFamily: "iranSansMobile",
  fontSize: 17.0,
);

SizedBox freeSpace = SizedBox(
  height: 30,
);
