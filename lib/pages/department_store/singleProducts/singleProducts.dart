import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/cart/cart.dart';
import 'package:pardemarketapp/pages/department_store/show_a_single_picture/showASinglePicture.dart';
import 'package:pardemarketapp/services/getProducts.dart';
import 'package:pardemarketapp/services/get_Token.dart';
import '../StorePage/storePage.dart';
import '../search/searchingDialog.dart';
import '../cart/cartBody.dart';

class SingleProduct extends StatefulWidget {
  List productSinglePage;

  SingleProduct(this.productSinglePage);
  // This widget is the root of your application.
  @override
  _SingleProductState createState() => _SingleProductState();
}

bool add_remove = false;

// List ProductOfSinglePage;
class _SingleProductState extends State<SingleProduct>
    with WidgetsBindingObserver {
  DbHelper dbHelper = new DbHelper();
  List<MyList> myList;
  MyList mylist;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
// "${ProductOfCartPage[0]["id"]}");
    if (ProductOfCartPage.length == 0) {
      add_remove = false;
    } else {
      for (int i = 0; i < ProductOfCartPage.length; i++) {
        if (widget.productSinglePage[itemIndex]["id"] ==
            ProductOfCartPage[i]["id"]) {
          add_remove = true;
          return;
        } else {
          add_remove = false;
        }
      }
    }
  }

  int count = 0;

  void getData() {

    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();

      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;
        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
          
        }

        setState(() {
          myList = myListData;
          count = count;
          mylist = myList[0];
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // if (ProductOfCartPage.length == 0) {

    //   add_remove = false;
    //       "\n\n     no   \n\n");
    // } else {
    //   for (int i = 0; i < ProductOfCartPage.length; i++) {
    //   "\n   id widget ==   ${widget.productSinglePage[itemIndex]["id"]}      i == ${ProductOfCartPage[i]["id"]}   ");
    //     if (widget.productSinglePage[itemIndex]["id"] ==
    //         ProductOfCartPage[i]["id"]) {
    //       add_remove = true;
    //           "\n\n     ${add_remove.toString()}   \n\n");
    //     } else {
    //       add_remove = false;
    //       "\n\n    ${add_remove.toString()}    \n\n");
    //     }
    //   }
    // }

    if (myList == null) {
      myList = new List<MyList>();

      getData();
    }

    double widthOfScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        height: 50.0,
        color: add_remove ? Color(0xffc80058) : Color(0xff369627),
        // margin: EdgeInsets.only(left: 30.0),
        child: InkWell(
          onTap: () {
            if (this.myList[0].boolCustomer == "true") {
              if (getStatus(widget.productSinglePage, itemIndex)
                  .contains("منتشر شد")) {
                if (add_remove == false) {
                  // ProductOfCartPage.add(widget.productSinglePage[itemIndex]);
                  ProductOfCartPage.insert(
                      0, widget.productSinglePage[itemIndex]);
                  quntityTextAdl.insert(0, 0);
                  quntityTextTaghe.insert(0, 0);
                  quntityTextCenti.insert(0, "0");
                  quntityTextMetraj.insert(0, 0);
                  quntityTextTedadi.insert(0, 0);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext) => Cart()));
                } else {
                  ProductOfCartPage.remove(widget.productSinglePage[itemIndex]);
                }
                setState(
                  () {
                    add_remove = !add_remove;
                  },
                );
              } else {
                showErrorDialog("این محصول هنوز منتشر نشده است", _scaffoldKey);
              }
            } else {
              showErrorDialog(
                  "برای این کار شما باید ثبت نام کنید", _scaffoldKey);
            }
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                add_remove ? "حذف از سبد خرید" : "افزودن به سبد خرید",
                style: TextStyle(
                  fontFamily: "iranSansMobile",
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              Icon(
                add_remove
                    ? Icons.remove_shopping_cart
                    : Icons.add_shopping_cart,
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 60.0,
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Icon(
                      Icons.arrow_back,
                      size: 28,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(left: 0.0),
                    alignment: Alignment.center,
                    child: Text(
                      getName(widget.productSinglePage, itemIndex),
                      style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: "iranSansMobile",
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    if (ProductOfCartPage.isEmpty) {
                      showErrorDialog("سبد خرید شما خالی میباشد", _scaffoldKey);
                    } else {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => Cart()));
                    }
                  },
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0, top: 8.0),
                        child: Icon(
                          Icons.shopping_cart,
                          size: 25,
                        ),
                      ),
                      ProductOfCartPage.length == 0
                          ? Container()
                          : Positioned(
                              top: 0,
                              right: 0,
                              child: Container(
                                alignment: Alignment.center,
                                width: 21.0,
                                height: 21.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    color: Colors.white,
                                    width: 2.0,
                                  ),
                                  color: Color(0xffc80058),
                                ),
                                child: Text(
                                  ProductOfCartPage.length.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11.0,
                                      fontWeight: FontWeight.bold
                                      // fontFamily: "iranSansMobile",
                                      ),
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
                Text("    "),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffA09A9A),
                      blurRadius: 12.0,
                    )
                  ]),
              child: InkWell(
                onTap: () {
                  imageAssets = getImage(widget.productSinglePage, itemIndex);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ShowASinglePicture(),
                    ),
                  );
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  child: Hero(
                    tag: "amin",
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/imgs/zero_to_infinity_by_volorf.gif',
                      image: getImage(widget.productSinglePage, itemIndex),
                      height: min(
                        316.0,
                        widthOfScreen / 1.2,
                      ),
                      width: min(
                        316.0,
                        widthOfScreen / 1.2,
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 21.5,
            ),
            Divider(
              height: 1.0,
              indent: 10.5,
              endIndent: 10.5,
              color: Color(0xffd0d0d0),
            ),
            SizedBox(
              height: 18.5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 9.0),
                    child: Text(
                      getCategorie(widget.productSinglePage, itemIndex),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.right,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 9.0),
                    child: Text(
                      ":دسته بندی",
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 9.0),
                    child: Text(
                      showRegularPrice(widget.productSinglePage, itemIndex),
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 9.0),
                    child: Text(
                      ":قیمت",
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 9.0),
                    child: Text(
                      getShowPrice(widget.productSinglePage, itemIndex),
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 9.0),
                    child: Text(
                      ":قیمت با تخفیف",
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 9.0),
                    child: Text(
                      getNoeErae(widget.productSinglePage, itemIndex)
                          .toString(),
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 9.0),
                    child: Text(
                      ":نوع ارائه",
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 9.0),
                    child: Text(
                      getNoeErae(widget.productSinglePage, itemIndex)
                              .contains("تعدادی")
                          ? getTedadTedadi(widget.productSinglePage, itemIndex)
                          : totalQuantity(),
                      textAlign: TextAlign.right,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 9.0),
                    child: Text(
                      getNoeErae(widget.productSinglePage, itemIndex)
                              .contains("تعدادی")
                          ? ":تعداد کل"
                          : ":متراژ کل",
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 18.5,
            ),
            Divider(
              height: 1.0,
              indent: 10.5,
              endIndent: 10.5,
              color: Color(0xffd0d0d0),
            ),
            SizedBox(
              height: 17,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  "توضیحات",
                  style: TextStyle(
                    fontSize: 17,
                    fontFamily: "iranSansMobile",
                  ),
                ),
                SizedBox(
                  width: 22.0,
                ),
              ],
            ),
            SizedBox(
              height: 12.0,
            ),
            Padding(
              padding: EdgeInsets.only(left: 33.0, right: 33.0, bottom: 50.0),
              child: Text(
                getdesdescription(widget.productSinglePage, itemIndex)
                    .replaceAll(new RegExp(r'<p>'), '')
                    .replaceAll(new RegExp(r'</p>'), '')
                    .replaceAll(new RegExp(r'<br />'), ''),
                textAlign: TextAlign.right,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                    fontSize: 15, fontFamily: "iranSansMobile", height: 1.5),
              ),
            ),
          ],
        ),
      ),
    );
  }

  totalQuantity() {
    double _sizeAdl =
        getNoeErae(widget.productSinglePage, itemIndex).contains("عدل")
            ? double.parse(getSizeAdl(widget.productSinglePage, itemIndex)) *
                double.parse(getTedadAdl(widget.productSinglePage, itemIndex))
            : 0;
    double _sizeTaghe =
        getNoeErae(widget.productSinglePage, itemIndex).contains("طاقه")
            ? double.parse(getSizeTaghe(widget.productSinglePage, itemIndex)) *
                double.parse(getTedadTaghe(widget.productSinglePage, itemIndex))
            : 0;
    double _sizeMetraj =
        getNoeErae(widget.productSinglePage, itemIndex).contains("متراژ")
            ? double.parse(getTedadMetraj(widget.productSinglePage, itemIndex))
            : 0;

    return (_sizeAdl + _sizeMetraj + _sizeTaghe).toString();
  }
}
