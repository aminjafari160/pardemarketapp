import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/get_offer.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/CategoryPage/CategoryPage.dart';
import 'package:pardemarketapp/pages/department_store/ListOfProducts/ListOfProducts.dart';
import 'package:pardemarketapp/pages/department_store/singleProducts/singleProducts.dart';
// import 'package:get_version/get_version.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:pardemarketapp/services/getVersionApp.dart';
import 'package:pardemarketapp/services/get_Token.dart';
import '../../../services/getProducts.dart';
import '../../../resource/writeVersion.dart';
import '../../../services/getImage.dart';
import 'package:url_launcher/url_launcher.dart';
import '../URLS/urlS.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class StorePage extends StatefulWidget {
  @override
  _StorePageState createState() => _StorePageState();
}

int itemIndex;

List ProductsOfStorePage_featured = new List();
List ProductsOfStorePage = new List();
List ProductsOfStorePage_bestseller = new List();
DbHelper dbHelper = new DbHelper();
List<MyList> myList;
MyList mylist;

bool _versionCheck = true;

class _StorePageState extends State<StorePage> {
  double amindouble = 2;
  String myketstr = "https://myket.ir/app/com.example.pardemarketapp/?lang=fa";
  String bazarstr = "https://cafebazaar.ir/app/com.example.pardemarketapp";
  // Platform messages are asynchronous, so we initialize in an async method.
  // Future initPlatformState() async {
  //   String projectCode;
  //   // Platform messages may fail, so we use a try/catch PlatformException.
  //   try {
  //     projectCode = await GetVersion.projectCode;
  //   } on PlatformException {
  //     projectCode = 'Failed to get build number.';
  //   }

  //   // If the widget was removed from the tree while the asynchronous platform
  //   // message was in flight, we want to discard the reply rather than calling
  //   // setState to update our non-existent appearance.
  //   if (!mounted) return;

  //   setState(() {
  //   print("projectcode           "+_projectCode);
  //     _projectCode = projectCode;
  //     return projectCode;
  //   });
  // }

  _dialogForVersion() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          content: Container(
            color: Colors.white,
            padding: const EdgeInsets.all(8.0),
            child: new Text(
              "شما از نسخه قدیمی استفاده میکنید\nلطفا برنامه خود را بروزرسانی کنید",
              textDirection: TextDirection.rtl,
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: Colors.black, fontFamily: "IranSansMobile"),
            ),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: new FlatButton(
                child: Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: new Text(
                    "نمیخواد",
                    style: TextStyle(
                        color: Color(0xffc80058), fontFamily: "IranSansMobile"),
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: new FlatButton(
                child: Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: new Text(
                    "بروزرسانی کن",
                    style: TextStyle(
                        color: Color(0xffc80058), fontFamily: "IranSansMobile"),
                  ),
                ),
                onPressed: () {
                  _launchURL(myketstr);
                },
              ),
            ),
            // usually buttons at the bottom of the dialog
            // new FlatButton(
            //   child: new Text(
            //     "آپدیت کن",
            //     style: TextStyle(
            //       color: Theme.of(context).errorColor,
            //       fontWeight: FontWeight.bold,
            //     ),
            //   ),
            //   onPressed: () {
            //     launchURL(urlLink);
            //   },
            // ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    getImageBrand().whenComplete(
      () => _carouselSliderBuilder(),
    );
    if (_versionCheck) {
      localPathVersion.whenComplete(() {
        localFileVersion.whenComplete(() {
          writeVersion("35").whenComplete(() {
            version().then((value) {
              readVersion().then((version) {
                if (value != version) {
                  _dialogForVersion();
                  _versionCheck = false;
                }
              });
            });
          });
        });
      });
    }
    get_token();
    getUrl("https://pardemarket.com/wp-content/uploads/2019/09/productFeatured",
            strProductFeatured)
        .then((value) {
      getProduct("featured", strProductFeatured);
    });
    getUrlBestseller(
            "https://pardemarket.com/wp-content/uploads/2019/09/productBestseller",
            strProductBestSeller)
        .whenComplete(() {
      getProductBestseller("bestseller", strProductBestSeller);
    });
    super.initState();
    _scrcontroller.addListener(() {
      if (_scrcontroller.offset >= _scrcontroller.position.maxScrollExtent) {}
    });

    refreshing();
    checkWifi();
  }

  Future checkWifi() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {}
    } on SocketException catch (_) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              content: Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text(
                  "اینترنت شما متصل نمیباشد",
                  textDirection: TextDirection.rtl,
                  style: TextStyle(fontFamily: "IranSansMobile"),
                ),
              ),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text(
                    "خروج",
                    style: TextStyle(
                      color: Color(0xffc80058),
                      fontFamily: "IranSansMobile",
                    ),
                  ),
                  onPressed: () {
                    exit(0);
                  },
                ),
              ],
            );
          });
    }
  }

  ScrollController _scrcontroller =
      new ScrollController(initialScrollOffset: 1.0);
  Future refreshing() async {
    http.Response response = await http.get(
      (baseUrl),
      headers: {
        "Accept": "application/json",
      },
    );
    ProductsOfStorePage = json.decode(response.body);
    setState(() {});
    return ProductsOfStorePage;
  }

  Future getProduct(String product, String url) async {
    http.Response response = await http.get(
      (url),
      headers: {
        "Accept": "application/json",
      },
    );
    ProductsOfStorePage_featured = json.decode(response.body);
    setState(() {});
    print(ProductsOfStorePage_featured);
    return product;
  }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///
  ///
  ///
  Future getProductBestseller(String product, String url) async {
    List _list = new List();
    http.Response response = await http.get(
      (url),
      headers: {
        "Accept": "application/json",
      },
    );
    _list = json.decode(response.body);
    for (var i = 0; i < _list.length; i++) {
      if (String_to_int(getRegularPrice(_list, i)) >
          String_to_int(getPrice(_list, i))) {
        ProductsOfStorePage_bestseller.add(_list[i]);
      }
    }

    setState(() {});
    return product;
  }

  @override
  Widget build(BuildContext context) {
    double widthOfScreen = MediaQuery.of(context).size.width;

    if (myList == null) {
      myList = new List<MyList>();

      getData();
    }
    return RefreshIndicator(
      color: Color(0xffc80058),
      backgroundColor: Color(0xfffafafa),
      onRefresh: refreshing,
      child: ProductsOfStorePage.length == 0
          ? Center(
              child: SpinKitCubeGrid(
                color: Color(0xffc80058),
              ),
            )
          : SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  FutureBuilder(
                      future: getImageBrand(),
                      builder: (c, data) {
                        return data.hasData
                            ? CarouselSlider(
                                aspectRatio: 200.0,
                                viewportFraction: .35,
                                height: 110.0,
                                autoPlay: true,
                                autoPlayAnimationDuration:
                                    Duration(milliseconds: 500),
                                items: __carouselSlider,
                              )
                            : Text("");
                      }),
                  SizedBox(
                    height: 10,
                  ),

                  // Container(
                  //   margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  //   decoration: BoxDecoration(
                  //     boxShadow: [
                  //       new BoxShadow(
                  //         color: Color(0xffD4D4D4),
                  //         blurRadius: 12,
                  //       ),
                  //     ],
                  //   ),
                  //   child: new ClipRRect(
                  //     borderRadius: new BorderRadius.circular(15.0),
                  //     child: Image.asset(
                  //       'assets/imgs/headerOfStorePage.jpeg',
                  //       width: 314.0,
                  //       height: 163.0,
                  //     ),
                  //   ),
                  // ),
                  // ),
                  // ),
                  // /////////////////////////////////////////   newest   ///////////////////////////

                  Container(
                    margin: EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 10.0),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Color(0xffe0e0e0), width: 1.0))),
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      titleCategory("کار زیر", 0),
                      titleCategory("کار رو", 1),
                      titleCategory("لوازم", 2),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(12.0, 10.0, 12.0, 10.0),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Color(0xffe0e0e0), width: 1.0))),
                  ),

                  SizedBox(
                    height: 15.0,
                  ),
                  Stack(
                    children: <Widget>[
                      Align(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              mylist.boolBecomeAVendor = "_boolBecomeAVendor";
                              mylist.nameOfUser = "_nameOfUser";
                              mylist.boolCustomer = "_boolCustomer";
                              mylist.phoneOfUser = "_phoneOfUser";
                              mylist.userId = "_userId";
                              dbHelper.update(mylist);
                            },
                            child: Text(
                              "تازه ها",
                              style: TextStyle(
                                fontFamily: 'iranYekan',
                                color: Color(0xffC80058),
                                fontSize: 17.0,
                              ),
                            ),
                          ),
                        ),
                        alignment: Alignment.topRight,
                      ),
                      Align(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ListOfProducts(baseUrl)));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "مشاهده همه",
                              style: TextStyle(
                                fontFamily: 'iranYekan',
                                color: Color(0xff999999),
                                fontSize: 15.5,
                              ),
                            ),
                          ),
                        ),
                        alignment: Alignment.topLeft,
                      ),
                    ],
                  ),

                  Container(
                    height: 250.0,
                    child: ListView.builder(
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: ProductsOfStorePage.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  itemIndex = index;
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          SingleProduct(ProductsOfStorePage),
                                    ),
                                  );
                                },
                                child: Container(
                                  width: 127,
                                  height: 200.0,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12.0)),
                                    color: Color(0xffFAFAFA),
                                    boxShadow: [
                                      new BoxShadow(
                                        color: Color(0xffD4D4D4),
                                        blurRadius: 12,
                                      )
                                    ],
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        child: Stack(
                                          alignment: Alignment.centerRight,
                                          children: <Widget>[
                                            Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 10.0),
                                              decoration: BoxDecoration(
                                                // color: Colors.red,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(19.0)),
                                              ),
                                              child: ClipRRect(
                                                borderRadius: new BorderRadius
                                                        .only(
                                                    topLeft:
                                                        Radius.circular(12.0),
                                                    topRight:
                                                        Radius.circular(12.0)),
                                                child: FadeInImage.assetNetwork(
                                                  placeholder:
                                                      'assets/imgs/zero_to_infinity_by_volorf.gif',
                                                  image: getImage(
                                                      ProductsOfStorePage,
                                                      index),
                                                  // width: 127,
                                                  // height: 130,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                              width: 127,
                                              height: 150,
                                            ),
                                            String_to_int(getRegularPrice(
                                                        ProductsOfStorePage,
                                                        index)) >
                                                    String_to_int(getPrice(
                                                        ProductsOfStorePage,
                                                        index))
                                                ? Positioned(
                                                    top: 0,
                                                    right: 0,
                                                    child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      width: 27.0,
                                                      height: 27.0,
                                                      child: Center(
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 0.0),
                                                          child: Text(
                                                            "%" +
                                                                offer(
                                                                        String_to_int(getRegularPrice(
                                                                            ProductsOfStorePage,
                                                                            index)),
                                                                        String_to_int(getPrice(
                                                                            ProductsOfStorePage,
                                                                            index)))
                                                                    .toString(),
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xfffafafa),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 10.0,
                                                                fontFamily:
                                                                    'IransansBold'),
                                                          ),
                                                        ),
                                                      ),
                                                      decoration: BoxDecoration(
                                                        color:
                                                            Color(0xffc80058),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topRight:
                                                              Radius.circular(
                                                                  12.0),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  12.0),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        width: 100.0,
                                        height: 30.0,
                                        child: Center(
                                          child: Text(
                                            getName(ProductsOfStorePage, index),
                                            overflow: TextOverflow.ellipsis,
                                            textDirection: TextDirection.rtl,
                                            style: TextStyle(
                                              fontFamily: 'iranYekan',
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 100.0,
                                        height: 30.0,
                                        child: Center(
                                          child: Text(
                                            getShowPrice(
                                                ProductsOfStorePage, index),
                                            overflow: TextOverflow.ellipsis,
                                            textDirection: TextDirection.rtl,
                                            style: TextStyle(
                                                color: Color(0xffC80058),
                                                fontFamily: "iranSansMobile"),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),

/////////////////////////////////////////////////     featured   ////////////////////////////////////////////////////////////////

                  Container(
                    margin: EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 10.0),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom:
                            BorderSide(color: Color(0xffe0e0e0), width: 1.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Stack(
                    children: <Widget>[
                      Align(
                        child: Padding(
                          padding:
                              const EdgeInsets.fromLTRB(8.0, 0.0, 12.0, 4.0),
                          child: Text(
                            "محصولات ویژه",
                            style: TextStyle(
                              fontFamily: 'iranYekan',
                              color: Color(0xffC80058),
                              fontSize: 15.5,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        alignment: Alignment.topRight,
                      ),
                      Align(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ListOfProducts(strProductFeatured)),
                            );
                          },
                          child: Padding(
                            padding:
                                const EdgeInsets.fromLTRB(16.0, 0.0, 8.0, 8.0),
                            child: Text(
                              "مشاهده همه",
                              style: TextStyle(
                                fontFamily: 'iranYekan',
                                color: Color(0xff999999),
                                fontSize: 15.5,
                                // fontWeight: FontWeight.bold ,
                              ),
                            ),
                          ),
                        ),
                        alignment: Alignment.topLeft,
                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 230.0,
                    child: ProductsOfStorePage_featured.length == 0
                        ? Center(
                            child: Text(
                              "هیچ محصولی در این قسمت اضافه نشده است",
                              style: TextStyle(
                                fontFamily: 'iranSansMobile',
                                color: Color(0xffC80058),
                              ),
                            ),
                          )
                        : ListView.builder(
                            controller: _scrcontroller,
                            scrollDirection: Axis.horizontal,
                            reverse: true,
                            itemCount: ProductsOfStorePage_featured.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: InkWell(
                                  onTap: () {
                                    itemIndex = index;
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SingleProduct(
                                            ProductsOfStorePage_featured),
                                      ),
                                    );
                                  },
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width: 127.0,
                                        height: 200.0,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12.0)),
                                          color: Color(0xffFAFAFA),
                                          boxShadow: [
                                            new BoxShadow(
                                              color: Color(0xffD4D4D4),
                                              blurRadius: 12,
                                            )
                                          ],
                                        ),
                                        child: Column(
                                          children: <Widget>[
                                            Expanded(
                                              child: Stack(
                                                alignment:
                                                    Alignment.centerRight,
                                                children: <Widget>[
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        bottom: 10.0),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  19.0)),
                                                    ),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          new BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(
                                                                      12.0),
                                                              topRight: Radius
                                                                  .circular(
                                                                      12.0)),
                                                      child: FadeInImage
                                                          .assetNetwork(
                                                        placeholder:
                                                            'assets/imgs/zero_to_infinity_by_volorf.gif',
                                                        image: getImage(
                                                            ProductsOfStorePage_featured,
                                                            index),
                                                        // width: 127,
                                                        // height: 130,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                    width: 127,
                                                    height: 150,
                                                  ),
                                                  String_to_int(getRegularPrice(
                                                              ProductsOfStorePage_featured,
                                                              index)) >
                                                          String_to_int(getPrice(
                                                              ProductsOfStorePage_featured,
                                                              index))
                                                      ? Positioned(
                                                          top: 0,
                                                          right: 0,
                                                          child: Container(
                                                            alignment: Alignment
                                                                .center,
                                                            width: 27.0,
                                                            height: 27.0,
                                                            child: Center(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            0.0),
                                                                child: Text(
                                                                  "%" +
                                                                      offer(
                                                                        String_to_int(
                                                                          getRegularPrice(
                                                                              ProductsOfStorePage_featured,
                                                                              index),
                                                                        ),
                                                                        String_to_int(
                                                                          getPrice(
                                                                              ProductsOfStorePage_featured,
                                                                              index),
                                                                        ),
                                                                      ).toString(),
                                                                  style: TextStyle(
                                                                      color: Color(
                                                                          0xfffafafa),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          10.0,
                                                                      fontFamily:
                                                                          'IransansBold'),
                                                                ),
                                                              ),
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              color: Color(
                                                                  0xffc80058),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                topRight: Radius
                                                                    .circular(
                                                                        12.0),
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        12.0),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      : Container(),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              width: 100.0,
                                              height: 30.0,
                                              child: Center(
                                                child: Text(
                                                  getName(
                                                      ProductsOfStorePage_featured,
                                                      index),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  textDirection:
                                                      TextDirection.rtl,
                                                  style: TextStyle(
                                                    fontFamily: 'iranYekan',
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: 100.0,
                                              height: 30.0,
                                              child: Center(
                                                child: Text(
                                                  getShowPrice(
                                                      ProductsOfStorePage_featured,
                                                      index),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  textDirection:
                                                      TextDirection.rtl,
                                                  style: TextStyle(
                                                      color: Color(0xffC80058),
                                                      fontFamily:
                                                          "iranSansMobile"),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                  ),

                  /////////////////////////////////////////   bestseller   ///////////////////////////

                  Container(
                    margin: EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 10.0),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: Color(0xffe0e0e0), width: 1.0))),
                  ),

                  SizedBox(
                    height: 15.0,
                  ),
                  Stack(
                    children: <Widget>[
                      Align(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "حراجی ها",
                            style: TextStyle(
                              fontFamily: 'iranYekan',
                              color: Color(0xffC80058),
                              fontSize: 17.0,
                            ),
                          ),
                        ),
                        alignment: Alignment.topRight,
                      ),
                      Align(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    ListOfProducts(strProductBestSeller),
                              ),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "مشاهده همه",
                              style: TextStyle(
                                fontFamily: 'iranYekan',
                                color: Color(0xff999999),
                                fontSize: 15.5,
                              ),
                            ),
                          ),
                        ),
                        alignment: Alignment.topLeft,
                      ),
                    ],
                  ),

                  Container(
                    height: 250.0,
                    child: ListView.builder(
                      reverse: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: ProductsOfStorePage_bestseller.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () {
                              itemIndex = index;
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SingleProduct(
                                          ProductsOfStorePage_bestseller)));
                            },
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: 127,
                                  height: 200.0,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12.0)),
                                    color: Color(0xffFAFAFA),
                                    boxShadow: [
                                      new BoxShadow(
                                        color: Color(0xffD4D4D4),
                                        blurRadius: 12,
                                      )
                                    ],
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                        child: Stack(
                                          alignment: Alignment.centerRight,
                                          children: <Widget>[
                                            Container(
                                              margin:
                                                  EdgeInsets.only(bottom: 10.0),
                                              decoration: BoxDecoration(
                                                // color: Colors.red,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(19.0)),
                                              ),
                                              child: ClipRRect(
                                                borderRadius: new BorderRadius
                                                        .only(
                                                    topLeft:
                                                        Radius.circular(12.0),
                                                    topRight:
                                                        Radius.circular(12.0)),
                                                child: FadeInImage.assetNetwork(
                                                  placeholder:
                                                      'assets/imgs/zero_to_infinity_by_volorf.gif',
                                                  image: getImage(
                                                      ProductsOfStorePage_bestseller,
                                                      index),
                                                  // width: 127,
                                                  // height: 130,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                              width: 127,
                                              height: 150,
                                            ),
                                            String_to_int(getRegularPrice(
                                                        ProductsOfStorePage_bestseller,
                                                        index)) >
                                                    String_to_int(getPrice(
                                                        ProductsOfStorePage_bestseller,
                                                        index))
                                                ? Positioned(
                                                    top: 0,
                                                    right: 0,
                                                    child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      width: 27.0,
                                                      height: 27.0,
                                                      child: Center(
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 0.0),
                                                          child: Text(
                                                            "%" +
                                                                offer(
                                                                        String_to_int(getRegularPrice(
                                                                            ProductsOfStorePage_bestseller,
                                                                            index)),
                                                                        String_to_int(getPrice(
                                                                            ProductsOfStorePage_bestseller,
                                                                            index)))
                                                                    .toString(),
                                                            style: TextStyle(
                                                                color: Color(
                                                                    0xfffafafa),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 10.0,
                                                                fontFamily:
                                                                    'IransansBold'),
                                                          ),
                                                        ),
                                                      ),
                                                      decoration: BoxDecoration(
                                                        color:
                                                            Color(0xffc80058),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topRight:
                                                              Radius.circular(
                                                                  12.0),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  12.0),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        width: 100.0,
                                        height: 30.0,
                                        child: Center(
                                          child: Text(
                                            getName(
                                                ProductsOfStorePage_bestseller,
                                                index),
                                            overflow: TextOverflow.ellipsis,
                                            textDirection: TextDirection.rtl,
                                            style: TextStyle(
                                              fontFamily: 'iranYekan',
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 100.0,
                                        height: 30.0,
                                        child: Center(
                                          child: Text(
                                            getShowPrice(
                                                ProductsOfStorePage_bestseller,
                                                index),
                                            overflow: TextOverflow.ellipsis,
                                            textDirection: TextDirection.rtl,
                                            style: TextStyle(
                                                color: Color(0xffC80058),
                                                fontFamily: "iranSansMobile"),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  int count = 0;

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();

      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;
        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
          mylist = myList[0];
        });
      });
    });
  }

  List<Widget> __carouselSlider = new List();

  _carouselSliderBuilder() {
    for (var i = 0; i < urlsBrand.length; i++) {
      print(__carouselSlider.length);
      __carouselSlider.add(
        ClipRRect(
          borderRadius: new BorderRadius.circular(15.0),
          child: Image.network(
            urlsBrand[i].toString(),
            width: 314.0,
            height: 163.0,
          ),
        ),
      );
    }
  }

  titleCategory(title, index) {
    return InkWell(
      onTap: () {
        _navCategory(context, index);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8),
        width: MediaQuery.of(context).size.width / 3.5,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xffc80058)),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          color: Colors.white,
        ),
        child: Text(
          title,
          style: TextStyle(
            fontFamily: "iranSansMobile",
            color: Color(0xffc80058),
          ),
        ),
      ),
    );
  }
}

_navCategory(context, index) {
  return Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => CategoryPage(index),
    ),
  );
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
