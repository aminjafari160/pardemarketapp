import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/IntroductionPage.dart';
import 'package:pardemarketapp/pages/department_store/cart/cart.dart';
import 'package:pardemarketapp/pages/department_store/cart/cartBody.dart';
import 'package:pardemarketapp/pages/department_store/search/searchingDialog.dart';

int count = 0;

class StoreAppBar extends StatefulWidget {
  @override
  _StoreAppBarState createState() => _StoreAppBarState();
}

class _StoreAppBarState extends State<StoreAppBar> {
  DbHelper dbHelper = new DbHelper();

  List<MyList> myList;

  MyList mylist;

  @override
  void initState() {
    super.initState();
    // getData();
    // mylist = this.myList[0];
  }

  @override
  Widget build(BuildContext context) {
    
    // mylist = this.myList[0];
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Container(
      decoration: BoxDecoration(
        border: Border(
            bottom: BorderSide(
          color: Color(0xffD0D0D0),
          width: 1.0,
        )),
        color: Color(0xffeaeaea),
      ),
      height: 74.0,
      width: 360.0,
      padding: EdgeInsets.only(top: 30.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                onTap: () {
                  if (ProductOfCartPage.isEmpty) {
                    
                  } else {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Cart()));
               
                  }},
                child: Stack(
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20.0),
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.shopping_cart,
                        color: darkColorFont,
                        size: 26.0,
                      ),
                    ),
                    ProductOfCartPage.length == 0
                        ? Container()
                        : Positioned(
                            top: -8,
                            right: -5,
                            child: Container(
                              alignment: Alignment.center,
                              width: 21.0,
                              height: 21.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: Colors.white,
                                  width: 2.0,
                                ),
                                color: Color(0xffc80058),
                              ),
                              child: Text(
                                ProductOfCartPage.length.toString(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11.0,
                                    fontWeight: FontWeight.bold
                                    // fontFamily: "iranSansMobile",
                                    ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  'پرده مارکت',
                  style: TextStyle(
                    color: RedColorFont,
                    fontFamily: 'sogand',
                    fontSize: 28.0,
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchingDialog()));
                },
                child: Container(
                  padding: EdgeInsets.only(right: 20.0),
                  alignment: Alignment.centerLeft,
                  child: Icon(
                    Icons.search,
                    color: darkColorFont,
                    size: 26.0,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
        });
      });
    });
  }
}
