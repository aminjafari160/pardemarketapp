import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/StorePage/storePage.dart';
import 'package:pardemarketapp/services/get_Token_forUser.dart';
import 'package:pardemarketapp/services/order_get.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:jalali_date/jalali_date.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import '../../../services/get_Token.dart';
import '../show_single_factor/list_Of_Product_In_Orders.dart';

int indexOfFactor;

List Orders_User, Orders_User_Buy;

class Show_All_Factors extends StatefulWidget {
  @override
  _Show_All_FactorsState createState() => _Show_All_FactorsState();
}

class _Show_All_FactorsState extends State<Show_All_Factors>
    with SingleTickerProviderStateMixin {
  reload() {
    setState(() {});
  }

  String userToken;
  TabController _tabBarController;
  @override
  void initState() {
    if (myList[0].boolCustomer == "true") {
      get_orders_user_Buy(reload, myList[0].userId);

      if (myList[0].boolBecomeAVendor == "true") {
        get_token_for_user(myList[0].phoneOfUser).then((onValue) {
          userToken = onValue.toString();
        }).whenComplete(() {
          get_orders_user_sale(userToken, reload);
        });
      }
    }

    super.initState();
    _tabBarController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Color(0xff530050),
              Color(0xffc80058),
            ], begin: Alignment.topRight, end: Alignment.bottomLeft)),
          ),
          bottom: TabBar(
            indicatorColor: Color(0xfffafafa),
            controller: _tabBarController,
            tabs: <Widget>[
              new Tab(
                child: Text(
                  "فاکتورهای فروش",
                  style: TextStyle(
                    color: Color(0xfffafafa),
                    fontFamily: "IranSansMobile",
                  ),
                ),
              ),
              new Tab(
                child: Text(
                  "فاکتورهای خرید",
                  style: TextStyle(
                    color: Color(0xfffafafa),
                    fontFamily: "IranSansMobile",
                  ),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          controller: _tabBarController,
          children: <Widget>[
            SaleFactor(),
            BuyFactor(),
          ],
        ));
  }
}

class SaleFactor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
        // RefreshIndicator(
        //   color: Color(0xffc80058),
        //   backgroundColor: Color(0xfffafafa),
        //   onRefresh: get_orders_user,
        //   child:

        myList[0].boolBecomeAVendor == "false"
            ? Center(
                child: Text(
                  "شما هنوز به عنوان فروشنده ثبت نام نکرده اید",
                  style: TextStyle(
                      color: Color(0xffc80058), fontFamily: "iranYekan"),
                ),
              )
            : Orders_User == null
                ? Center(
                    child: SpinKitCubeGrid(
                      color: Color(0xffc80058),
                    ),
                  )
                : Orders_User.length == 0
                    ? Center(
                        child: Text(
                          "هیچ موردی یافت نشد",
                          style: TextStyle(
                              color: Color(0xffc80058),
                              fontFamily: "iranYekan"),
                        ),
                      )
                    : ListView.builder(
                        itemCount: Orders_User.length,
                        itemBuilder: (BuildContext context, int index) {
                          return 

statusOfOrder(Orders_User, index) == "processing"?
                          
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xffd4d4d4),
                                        offset: Offset(0, 3),
                                        blurRadius: 10.0)
                                  ]),
                              margin: EdgeInsets.fromLTRB(15.0, 3, 15, 8),
                              child: Column(
                                children: <Widget>[
                                  // Padding(
                                  //   padding: const EdgeInsets.all(20.0),
                                  //   child: Row(
                                  //     mainAxisAlignment: MainAxisAlignment.center,
                                  //     children: <Widget>[
                                  //       // SizedBox(width: 40.0,),
                                  //       Text(
                                  //         "خرید",
                                  //         style: TextStyle(
                                  //           color: Color(0xffaa2266),
                                  //           fontFamily: "IranSansMobile",
                                  //         ),
                                  //       ),
                                  //       SizedBox(
                                  //         width: 20.0,
                                  //       ),
                                  //       Text(
                                  //         "نوع فاکتور",
                                  //         style: TextStyle(
                                  //           color: Color(0xffaa2266),
                                  //           fontFamily: "IranSansMobile",
                                  //         ),
                                  //       ),
                                  //     ],
                                  //   ),
                                  // ),
                                  // Divider(
                                  //   color: Colors.grey,
                                  //   height: 2.0,
                                  //   endIndent: 20.0,
                                  //   indent: 20.0,
                                  // ),
                                  InkWell(
                                    onTap: () {
                                      indexOfFactor = index;
                                      listOfProductInLineItems =
                                          listOfProductInOrder(
                                              Orders_User, index);
                                      totalPrice =
                                          totalpriceOfSingleProductInList(

                                              Orders_User, index);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ListOfProductInOrder("فاکتور خرید")));
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Icon(
                                            Icons.arrow_back_ios,
                                            color: Color(0xff4433aa),
                                            size: 18.0,
                                          ),
                                          Text(
                                            "مشاهده جزئیات",
                                            style: TextStyle(
                                              color: Color(0xff4433aa),
                                              fontFamily: "IranSansMobile",
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                    height: 2.0,
                                    endIndent: 20.0,
                                    indent: 20.0,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          converDate(dateCreatedOrderUser(
                                                  Orders_User, index)) +
                                              "  " +
                                              timeCreatedOrderUser(
                                                  Orders_User, index),
                                          style: TextStyle(
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                        Text(
                                          "تاریخ",
                                          style: TextStyle(
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                    height: 2.0,
                                    endIndent: 20.0,
                                    indent: 20.0,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          "${totalpriceOfSingleProductInList(Orders_User, index)}",
                                          style: TextStyle(
                                            color: Color(0xff33aa55),
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                        Text(
                                          "مبلغ کل",
                                          style: TextStyle(
                                            // color: Color(0xff33aa55),
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ):SizedBox();
                        },
                        // ),
                      );
  }
}

class BuyFactor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
        // RefreshIndicator(
        //   color: Color(0xffc80058),
        //   backgroundColor: Color(0xfffafafa),
        //   onRefresh: get_orders_user,
        //   child:

        myList[0].boolCustomer == "false"
            ? Center(
                child: Text(
                  "شما هنوز ثبت نام نکرده اید",
                  style: TextStyle(
                      color: Color(0xffc80058), fontFamily: "iranYekan"),
                ),
              )
            : Orders_User_Buy == null
                ? Center(
                    child: SpinKitCubeGrid(
                      color: Color(0xffc80058),
                    ),
                  )
                : Orders_User_Buy.length == 0
                    ? Center(
                        child: Text(
                          "هیچ موردی یافت نشد",
                          style: TextStyle(
                              color: Color(0xffc80058),
                              fontFamily: "iranYekan"),
                        ),
                      )
                    : ListView.builder(
                        itemCount: Orders_User_Buy.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xffd4d4d4),
                                        offset: Offset(0, 3),
                                        blurRadius: 10.0)
                                  ]),
                              margin: EdgeInsets.fromLTRB(15.0, 3, 15, 8),
                              child: Column(
                                children: <Widget>[
                                  // Padding(
                                  //   padding: const EdgeInsets.all(20.0),
                                  //   child: Row(
                                  //     mainAxisAlignment: MainAxisAlignment.center,
                                  //     children: <Widget>[
                                  //       // SizedBox(width: 40.0,),
                                  //       Text(
                                  //         "خرید",
                                  //         style: TextStyle(
                                  //           color: Color(0xffaa2266),
                                  //           fontFamily: "IranSansMobile",
                                  //         ),
                                  //       ),
                                  //       SizedBox(
                                  //         width: 20.0,
                                  //       ),
                                  //       Text(
                                  //         "نوع فاکتور",
                                  //         style: TextStyle(
                                  //           color: Color(0xffaa2266),
                                  //           fontFamily: "IranSansMobile",
                                  //         ),
                                  //       ),
                                  //     ],
                                  //   ),
                                  // ),
                                  // Divider(
                                  //   color: Colors.grey,
                                  //   height: 2.0,
                                  //   endIndent: 20.0,
                                  //   indent: 20.0,
                                  // ),
                                  InkWell(
                                    onTap: () {
                                      indexOfFactor = index;
                                      orderSale = false;
                                      listOfProductInLineItems =
                                          listOfProductInOrder(
                                              Orders_User_Buy, index);
                                      totalPrice =
                                          totalpriceOfSingleProductInList(
                                              Orders_User_Buy, index);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ListOfProductInOrder("فاکتور خرید")));
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Icon(
                                            Icons.arrow_back_ios,
                                            color: Color(0xff4433aa),
                                            size: 18.0,
                                          ),
                                          Text(
                                            "مشاهده جزئیات",
                                            style: TextStyle(
                                              color: Color(0xff4433aa),
                                              fontFamily: "IranSansMobile",
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                    height: 2.0,
                                    endIndent: 20.0,
                                    indent: 20.0,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          converDate(dateCreatedOrderUser(
                                                  Orders_User_Buy, index)) +
                                              "  " +
                                              timeCreatedOrderUser(
                                                  Orders_User_Buy, index),
                                          style: TextStyle(
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                        Text(
                                          "تاریخ",
                                          style: TextStyle(
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                    height: 2.0,
                                    endIndent: 20.0,
                                    indent: 20.0,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          "${totalpriceOfSingleProductInList(Orders_User_Buy, index)}",
                                          style: TextStyle(
                                            color: Color(0xff33aa55),
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                        Text(
                                          "مبلغ کل",
                                          style: TextStyle(
                                            // color: Color(0xff33aa55),
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Colors.grey,
                                    height: 2.0,
                                    endIndent: 20.0,
                                    indent: 20.0,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(20.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          "${statusOfSale(statusOfOrder(Orders_User_Buy, index))}",
                                          style: TextStyle(
                                            color: Color(0xff33aa55),
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                        Text(
                                          "وضعیت پرداخت",
                                          style: TextStyle(
                                            // color: Color(0xff33aa55),
                                            fontFamily: "IranSansMobile",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        // ),
                      );
  }
}

String urlBuy =
    'https://pardemarket.com/wp-json/wc/v3/orders?consumer_key=ck_79788848af39dd8c025955af50f3284b1e7a8b2f&consumer_secret=cs_573910910f89ecd1b24cf406364e0be49baf49cd&per_page=100';
String urlSale = 'https://pardemarket.com/wp-json/dokan/v1/orders?per_page=100';

Future get_orders_user_sale(String token, [Function reload]) async {

  http.Response response = await http.get((urlSale), headers: {
    "Accept": "application/json",
    "Authorization": "Bearer $token",
  });
  Orders_User = json.decode(response.body);

  reload();
  return Orders_User;
}

Future get_orders_user_Buy(Function reload, String id) async {

  http.Response response = await http.get(
    (urlBuy /* + "&customer=$id" */),
    headers: {
      "Accept": "application/json",
      'Content-type': 'application/json',
    },
  );
  Orders_User_Buy = json.decode(response.body);

  reload();
  return Orders_User_Buy;
}

converDate(String date) {
  int _year = int.parse(date.substring(0, 4));
  int _month = int.parse(date.substring(5, 7));
  int _day = int.parse(date.substring(8, 10));

  return PersianDate.fromDateTime(DateTime(_year, _month, _day)).toString();
}

statusOfSale(String str){

  if (str == "processing") {

    return "پرداخت شده";
    
  }else if(str == "pending"){
    return "پرداخت نشده";
  }else if(str == "completed"){
    return "تکمیل شده";
  }else{
    return "نامعلوم";
  }

}