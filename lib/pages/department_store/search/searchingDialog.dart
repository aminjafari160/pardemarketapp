import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/function/get_offer.dart';
import 'package:pardemarketapp/pages/department_store/URLS/urlS.dart';
import 'package:pardemarketapp/pages/department_store/singleProducts/singleProducts.dart';
import '../../../services/getProducts.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../StorePage/storePage.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';

class SearchingDialog extends StatefulWidget {
  @override
  _SearchingDialogState createState() => _SearchingDialogState();
}

bool boolComplete = true;

class _SearchingDialogState extends State<SearchingDialog> {
  List ProductOfSearchPage = new List();
  bool boolSearch = true;
  String searchString = 'فیلد خالی است';
  TextEditingController textFieldController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    getProducts(searchUrl);
    super.initState();
  }

  reload() {
    return setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double widthOfScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffc80058),
        onPressed: () {
          cupertinForSortPrice(
              context, reload, textFieldController.text, getProducts);
        },
        child: Icon(
          Icons.sort,
          color: Color(0xfffafafa),
        ),
      ),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(90.0),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(10.0, 35.0, 10, 0),
              height: 50.0,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: boolSearch ? Color(0xffaaaaaa) : Color(0xffE40064),
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  color: Color(0xffffffff),
                  boxShadow: [
                    new BoxShadow(
                      color: Color(0xffdddddd),
                      blurRadius: 2.0,
                      // offset: Offset(0, 2),
                    )
                  ]),
              child: TextField(
                style: TextStyle(fontFamily: 'IransansMobile'),
                controller: textFieldController,
                autofocus: true,
                textAlign: TextAlign.right,
                textDirection: TextDirection.rtl,
                textInputAction: TextInputAction.search,
                onEditingComplete: () {
                  getProducts(searchUrl + textFieldController.text);
                  FocusScope.of(context).requestFocus(new FocusNode());
                  boolComplete = true;
                },
                onChanged: (value) {
                  setState(() {
                    // boolComplete = true;
                    textFieldController.text.length == 0
                        ? boolSearch = true
                        : boolSearch = false;
                  });
                },
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(0.0, 9.0, 2.0, 0.0),
                  border: InputBorder.none,
                  hintText: "...جستجو",
                  suffixIcon: InkWell(
                    onTap: () {
                      getProducts(searchUrl + textFieldController.text);
                      FocusScope.of(context).requestFocus(new FocusNode());
                      boolComplete = true;
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Icon(
                        Icons.search,
                        color:
                            boolSearch ? Color(0xffaaaaaa) : Color(0xffE40064),
                        size: 25.0,
                      ),
                    ),
                  ),
                  prefixIcon: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 5.0),
                      child: Icon(
                        Icons.arrow_back,
                        color: Color(0xffE40064),
                        size: 26.0,
                      ),
                    ),
                  ),
                  hintStyle: TextStyle(
                    color: Color(0xff8d8d8d),
                    fontSize: 17,
                    fontFamily: 'iransans',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      body: boolComplete
          ? Center(
              child: SpinKitThreeBounce(
                size: 28.0,
                color: Color(0xffc80058),
              ),
            )
          : ProductOfSearchPage.length == 0
              ? Center(
                  child: Text(
                    "هیچ محصولی یافت نشد",
                    style: TextStyle(
                        color: Color(0xffc80058), fontFamily: "IranSansMobile"),
                  ),
                )
              : ListView.builder(
                  itemCount: ProductOfSearchPage.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: InkWell(
                        onTap: () {
                          itemIndex = index;
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  SingleProduct(ProductOfSearchPage),
                            ),
                          );
                        },
                        child: Stack(
                          alignment: Alignment.centerRight,
                          children: <Widget>[
                            Positioned(
                              left: 10.0,
                              child: Container(
                                width: widthOfScreen / 1.4,
                                height: 111.0,
                                decoration: BoxDecoration(
                                  color: Color(0xfffafafa),
                                  boxShadow: [
                                    new BoxShadow(
                                        color: Color(0xffCBCBCB), blurRadius: 6)
                                  ],
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(19.0),
                                  ),
                                ),
                                child: Stack(
                                  alignment: Alignment.centerRight,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(right: 75.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 40.0),
                                            child: Text(
                                              getName(
                                                  ProductOfSearchPage, index),
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontFamily: 'iranSansMobile'),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 18.0),
                                            child: 
                                                Text(
                                                  "دسته بندی :" +
                                                      "${getCategorie(ProductOfSearchPage, index)}",
                                                  textDirection:
                                                      TextDirection.rtl,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Color(0xff787878),
                                                      fontFamily:
                                                          "IransansLight"),
                                                
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      left: 10.0,
                                      bottom: 10.0,
                                      child: Text(
                                        getShowPrice(
                                            ProductOfSearchPage, index),
                                        textDirection: TextDirection.rtl,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Color(0xffc80058),
                                          fontFamily: "IransansLight",
                                        ),
                                      ),
                                    ),
                                    String_to_int(getRegularPrice(
                                                ProductOfSearchPage, index)) >
                                            String_to_int(getPrice(
                                                ProductOfSearchPage, index))
                                        ? Positioned(
                                            top: 0,
                                            left: 0,
                                            child: Container(
                                              alignment: Alignment.center,
                                              width: 39.0,
                                              height: 39.0,
                                              child: Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 0.0),
                                                  child: Text(
                                                    "%" +
                                                        offer(
                                                          String_to_int(
                                                            getRegularPrice(
                                                                ProductOfSearchPage,
                                                                index),
                                                          ),
                                                          String_to_int(
                                                            getPrice(
                                                                ProductOfSearchPage,
                                                                index),
                                                          ),
                                                        ).toString(),
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xfffafafa),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15.0,
                                                        fontFamily:
                                                            'IransansBold'),
                                                  ),
                                                ),
                                              ),
                                              decoration: BoxDecoration(
                                                color: Color(0xffc80058),
                                                borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(20.0),
                                                  bottomRight:
                                                      Radius.circular(20.0),
                                                ),
                                              ),
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                // color: Colors.red,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(19.0)),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(19.0),
                                ),
                                child: FadeInImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                    getImage(ProductOfSearchPage, index),
                                  ),
                                  placeholder: AssetImage(
                                    'assets/imgs/zero_to_infinity_by_volorf.gif',
                                  ),
                                ),
                              ),
                              width: 131.0,
                              height: 131.0,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
    );
  }

  Future<List> getProducts(String url) async {
    http.Response response = await http.get(
      (url),
      headers: {
        "Accept": "application/json",
      },
    );

    ProductOfSearchPage = json.decode(response.body);

    boolComplete = false;
    setState(() {});
    return ProductOfSearchPage = json.decode(response.body);
  }
}

cupertinForSortPrice(BuildContext context, Function reload,
    [String url, var getProducts]) {
  String orderby = "&orderby=price";
  String orderasc = "&order=asc";
  String orderdesc = "&order=desc";

  return showCupertinoModalPopup(
    context: context,
    builder: (BuildContext context) => CupertinoActionSheet(
      title: const Text(
        ': مرتب سازی بر اساس ',
        textDirection: TextDirection.ltr,
        style: TextStyle(
          fontFamily: "iranSansD",
          color: Color(0xffc80058),
          fontSize: 16.0,
        ),
      ),
      actions: <Widget>[
        CupertinoActionSheetAction(
          child: const Text(
            'گرانترین',
            style: TextStyle(
              fontFamily: "iranSansD",
              color: Colors.black,
              fontSize: 16.0,
            ),
          ),
          isDefaultAction: true,
          onPressed: () {
            boolComplete = true;
            reload();
            getProducts(searchUrl + url + orderby + orderdesc);
            Navigator.pop(context);
          },
        ),
        CupertinoActionSheetAction(
          child: const Text(
            'ارزانترین',
            style: TextStyle(
              fontFamily: "iranSansD",
              color: Colors.black,
              fontSize: 16.0,
            ),
          ),
          isDefaultAction: true,
          onPressed: () {
            boolComplete = true;
            reload();
            getProducts(searchUrl + url + orderby + orderasc);
            Navigator.pop(context);
          },
        ),
      ],
    ),
  );
}
