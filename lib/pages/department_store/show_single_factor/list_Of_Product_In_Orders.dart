import 'package:flutter/material.dart';
import 'package:pardemarketapp/function/monyString.dart';
import 'package:pardemarketapp/services/order_get.dart';
import '../show_all_factors/show_all_factors.dart';

class ListOfProductInOrder extends StatelessWidget {
  String strFactor;
  ListOfProductInOrder(this.strFactor);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListofProductInOrder(strFactor),
    );
  }
}

List listOfProductInLineItems = new List();
String totalPrice;
bool orderSale = true;

class ListofProductInOrder extends StatelessWidget {
  String strFactor;
  ListofProductInOrder(this.strFactor);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              // SizedBox(width: 50.0),
              Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "تاریخ فاکتور \n${converDate(orderSale ? dateCreatedOrderUser(Orders_User, indexOfFactor) : dateCreatedOrderUser(Orders_User_Buy, indexOfFactor))}",
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Container(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "شماره فاکتور \n${orderSale ? orderKeyOfSingleProductInList(Orders_User, indexOfFactor) : orderKeyOfSingleProductInList(Orders_User_Buy, indexOfFactor)}",
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 25.0),
                    child: Text(
                      strFactor,
                      style: TextStyle(
                        fontFamily: "iranSansMobile",
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Text("پرده مارکت",
                        style: TextStyle(
                          fontFamily: "iranSansMobile",
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                  SizedBox(height: 15.0),
                  Text("pardemarket.com",
                      style: TextStyle(
                        fontFamily: "iranSansMobile",
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 18.0),
                child: Image.asset(
                  "assets/imgs/logo.png",
                  width: 50.0,
                  height: 50.0,
                ),
              ),
            ],
          ),
          // Padding(
          //   padding: const EdgeInsets.only(top: 25.0),
          //   child: Text(
          //     "آدرس: تهران بازار عباس آباد کوچه علی آبادی پاساژ علی آبادی طبقه دوم پلاک 101",
          //     style: TextStyle(fontFamily: "iranSansMobile", fontSize: 10.0),
          //   ),
          // ),
          // SizedBox(
          //   height: 10.0,
          // ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: <Widget>[
          //     Expanded(
          //       child: Center(
          //         child: Text(
          //           "تلفن: 55692752",
          //           style:
          //               TextStyle(fontSize: 10, fontFamily: "IranSansMobile"),
          //         ),
          //       ),
          //     ),
          //     Expanded(
          //       child: Center(
          //         child: Text(
          //           "موبایل: 09125436616",
          //           style:
          //               TextStyle(fontSize: 10, fontFamily: "IranSansMobile"),
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          // SizedBox(
          //   height: 10.0,
          // ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: <Widget>[
          //     Expanded(
          //       child: Center(
          //         child: Text(
          //           "تلفن: 55696774",
          //           style:
          //               TextStyle(fontSize: 10, fontFamily: "IranSansMobile"),
          //         ),
          //       ),
          //     ),
          //     Expanded(
          //       child: Center(
          //         child: Text(
          //           "موبایل: 09035436616",
          //           style:
          //               TextStyle(fontSize: 10, fontFamily: "IranSansMobile"),
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          Divider(
            height: 25.0,
            color: Colors.black,
          ),
          // Container(
          //   alignment: Alignment.centerRight,
          //   child: Text(
          //     "طرف حساب آقای/خانم : ",
          //     textDirection: TextDirection.rtl,
          //     style: TextStyle(fontFamily: "iranSansMobile"),
          //   ),
          // ),
          Expanded(
            child: ListView.builder(
              itemCount: listOfProductInLineItems.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  decoration: BoxDecoration(
                      border: Border(
                          left: BorderSide(color: Colors.black),
                          right: BorderSide(color: Colors.black))),
                  child: Column(
                    children: <Widget>[
                      index == 0
                          ? Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      left: BorderSide(color: Colors.black),
                                      right: BorderSide(color: Colors.black))),
                              child: Column(
                                children: <Widget>[
                                  index == 0
                                      ? Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 1.0,
                                          color: Colors.black,
                                        )
                                      : SizedBox(
                                          height: 0,
                                        ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 3,
                                          child: Center(
                                            child: Text(
                                              "قیمت کل",
                                              style: TextStyle(
                                                  fontFamily: "IranSansMobile"),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child: Center(
                                            child: Text(
                                              "قیمت (هرواحد)",
                                              style: TextStyle(
                                                  fontFamily: "IranSansMobile"),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Center(
                                            child: Text(
                                              " متراژ",
                                              style: TextStyle(
                                                  fontFamily: "IranSansMobile"),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 4,
                                          child: Center(
                                            child: Text(
                                              "نام محصول",
                                              style: TextStyle(
                                                  fontFamily: "IranSansMobile"),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.black,
                                    height: 1.0,
                                  ),
                                ],
                              ),
                            )
                          : SizedBox(
                              height: 0.0,
                            ),
                      index == 0
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              height: 1.0,
                              color: Colors.black,
                            )
                          : SizedBox(
                              height: 0,
                            ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Center(
                                child: Text(
                                  toMony(
                                    
                                   ( (quntityOfSingleProductInList_int(listOfProductInLineItems, index)) *
                                    (priceOfSingleProductInList_int(listOfProductInLineItems, index))).toStringAsFixed(0)

                                  ),
                                  style:
                                      TextStyle(fontFamily: "IranSansMobile"),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Center(
                                child: Text(
                                  toMony(priceOfSingleProductInList(
                                      listOfProductInLineItems, index)),
                                  style:
                                      TextStyle(fontFamily: "IranSansMobile"),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: Text(
                                  "${quntityOfSingleProductInList(listOfProductInLineItems, index)}",
                                  style:
                                      TextStyle(fontFamily: "IranSansMobile"),
                                      textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Center(
                                child: Text(
                                  "${nameOfSingleProductInList(listOfProductInLineItems, index)}",
                                  style:
                                      TextStyle(fontFamily: "IranSansMobile"),
                                      textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.black,
                        height: 1.0,
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
            ),
            child: Column(
              children: <Widget>[
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                //   children: <Widget>[
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Text(
                //         "0",
                //         style: TextStyle(fontFamily: "IranSansMobile"),
                //       ),
                //     ),
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Text(
                //         "  :  جمع تخفیف ها",
                //         style: TextStyle(fontFamily: "IranSansMobile"),
                //       ),
                //     ),
                //   ],
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        toMony(totalPrice),
                        style: TextStyle(fontFamily: "IranSansMobile"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "  :  مبلغ کل",
                        style: TextStyle(fontFamily: "IranSansMobile"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
