import 'package:flutter/material.dart';
import 'package:pardemarketapp/services/createATicket.dart';

  String phonenumber, title, content;
class FormOfReciver extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color(0xffeeeeee),
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  height: 61.0,
                ),
                Text(
                  "مشخصات خریدار",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xffc80058),
                    fontSize: 25.0,
                    fontFamily: 'iranYekan',
                    fontWeight: FontWeight.bold
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                Container(
                  width: width / 1.2,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    onChanged: (value) {
                      title = value;
                    },
                    style: TextStyle(fontFamily: 'iranSansMobile',),
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "نام و نام خانوادگی",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontFamily: 'iranSansMobile',
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  width: width / 1.2,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    style: TextStyle(fontFamily: 'iranSansMobile',),
                    onChanged: (value) {
                      phonenumber = value;
                    },
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "شماره تماس",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontSize: 17,
                        fontFamily: 'iranSansMobile',
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  width: width / 1.2,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    style: TextStyle(fontFamily: 'iranSansMobile',),
                    onChanged: (value) {
                      title = value;
                    },
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "استان",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontFamily: 'iranSansMobile',
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  width: width / 1.2,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    style: TextStyle(fontFamily: 'iranSansMobile',),
                    onChanged: (value) {
                      title = value;
                    },
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "شهر",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontFamily: 'iranSansMobile',
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  width: width / 1.2,
                  // height: 120,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    style: TextStyle(fontFamily: 'iranSansMobile',),
                    minLines: 5,
                    maxLines: 5,
                    onChanged: (value) {
                      title = value;
                    },
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "نشانی",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontFamily: 'iranSansMobile',
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                // Container(
                //   width: width / 1.2,
                //   height: 232,
                //   decoration: BoxDecoration(
                //     color: Color(0xfffafafa),
                //     borderRadius: BorderRadius.all(
                //       Radius.circular(16.0),
                //     ),
                //     boxShadow: [
                //       BoxShadow(
                //         blurRadius: 16.0,
                //         color: Color(0xffd1d1d1),
                //         offset: Offset(0.0, 3.0),
                //       ),
                //     ],
                //   ),
                //   child: TextField(
                //     onChanged: (value) {
                //       content = value;
                //     },
                //     textAlign: TextAlign.right,
                //     decoration: InputDecoration(
                //       contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                //       border: InputBorder.none,
                //       hintText: "متن پیام",
                //       hintStyle: TextStyle(
                //         color: Color(0xff8d8d8d),
                //         fontSize: 17,
                //         fontFamily: 'iransans',
                //       ),
                //     ),
                //   ),
                // ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // createATicket(title, content, phonenumber, 51);
                  },
                  child: Container(
                    height: 44,
                    width: 125,
                    decoration: BoxDecoration(
                      color: Color(0xffc80058),
                      borderRadius: BorderRadius.all(
                        Radius.circular(15.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xff9b9b9b),
                          blurRadius: 16.0,
                          offset: Offset(0.0, 3.0),
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 17.0,
                        ),
                        Text(
                          "تکمیل فرم",
                          style: TextStyle(
                              color: Color(0xfffafafa),
                              fontFamily: 'iranYekan',
                              fontSize: 16.0),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Image.asset(
                          "assets/imgs/send.png",
                          height: 22.0,
                          width: 22.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Text(""),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
