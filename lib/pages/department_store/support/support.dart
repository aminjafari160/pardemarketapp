import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/services/createATicket.dart';
import 'package:pardemarketapp/services/sms_service.dart';

String phonenumber, title, content;

class Support extends StatefulWidget {
  @override
  _SupportState createState() => _SupportState();
}

class _SupportState extends State<Support> {
  List<MyList> myList;

  MyList mylist;

  DbHelper dbHelper = new DbHelper();

  int count = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xffeeeeee),
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                SizedBox(
                  height: 61.0,
                ),
                Text(
                  "پیام به پشتیبانی",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xffc80058),
                    fontSize: 25.0,
                    fontFamily: 'iranSansMobile',
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                Container(
                  width: width / 1.2,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                      fontSize: 17,
                    ),
                    onChanged: (value) {
                      title = value;
                    },
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "نام",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontFamily: 'iranSansMobile',
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  width: width / 1.2,
                  height: 60,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    textDirection: TextDirection.rtl,
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                      fontSize: 17,
                    ),
                    onChanged: (value) {
                      phonenumber = value;
                    },
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "شماره همراه",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontSize: 17,
                        fontFamily: 'iranSansMobile',
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  alignment: Alignment.center,
                  width: width / 1.2,
                  // height: 232,
                  decoration: BoxDecoration(
                    color: Color(0xfffafafa),
                    borderRadius: BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 16.0,
                        color: Color(0xffd1d1d1),
                        offset: Offset(0.0, 3.0),
                      ),
                    ],
                  ),
                  child: TextField(
                    textDirection: TextDirection.rtl,
                    maxLines: 35,
                    minLines: 12,
                    style: TextStyle(
                      fontFamily: 'iranSansMobile',
                      fontSize: 17,
                    ),
                    onChanged: (value) {
                      content = value;
                    },
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(24.0, 15.0, 24.0, 0.0),
                      border: InputBorder.none,
                      hintText: "متن پیام",
                      hintStyle: TextStyle(
                        color: Color(0xff8d8d8d),
                        fontSize: 17,
                        fontFamily: 'iranSansMobile',
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    if (title == null ||
                        content == null ||
                        phonenumber == null) {
                      showErrorDialog(
                          "تمامی فیلد های بالا را کامل کنید", _scaffoldKey);
                    } else {
                      _scaffoldKey.currentState
                          .showSnackBar(loading_SnackBar());
                      createATicket(
                        title,
                        content,
                        phonenumber,
                        this.myList[0].userId == "false"
                            ? 1
                            : int.parse(this.myList[0].userId),
                      ).then((value) {
                     String   message = 'سلام\nتشکر از ارتباط شما با فروشگاه آنلاین پرده مارکت\nهمکاران ما در اسرع وقت با شما تماس خواهند گرفت';
                        
                          smsWithMassage(
                                  phonenumber, message);
                        _scaffoldKey.currentState.hideCurrentSnackBar();
                        showErrorDialog(
                            "پیام شما با موفقیت ارسال شد", _scaffoldKey);
                      }).catchError((errore) {
                        _scaffoldKey.currentState.hideCurrentSnackBar();
                        showErrorDialog(
                            " مشگلی رخ داده است لطفا اتصال اینترنت خود را چک کنید",
                            _scaffoldKey);
                      }).whenComplete(() {
                        Future.delayed(Duration(milliseconds: 1750))
                            .whenComplete(() {
                          Navigator.pop(context);
                        });
                      });
                    }

                  },
                  child: Container(
                    height: 44,
                    width: 125,
                    decoration: BoxDecoration(
                      color: Color(0xffc80058),
                      borderRadius: BorderRadius.all(
                        Radius.circular(30.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xff9b9b9b),
                          blurRadius: 16.0,
                          offset: Offset(0.0, 3.0),
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 17.0,
                        ),
                        Text(
                          "فرستادن",
                          style: TextStyle(
                              color: Color(0xfffafafa),
                              fontFamily: 'iranYekan',
                              fontSize: 16.0),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Image.asset(
                          "assets/imgs/send.png",
                          height: 22.0,
                          width: 22.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Text(""),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
        });
      });
    });
  }
}
