import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jalali_date/jalali_date.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/monyString.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/get_iformation_of_buyer/get_iformation_of_buyer.dart';
import 'package:pardemarketapp/services/Payment.dart';
import 'package:pardemarketapp/services/create_order.dart';
import 'package:pardemarketapp/services/getProducts.dart';
import 'package:pardemarketapp/services/sms_service.dart';
import 'package:url_launcher/url_launcher.dart';
import '../cart/cartBody.dart';

class PreviewFactor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Previewfactor(),
    );
  }
}

List listOfProductInLineItems = new List();

class Previewfactor extends StatefulWidget {
  @override
  _PreviewfactorState createState() => _PreviewfactorState();
}

class _PreviewfactorState extends State<Previewfactor>
    with WidgetsBindingObserver {
  DbHelper dbHelper = new DbHelper();
  List<MyList> myList;
  MyList mylist;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  List _nameOfProduct = new List();
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      if (payment_send_response != {} ) {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            duration: Duration(seconds: 10),
            content: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                    backgroundColor: Colors.red,
                  ),
                  Text(
                    "لطفا صبور باشید",
                    style: TextStyle(
                        color: Color(0xffffffff),
                        fontFamily: "iranYekan",
                        fontSize: 16),
                  ),
                ],
              ),
            ),
            backgroundColor: Color(0xffc80058),
          ),
        );

        payment_verify(payment_send_response["id"]).then((value) {
          if (verify_transaction_response["status"]
              .toString()
              .contains("100")) {
            MaterialPageRoute(
              builder: (context) =>
                  Get_Information_Of_Buyer(_nameOfProduct, ProductOfCartPage),
            );
          } else {
            Navigator.pop(context);
            dialog(
              verify_transaction_response["error_message"],
              "برگشت",
              context,
              Icon(
                Icons.close,
                size: 50.0,
                color: Colors.red,
              ),
            );
          }
        });
      }
    }
  }

  int count = 0;
  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
          mylist = this.myList[0];
        });
      });
    });
  }

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }

    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 18.0),
                  child: Text(
                    "تاریخ فاکتور \n${todayDate()}",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(fontFamily: "iranSansMobile"),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: Text(
                        "فاکتور فروش",
                        style: TextStyle(
                          fontFamily: "iranSansMobile",
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Text("پرده مارکت",
                          style: TextStyle(
                            fontFamily: "iranSansMobile",
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                    SizedBox(height: 15.0),
                    Text("pardemarket.com",
                        style: TextStyle(
                          fontFamily: "iranSansMobile",
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        )),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 18.0),
                  child: Image.asset(
                    "assets/imgs/logo.png",
                    width: 50.0,
                    height: 50.0,
                  ),
                ),
              ],
            ),

            Divider(
              height: 25.0,
              color: Colors.black,
            ),
            // Container(
            //   alignment: Alignment.centerRight,
            //   child: Text(
            //     "طرف حساب آقای/خانم : ",
            //     textDirection: TextDirection.rtl,
            //     style: TextStyle(fontFamily: "iranSansMobile"),
            //   ),
            // ),
            Expanded(
              child: ListView.builder(
                itemCount: ProductOfCartPage.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                        border: Border(
                            left: BorderSide(color: Colors.black),
                            right: BorderSide(color: Colors.black))),
                    child: Column(
                      children: <Widget>[
                        index == 0
                            ? Container(
                                decoration: BoxDecoration(
                                    border: Border(
                                        left: BorderSide(color: Colors.black),
                                        right:
                                            BorderSide(color: Colors.black))),
                                child: Column(
                                  children: <Widget>[
                                    index == 0
                                        ? Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            height: 1.0,
                                            color: Colors.black,
                                          )
                                        : SizedBox(
                                            height: 0,
                                          ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Expanded(
                                            flex: 3,
                                            child: Center(
                                              child: Text(
                                                "قیمت کل",
                                                style: TextStyle(
                                                  fontFamily: "IranSansMobile",
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Center(
                                              child: Text(
                                                "قیمت (هرواحد)",
                                                style: TextStyle(
                                                  fontFamily: "IranSansMobile",
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            child: Center(
                                              child: Text(
                                                "متراژ",
                                                style: TextStyle(
                                                    fontFamily:
                                                        "IranSansMobile"),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 4,
                                            child: Center(
                                              child: Text(
                                                "نام محصول",
                                                style: TextStyle(
                                                    fontFamily:
                                                        "IranSansMobile"),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      color: Colors.black,
                                      height: 1.0,
                                    ),
                                  ],
                                ),
                              )
                            : SizedBox(
                                height: 0.0,
                              ),
                        index == 0
                            ? Container(
                                width: MediaQuery.of(context).size.width,
                                height: 1.0,
                                color: Colors.black,
                              )
                            : SizedBox(
                                height: 0,
                              ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Center(
                                  child: Text(
                                    toMony(
                                      (showRegularPrice_int(
                                                  ProductOfCartPage, index) *
                                              totalSizeOfCartProduct(
                                                  ProductOfCartPage, index))
                                          .toStringAsFixed(0),
                                    ),
                                    style:
                                        TextStyle(fontFamily: "IranSansMobile"),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Center(
                                  child: Text(
                                    showRegularPrice(ProductOfCartPage, index)
                                        .replaceAll('  ریال', ""),
                                    style:
                                        TextStyle(fontFamily: "IranSansMobile"),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Center(
                                  child: Text(
                                    totalSizeOfCartProduct(
                                            ProductOfCartPage, index)
                                        .toStringAsFixed(2),
                                    style:
                                        TextStyle(fontFamily: "IranSansMobile"),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 4,
                                child: Center(
                                  child: Text(
                                    getCategorie(ProductOfCartPage, index)
                                            .toString() +
                                        "  " +
                                        getName(ProductOfCartPage, index),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: "IranSansMobile",
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black,
                          height: 1.0,
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 5.0),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          toMony(get_offer_price_of_cart(ProductOfCartPage)
                              .toString()),
                          style: TextStyle(fontFamily: "IranSansMobile"),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "  :  جمع تخفیف ها",
                          style: TextStyle(fontFamily: "IranSansMobile"),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          toMony(
                              get_price_of_cart(ProductOfCartPage).toString()),
                          style: TextStyle(fontFamily: "IranSansMobile"),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "  :  مبلغ کل",
                          style: TextStyle(fontFamily: "IranSansMobile"),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),

            InkWell(
              onTap: () {
                dialog_for_payment();
              },
              child: Container(
                margin: EdgeInsets.only(top: 20.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Color(0xffc80058),
                  borderRadius: BorderRadius.all(Radius.circular(16.0)),
                ),
                width: 131.0,
                height: 51.0,
                child: Text(
                  "تکمیل خرید",
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'iranSansD',
                    fontSize: 16.0,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  dialog_for_payment() {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
          title: const Text(
            'آیا خرید خود را نهایی میکنید؟',
            style: TextStyle(
                fontFamily: "iranSansD", color: Colors.black, fontSize: 16.0),
          ),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: Text(
                'بلی',
                style: TextStyle(
                  color: Color(0xffc80058),
                  fontFamily: "iranSansD",
                  fontSize: 14.0,
                ),
              ),
              onPressed: () async{
                if (_counter == 0)  {
                  _counter++;
                  int total =  get_price_of_cart(ProductOfCartPage);
                  if ( total < 100 ||
                      ProductOfCartPage.isEmpty) {
                    dialog("قیمت خرید نمیتواند کمتر از 100 تومان باشد", "برگشت", context,
                        Text(""));
                  } else if (get_price_of_cart(ProductOfCartPage) > 500000000) {
                    dialog("نمی توانید تراکنش بیشتر از 50 میلیون داشت", "برگشت",
                        context, Text(""));
                  } else {
                    _nameOfProduct = [];
                    list_of_product = [];
                    for (var i = 0; i < ProductOfCartPage.length; i++) {
                      bool _adl =
                          getNoeErae(ProductOfCartPage, i).contains("عدل");
                      bool _taghe =
                          getNoeErae(ProductOfCartPage, i).contains("طاقه");
                      bool _metraj =
                          getNoeErae(ProductOfCartPage, i).contains("متراژی");
                      // print(_metraj);
                      // print(_taghe);
                      // print(_taghe ? quntityTextTaghe[i] * double.parse(getSizeTaghe(ProductOfCartPage, indexTaghe)) : 0);
                      // print( double.parse(getSizeTaghe(ProductOfCartPage, indexTaghe)) );
                      // print( quntityTextTaghe[i] );
                      // print(_adl);
                      _nameOfProduct.insert(
                          i, "${getName(ProductOfCartPage, i)}");
                      list_of_product.insert(
                        i,
                        {
                          "product_id": getId(ProductOfCartPage, i),
                          "quantity":
                              "${(_metraj ? (quntityTextMetraj[i] + (double.parse(quntityTextCenti[i]) / 100)) : 0 + (_adl ? (quntityTextAdl[i] * int.parse(getSizeAdl(ProductOfCartPage, indexAdl))) : 0) + (_taghe ? quntityTextTaghe[i] * int.parse(getTedadTaghe(ProductOfCartPage, indexTaghe)) : 0))}"
                        },
                      );
                    }
                    String nameofProduct = _nameOfProduct
                        .toString()
                        .replaceAll("[", "")
                        .replaceAll("]", "");
                    String mssageOfSMS =
                        "سلام سفارش شما ثبت شده است و هم اکنون در وضعیت در حال انجام می باشد\nایتم های سفارش : $nameofProduct\nمبلغ سفارش : ${get_price_of_cart(ProductOfCartPage)} ریال \npardemarket.com";

                    create_order_pendding(
                        "pendding",
                        "pendding",
                        "pendding",
                        this.myList[0].phoneOfUser,
                        this.myList[0].boolCustomer == "true"
                            ? this.myList[0].userId
                            : "1",
                        list_of_product);
                    Navigator.pop(context);
                    _scaffoldKey.currentState.showSnackBar(
                      SnackBar(
                        behavior: SnackBarBehavior.floating,
                        duration: Duration(seconds: 10),
                        content: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Colors.white),
                                backgroundColor: Colors.red,
                              ),
                              Text(
                                "لطفا صبور باشید",
                                style: TextStyle(
                                    color: Color(0xffffffff),
                                    fontFamily: "iranYekan",
                                    fontSize: 16),
                              ),
                            ],
                          ),
                        ),
                        backgroundColor: Color(0xffc80058),
                      ),
                    );

                    payment_send(get_price_of_cart(ProductOfCartPage))
                        .whenComplete(
                      () {
                        if (payment_send_response["link"]
                            .toString()
                            .contains("https")) {
                          smsWithMassage(
                                  this.myList[0].phoneOfUser, mssageOfSMS)
                              .whenComplete(() {
                            _counter = 0;
                          });
                          _launchURL(payment_send_response["link"].toString());
                        } else {
                          dialog(
                            "مشگلی رخ داده است دوباره امتحان کنید",
                            "برگشت",
                            context,
                            Icon(
                              Icons.close,
                              size: 50.0,
                              color: Color(0xffc80058),
                            ),
                          );
                        }
                      },
                    );
                  }
                }
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: const Text(
              'انصراف',
              style: TextStyle(
                  fontFamily: "iranSansD", color: Colors.black, fontSize: 16.0),
            ),
            isDefaultAction: true,
            onPressed: () {
              Future.delayed(
                Duration(seconds: 1),
              ).whenComplete(() {
                Navigator.pop(context);
              });
            },
          )),
    );
  }

  int _counter = 0;
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

todayDate() {
  String date = DateTime.now().toString();
  int _year = int.parse(date.substring(0, 4));
  int _month = int.parse(date.substring(5, 7));
  int _day = int.parse(date.substring(8, 10));

  return PersianDate.fromDateTime(DateTime(_year, _month, _day)).toString();
}
