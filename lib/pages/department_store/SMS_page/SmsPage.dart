import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/function/snackBar.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/BottomNavBar.dart';
import 'package:pardemarketapp/pages/department_store/MyStore/myStore.dart';
import 'package:pardemarketapp/pages/department_store/formOfReciver/formOfResiver.dart'
    as prefix0;
import 'package:pardemarketapp/pages/department_store/support/support.dart'
    as prefix1;
import 'package:pardemarketapp/pages/department_store/term/term.dart';
import 'package:pardemarketapp/pages/login/loginPage.dart';
import 'package:pardemarketapp/services/get_Token_forUser.dart';
import 'package:pardemarketapp/services/login_services.dart';
import 'package:pardemarketapp/services/sms_service.dart';
import 'package:quiver/async.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Color RedColor = Color(0xffc80058);
String registerPhoneNumber;
bool DoneSecond = true;
bool DoneSMS = false;
int _start = 45;
int _current = 60;
String phoneNumber;

final GlobalKey<ScaffoldState> _scaffoldKeySMS = new GlobalKey<ScaffoldState>();

class SmsPage extends StatefulWidget {
  @override
  _SmsPageState createState() => _SmsPageState();
}

class _SmsPageState extends State<SmsPage> with TickerProviderStateMixin {
  DbHelper dbHelper = new DbHelper();

  List<MyList> myList;

  MyList mylist;

  int count = 0;

  int _countForFirstClick = 0;
  // String phoneNumber;

  void _showToast() {
    _scaffoldKeySMS.currentState.showSnackBar(new SnackBar(
      content: Text(
        "کد وارد شده صحیح نمی باشد.",
        style: TextStyle(
          fontFamily: "IransansLight",
          fontSize: 18,
          color: Color(0xffffffff),
        ),
        textDirection: TextDirection.rtl,
      ),
      backgroundColor: Color(0xffc80058),
      duration: Duration(seconds: 10),
    ));
  }

  void startTimer() {
    CountdownTimer countdownTimer = new CountdownTimer(
      new Duration(seconds: _start),
      new Duration(seconds: 1),
    );
    var sub = countdownTimer.listen(null);
    sub.onData((duration) {
      setState(() {
        _current = _start - duration.elapsed.inSeconds;
      });
    });
    sub.onDone(() {
      DoneSecond = false;
      sub.cancel();
    });
  }

  @override
  void initState() {
    super.initState();
    getData();

    startTimer();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }

    return Scaffold(
      key: _scaffoldKeySMS,
      backgroundColor: Color(0xffF5F5F5),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: Padding(
          padding: const EdgeInsets.only(top: 25.0),
          child: Row(
            children: <Widget>[
              IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    size: 24,
                  ),
                  onPressed: null),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: height * 0.20 + 7,
                padding: EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "پرده مارکت",
                      style: TextStyle(
                          fontFamily: 'sogand', fontSize: 40, color: RedColor),
                    ),
                    Text(
                      "!کد فعالسازی به شماره " + phone_Number + " ارسال شد",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'IransansLight',
                          fontSize: 14,
                          color: RedColor),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  textDirection: TextDirection.rtl,
                  children: <Widget>[
                    Container(
                        width: 28,
                        height: 25,
                        child: Image.asset("assets/imgs/smartphone.png")),
                    Container(
                      width: 225.0,
                      height: 40.0,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        onChanged: (value) {
                          registerPhoneNumber = value;
                        },
                        style: TextStyle(fontFamily: "iranSansMobile"),
                        cursorColor: Color(0xffc80058),
                        decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(0.0, 5.0, 10.0, 15.0),
                          hintText: "کد فعالسازی",
                          hintStyle: TextStyle(
                            fontFamily: "iranSansMobile",
                            fontSize: 15,
                            color: Color(0xffbfbfbf),
                          ),
                          // border: InputBorder.none,
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xffbdbdbd),
                              width: 2,
                            ),
                          ),

                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xffc80058),
                              width: 2,
                            ),
                          ),
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
              ),
              MaterialButton(
                onPressed: () {
                  if (_countForFirstClick == 0) {
                    SMS_Check(phone_Number, registerPhoneNumber).then(
                      (onValue) {
                        if (onValue.toString() == "true") {
                          _countForFirstClick++;
                          _scaffoldKeySMS.currentState
                              .showSnackBar(loading_SnackBar());
                          Register(
                            phone_Number,
                            '@pardemarket2022',
                            registerEmail,
                          ).then((onValue) {
                            print("\n\nonvalue\n");
                            if (onValue.toString().contains("id") == true &&
                                onValue.toString().contains("username") ==
                                    true &&
                                onValue.toString().contains("locale") == true) {
                              this.mylist.boolCustomer = "true";
                              this.mylist.userId = "$id_User";

                              this.mylist.phoneOfUser = phone_Number;
                              dbHelper.update(mylist);
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => BottomNavyBarMain(),
                                ),
                              );
                            }
                          }).whenComplete(() {
                            if (forId["code"]
                                    .toString()
                                    .contains("existing_user_login") ==
                                true) {
                              Login(phone_Number, "@pardemarket2022").then(
                                (onValue) {
                                  if (onValue != null) {
                                    this.mylist.phoneOfUser = phone_Number;
                                    this.mylist.userId = "$id_User";
                                    if (onValue
                                        .toString()
                                        .contains("roles: [seller]")) {
                                      this.mylist.boolBecomeAVendor = "true";
                                      this.mylist.boolCustomer = "true";
                                      dbHelper.update(mylist);
                                      _scaffoldKeySMS.currentState
                                          .hideCurrentSnackBar();
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => MyStore(),
                                        ),
                                      );
                                    } else if (onValue
                                        .toString()
                                        .contains("roles: [customer]")) {
                                      this.mylist.boolCustomer = "true";
                                      dbHelper.update(mylist);
                                      Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => Term(),
                                        ),
                                      );
                                    } else {
                                      _showErrorDialog(
                                          "خطایی رخ داده است دوباره امتحان کنید");
                                    }
                                    dbHelper.update(mylist);
                                  } else {
                                    _showErrorDialog(
                                        "خطایی رخ داده است دوباره امتحان کنید");
                                  }
                                },
                              );
                            }
                          }).catchError((onError) {});
                        } else if (onValue.toString() == "false") {
                          _scaffoldKeySMS.currentState.hideCurrentSnackBar();
                          _showErrorDialog("کد وارد شده صحیح نمی باشد");
                        } else {
                          _showErrorDialog(
                              "خطایی رخ داده است. دوباره امتحان کنید");
                        }
                      },
                    );
                  }
                },
                child: Text(
                  "تایید",
                  style: TextStyle(
                      color: Color(0xffffffff),
                      fontFamily: "iranYekan",
                      fontSize: 17),
                ),
                color: Color(0xffc80058),
                disabledColor: Color(0xffc80058),
                height: 48.0,
                minWidth: 255.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  DoneSecond
                      ? Text(
                          "$_current",
                          style: TextStyle(
                            fontFamily: "IransansLight",
                            fontSize: 14,
                            color: Color(0xff8E8E8E),
                          ),
                        )
                      : InkWell(
                          onTap: () {
                            if (mounted) {
                              setState(() {
                                startTimer();
                                DoneSecond = true;
                                SMS_Register(phoneNumber);
                              });
                            }
                          },
                          child: Text(
                            "ارسال مجدد",
                            style: TextStyle(
                              fontFamily: "IransansLight",
                              fontSize: 14,
                              color: Color(0xffc80058),
                            ),
                          ),
                        ),
                  Text(
                    " :زمان باقی مانده تا ارسال مجدد ",
                    style: TextStyle(
                      fontFamily: "iranSansMobile",
                      fontSize: 14,
                      color: Color(0xff8E8E8E),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
          mylist = myList[0];
        });
      });
    });
  }
}

void _showErrorDialog(String str) {
  _scaffoldKeySMS.currentState.showSnackBar(
    new SnackBar(
      content: Text(
        str,
        style: TextStyle(
          fontFamily: "IransansLight",
          fontSize: 18,
          color: Color(0xffffffff),
        ),
        textDirection: TextDirection.rtl,
      ),
      backgroundColor: Color(0xffc80058),
    ),
  );
}
