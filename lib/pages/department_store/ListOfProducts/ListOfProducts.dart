import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/pages/department_store/StorePage/storePage.dart';
import 'package:pardemarketapp/pages/department_store/search/searchingDialog.dart';
import 'package:pardemarketapp/pages/department_store/singleProducts/singleProducts.dart';
import '../../../services/getProducts.dart';
import '../../department_store/URLS/urlS.dart';
import '../../../function/get_offer.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';

GlobalKey<ScaffoldState> _scaffoldKey_ListPage = new GlobalKey<ScaffoldState>();

class ListOfProducts extends StatelessWidget {

  String _url ;
  ListOfProducts(this._url);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey_ListPage,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: Color(0xffD0D0D0), width: 1.0)),
            color: Color(0xffeaeaea),
          ),
          height: 80.0,
          width: 360.0,
          padding: EdgeInsets.only(top: 35.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 20.0),
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.arrow_back_ios,
                        size: 26.0,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      'پرده مارکت',
                      style: TextStyle(
                        color: Color(0xffc80058),
                        fontFamily: 'sogand',
                        fontSize: 28.0,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      showDialog(
                        child: SearchingDialog(),
                        context: context,
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.only(right: 20.0),
                      alignment: Alignment.centerLeft,
                      child: Icon(
                        Icons.search,
                        size: 26.0,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      body: ListOfproducts(this._url),
    );
  }
}

class ListOfproducts extends StatefulWidget {
  String _url;
  ListOfproducts(this._url);
  @override
  _ListOfProductsState createState() => _ListOfProductsState();
}

class _ListOfProductsState extends State<ListOfproducts> {
  List ProductOfListPage = new List();

  ScrollController _scrollController_listPage =
      new ScrollController(initialScrollOffset: 0.0);
  int a = 0;
  @override
  void initState() {
    getProducts(widget._url).whenComplete(() {
      setState(() {
        amin = false;
      });
    });
    _scrollController_listPage.addListener(() {
      if (_scrollController_listPage.offset >=
              _scrollController_listPage.position.maxScrollExtent / 1.5 &&
          _scrollController_listPage.position.userScrollDirection.index == 2) {
        if (a == 0) {
          a = 1;
          setState(() {});
          bottom = false;
          getProducts(widget._url +
                  "&before=${get_date_created(ProductOfListPage, ProductOfListPage.length - 1)}")
              .whenComplete(() {
            bottom = true;
            a = 0;
            setState(() {});
          });
        }
      }
    });
    super.initState();
  }

  bool bottom = true;
  bool amin = true;
  @override
  Widget build(BuildContext context) {
    double widthOfScreen = MediaQuery.of(context).size.width;
    double heightOfScreen = MediaQuery.of(context).size.height;

    // return FutureBuilder(
    //   // future: getProducts(sortPrice_Url),
    //   builder: (BuildContext context, AsyncSnapshot snapshot) {
    //     if (snapshot.data == null) {
    //       return Center(
    //         child: SpinKitCubeGrid(
    //           color: Color(0xffc80058),
    //         ),
    //       );
    //     } else {
    return Column(
      children: <Widget>[
        Expanded(
          child: amin
              ? Center(
                  child: SpinKitCubeGrid(
                  color: Color(0xffc80058),
                ))
              : ListView.builder(
                  controller: _scrollController_listPage,
                  itemCount: ProductOfListPage.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        itemIndex = index;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    SingleProduct(ProductOfListPage)));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Stack(
                          alignment: Alignment.centerRight,
                          children: <Widget>[
                            Positioned(
                              left: 10.0,
                              child: Container(
                                width: widthOfScreen / 1.4,
                                height: 111.0,
                                decoration: BoxDecoration(
                                  color: Color(0xfffafafa),
                                  boxShadow: [
                                    new BoxShadow(
                                        color: Color(0xffCBCBCB), blurRadius: 6)
                                  ],
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(19.0),
                                  ),
                                ),
                                child: Stack(
                                  alignment: Alignment.centerRight,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(right: 75.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 40.0),
                                            child: Text(
                                              getName(ProductOfListPage, index) ,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontFamily: "iranSansMobile"),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 18.0),
                                            child: Text(
                                              "دسته : " +
                                                  getCategorie(
                                                      ProductOfListPage, index),
                                              textDirection: TextDirection.rtl,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontFamily: "iranSansMobile",
                                                fontSize: 13.0,
                                                color: Color(0xff787878),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      left: 10.0,
                                      bottom: 10.0,
                                      child: Text(
                                        getShowPrice(ProductOfListPage, index),
                                        // get_date_created(
                                        //     ProductOfListPage, index),
                                        textDirection: TextDirection.rtl,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          fontFamily: "iranSansMobile",
                                          color: Color(0xffc80058),
                                        ),
                                      ),
                                    ),
                                    String_to_int(getRegularPrice(
                                                ProductOfListPage, index)) >
                                            String_to_int(getPrice(
                                                ProductOfListPage, index))
                                        ? Positioned(
                                            top: 0,
                                            left: 0,
                                            child: Container(
                                              width: 40.0,
                                              height: 40.0,
                                              child: Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "%" +
                                                        offer(
                                                                String_to_int(
                                                                    getRegularPrice(
                                                                        ProductOfListPage,
                                                                        index)),
                                                                String_to_int(
                                                                    getPrice(
                                                                        ProductOfListPage,
                                                                        index)))
                                                            .toString(),
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xfffafafa),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily:
                                                            'IransansBold'),
                                                  ),
                                                ),
                                              ),
                                              decoration: BoxDecoration(
                                                  color: Color(0xffc80058),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  19.0),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  19.0))),
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                // color: Colors.red,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(19.0)),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(19.0),
                                ),
                                child: FadeInImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                    getImage(ProductOfListPage, index),
                                  ),
                                  placeholder: AssetImage(
                                      'assets/imgs/zero_to_infinity_by_volorf.gif'),
                                ),
                              ),
                              width: 131.0,
                              height: 131.0,
                            ),
                            bottom
                                ? Container()
                                : Positioned(
                                    bottom: 0.0,
                                    child: index == ProductOfListPage.length - 1
                                        ? Container(
                                            alignment: Alignment.center,
                                            width: widthOfScreen - 30,
                                            height: 50,
                                            color: Color(0xffc80058),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                CircularProgressIndicator(
                                                  backgroundColor: Colors.white,
                                                  valueColor:
                                                      AlwaysStoppedAnimation<
                                                              Color>(
                                                          Color(0xffc80058)),
                                                ),
                                                Text(
                                                  "درحال گرفتن اطلاعات",
                                                  style: TextStyle(
                                                      fontFamily: "iranSansD",
                                                      color: Colors.white),
                                                )
                                              ],
                                            ),
                                          )
                                        : Container(),
                                  ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
        ),
        // bottom
        //     ? Container()
        //     :
      ],
    );
    //     }
    //   },
    // );
  }

  Future<List> getProducts(String url) async {
    List a = new List();
    http.Response response = await http.get(
      (url),
      headers: {
        "Accept": "application/json",
      },
    );

    a = json.decode(response.body);
    for (var i = 0; i < a.length; i++) {
      ProductOfListPage.add(a[i]);
    }

    return ProductOfListPage;
  }
}
