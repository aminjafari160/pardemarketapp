import 'package:flutter/material.dart';
import 'package:pardemarketapp/function/monyString.dart';
import 'package:pardemarketapp/pages/department_store/cart/cartBody.dart';
import 'package:pardemarketapp/services/getProducts.dart';

class Purchase_Invoice extends StatefulWidget {
  @override
  _Purchase_InvoiceState createState() => _Purchase_InvoiceState();
}

class _Purchase_InvoiceState extends State<Purchase_Invoice> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: Container(
        margin: EdgeInsets.only(left: 30.0),
        height: 50.0,
        color: /* Color(0xffc80058) */ Color(0xee333333),
        child: InkWell(
          onTap: () {},
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "پرداخت آنلاین",
                style: TextStyle(
                    fontFamily: "iranSansMobile",
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              SizedBox(
                width: 10.0,
              ),
              Icon(
                Icons.payment,
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 80.0),
              child: Text(
                "پرده مارکت",
                style: TextStyle(
                  fontFamily: "sogand",
                  fontSize: 30.0,
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),

            Text(
              "مرجع خرید و فروش آنلاین پرده",
              style: TextStyle(fontFamily: "IranSansMobile"),
            ),
            Container(
              padding: EdgeInsets.only(top: 35.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 4,
                    child: Center(
                      child: Text(
                        "قیمت هر محصول ",
                        style: TextStyle(
                          fontFamily: "iranYekan",
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        "تعداد",
                        style: TextStyle(
                          fontFamily: "iranYekan",
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Center(
                      child: Text(
                        "نام محصول",
                        style: TextStyle(
                          fontFamily: "iranYekan",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(bottom: 60.0),
                child: ListView.builder(
                  itemCount: ProductOfCartPage.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Center(
                                child: Text(
                                  toMony(getShowPrice(ProductOfCartPage, index)
                                      .replaceAll("ریال", "")),
                                  style: TextStyle(
                                    fontFamily: "iranSansMobile",
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: Text(
                                  "${quntityTextMetraj[index]}",
                                  style: TextStyle(
                                    fontFamily: "iranSansMobile",
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Center(
                                child: Text(
                                  "${getName(ProductOfCartPage, index)}",
                                  style: TextStyle(
                                    fontFamily: "iranSansD",
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Divider(
                          endIndent: 10.0,
                          indent: 10.0,
                          color: Colors.black,
                        ),
                        index == ProductOfCartPage.length - 1
                            ? Container(
                                padding: EdgeInsets.all(20.0),
                                alignment: Alignment.topLeft,
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "قیمت کل : ${get_price_of_cart(ProductOfCartPage).toString()}" +
                                          "  ریال  ",
                                      style: TextStyle(
                                          fontFamily: "IranSansMobile"),
                                    ),
                                  ],
                                ),
                              )
                            : SizedBox(),
                      ],
                    );
                  },
                ),
              ),
            ),
            // Expanded(
            //   child: Container(
            //     padding: EdgeInsets.all(20.0),
            //     alignment: Alignment.topLeft,
            //     child: Row(
            //       children: <Widget>[
            //         Text(
            //           "قیمت کل : ${get_price_of_cart(ProductOfCartPage).toString()}" +
            //               "  ریال  ",
            //           style: TextStyle(fontFamily: "IranSansMobile"),
            //         ),
            //       ],
            //     ),
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
