import 'dart:async';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/services/addProducts.dart';
import 'package:pardemarketapp/services/create_order.dart';
import 'package:pardemarketapp/services/getProducts.dart';
import 'package:pardemarketapp/services/get_Token.dart';
import 'package:pardemarketapp/services/get_Token_forUser.dart';
import 'package:pardemarketapp/services/sms_service.dart';
import '../BottomNavBar.dart';
import '../cart/cartBody.dart';
import 'package:flutter/material.dart';

class Get_Information_Of_Buyer extends StatefulWidget {
  List _nameOfProduct;
  List _productOfCartPage;
  Get_Information_Of_Buyer(this._nameOfProduct, this._productOfCartPage);
  // This widget is the root of your application.
  @override
  _Get_Information_Of_BuyerState createState() =>
      _Get_Information_Of_BuyerState();
}

class _Get_Information_Of_BuyerState extends State<Get_Information_Of_Buyer> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DbHelper dbHelper = new DbHelper();
  List<MyList> myList;
  MyList mylist;

  @override
  void initState() {
    super.initState();
  }

  String first_name, last_name, phone_number, address;
  int count = 0;
  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
          mylist = this.myList[0];
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }

    double withOfScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xffd5d5d5),
      body: WillPopScope(
        child: Center(
          child: Container(
            // margin: EdgeInsets.fromLTRB(20.0, 45.0, 20.0, 25.0),
            decoration: BoxDecoration(
              color: Color(0xfff5f5f5),
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
              boxShadow: [
                new BoxShadow(
                  color: Color(0xffA8A8A8),
                  blurRadius: 21.0,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            width: MediaQuery.of(context).size.width - 30,
            // height: MediaQuery.of(context).size.height - 60,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                    child: Material(
                      borderRadius: BorderRadius.all(Radius.circular(35.0)),
                      elevation: 2.0,
                      child: TextField(
                        onChanged: (value) {
                          first_name = value;
                        },
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'iranSansMobile',
                        ),
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                          border: InputBorder.none,
                          hintText: "نام تحویل گیرنده",
                          hintStyle: TextStyle(
                            fontFamily: 'iranSansMobile',
                            fontSize: 15.0,
                            color: Color(0xff999999),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                    child: Material(
                      borderRadius: BorderRadius.all(Radius.circular(35.0)),
                      elevation: 2.0,
                      child: TextField(
                        onChanged: (value) {
                          last_name = value;
                        },
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'iranSansMobile',
                        ),
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                            border: InputBorder.none,
                            hintText: "نام خانوادگی تحویل گیرنده",
                            hintStyle: TextStyle(
                              fontFamily: 'iranSansMobile',
                              color: Color(0xff999999),
                              fontSize: 15.0,
                            )),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 13.0, left: 13.0),
                    child: Material(
                      borderRadius: BorderRadius.all(Radius.circular(35.0)),
                      elevation: 2.0,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        onChanged: (value) {
                          phone_number = value;
                        },
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'iranSansMobile',
                        ),
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                            border: InputBorder.none,
                            hintText: "شماره همراه",
                            hintStyle: TextStyle(
                              fontFamily: 'iranSansMobile',
                              color: Color(0xff999999),
                              fontSize: 15.0,
                            )),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        right: 13.0, left: 13.0, bottom: 20.0),
                    child: Material(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                      elevation: 2.0,
                      child: TextField(
                        onChanged: (value) {
                          address = value;
                        },
                        maxLines: 4,
                        minLines: 4,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'iranSansMobile',
                        ),
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.fromLTRB(0.0, 10.0, 18.0, 10.0),
                            border: InputBorder.none,
                            hintText: "آدرس تحویل گیرنده",
                            hintStyle: TextStyle(
                              fontFamily: 'iranSansMobile',
                              fontSize: 15.0,
                              color: Color(0xff999999),
                            )),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (first_name == null) {
                        _showErrorDialog("نامی وارد نشده است");
                      } else if (last_name == null) {
                        _showErrorDialog("نام خانوادگی وارد نشده است");
                      } else if (phone_number == null) {
                        _showErrorDialog("شماره همراه وارد نشده است");
                      } else if (address == null) {
                        _showErrorDialog("آدرس وارد نشده است");
                      } else {
                        for (var i = 0; i < ProductOfCartPage.length; i++) {
                          // get_token_for_user(storePhone(widget._productOfCartPage, i))
                          //     .then((value) {
                          //   editProduct(
                          //       body, value, getId(widget._productOfCartPage, i));
                          // });

                          // _nameOfProduct.insert(i, "${getName(ProductOfCartPage, i)}");
                          // list_of_product.insert(
                          //   i,
                          //   {
                          //     "product_id": getId(ProductOfCartPage, i),
                          //     "quantity":
                          //         "${(_metraj ? (quntityTextMetraj[i] + (double.parse(quntityTextCenti[i]) / 100)) : 0 + (_adl ? (quntityTextAdl[i] * int.parse(getTedadAdl(ProductOfCartPage, indexAdl))) : 0) + (_taghe ? quntityTextTaghe[i] * int.parse(getTedadTaghe(ProductOfCartPage, indexTaghe)) : 0)) * 100}"
                          //   },
                          // );
                        }

                        String nameofProduct = widget._nameOfProduct
                            .toString()
                            .replaceAll("[", "")
                            .replaceAll("]", "");

                        String mssageOfSMSbuyer =
                            "سلام سفارش شما ثبت شده است و هم اکنون پرداخت شده می باشد\nآیتم های سفارش: $nameofProduct\nمبلغ سفارش : ${get_price_of_cart(ProductOfCartPage)} ریال \npardemarket.com";

                        String mssageOfSMSstore =
                            "سلام\nیک سفارش برای شما ثبت شده است\nلطفا به اپلیکیشن مراجعه کنید\nفروشگاه اینترنتی پرده مارکت\npardemarket.com";

                        List idStoreString = ["0"];
                        for (var i = 0;
                            i < widget._productOfCartPage.length;
                            i++) {
                          bool _adl =
                              getNoeErae(ProductOfCartPage, i).contains("عدل");
                          bool _taghe =
                              getNoeErae(ProductOfCartPage, i).contains("طاقه");
                          bool _metraj = getNoeErae(ProductOfCartPage, i)
                              .contains("متراژی");
                          bool _tedadi = getNoeErae(ProductOfCartPage, i)
                              .contains("تعدادی");



                          String storPhone;

                          if (!idStoreString.toString().contains(
                              idStore(widget._productOfCartPage, i))) {
                            
                            idStoreString.insert(
                                0, idStore(widget._productOfCartPage, i));
                            storPhone =
                                storePhone(widget._productOfCartPage, i);
                            smsWithMassage(storPhone, mssageOfSMSstore);
                          } else {
                            idStoreString.insert(
                                0, idStore(widget._productOfCartPage, i));
                          }

                          String reg = (double.parse(
                                    replaceEnglishNumber(
                                        getRegularPrice(ProductOfCartPage, i)
                                            .replaceAll("ریال", "")),
                                  ) /
                                  100)
                              .toString();

                          String sal = getPrice(ProductOfCartPage, i) == null
                              ? (double.parse(replaceEnglishNumber(
                                          getRegularPrice(ProductOfCartPage, i)
                                              .replaceAll("ریال", ""))) /
                                      100)
                                  .toString()
                              : (double.parse(replaceEnglishNumber(
                                          getPrice(ProductOfCartPage, i)
                                              .replaceAll("ریال", ""))) /
                                      100)
                                  .toString();

                          Map body = {
                            // "name": getName(ProductOfCartPage, i),
                            // "regular_price": reg,
                            // "sale_price": sal,
                            // "description":
                            //     getdesdescription(ProductOfCartPage, i),
                            // "categories": [
                            //   {
                            //     "id": "${getIdCategorie(ProductOfCartPage, i)}",
                            //   },
                            //   {
                            //     "id":
                            //         "${getIdSubCategorie(ProductOfCartPage, i)}",
                            //   }
                            // ],
                            // "status": "publish",
                            // // srcOfImage == null ? "" : "images": [
                            // //   {
                            // //     "position":0,
                            // //     "src": srcOfImage,
                            // //   }
                            // // ],
                            "attributes": [
                              {
                                "id": 3,
                                "name": "نوع ارائه",
                                "position": 0,
                                "visible": true,
                                "variation": false,
                                "options": "${getNoeErae(ProductOfCartPage, i)}"
                              },
                              _adl
                                  ? {
                                      "id": 9,
                                      "name": "اندازه عدل",
                                      "position": 0,
                                      "visible": true,
                                      "variation": false,
                                      "options":
                                          getSizeAdl(ProductOfCartPage, i)
                                    }
                                  : {},
                              _adl
                                  ? {
                                      "id": 10,
                                      "name": "تعداد عدل",
                                      "position": 0,
                                      "visible": true,
                                      "variation": false,
                                      "options": (int.parse(getTedadAdl(ProductOfCartPage, i)) - quntityTextAdl[i]).toString()
                                    }
                                  : {},
                              _taghe
                                  ? {
                                      "id": 6,
                                      "name": "اندازه طاقه",
                                      "position": 0,
                                      "visible": true,
                                      "variation": false,
                                      "options":
                                          getSizeTaghe(ProductOfCartPage, i)
                                    }
                                  : {},
                              _taghe
                                  ? {
                                      "id": 12,
                                      "name": "تعداد طاقه",
                                      "position": 0,
                                      "visible": true,
                                      "variation": false,
                                      "options": (int.parse(getTedadTaghe(ProductOfCartPage, i)) - quntityTextTaghe[i]).toString()
                                    }
                                  : {},
                              _metraj
                                  ? {
                                      "id": 13,
                                      "name": "معیار متراژ",
                                      "position": 0,
                                      "visible": true,
                                      "variation": false,
                                      "options": getSizeMetraj(ProductOfCartPage, i)
                                    }
                                  : {},
                              _metraj
                                  ? {
                                      "id": 8,
                                      "name": "اندازه متراژی",
                                      "position": 0,
                                      "visible": true,
                                      "variation": false,
                                      "options":
                                      (int.parse(getTedadMetraj(ProductOfCartPage, i)) - quntityTextMetraj[i]).toString()
                                          
                                    }
                                  : {},
                              _tedadi
                                  ? {
                                      "id": 15,
                                      "name": "تعداد تعدادی",
                                      "position": 0,
                                      "visible": true,
                                      "variation": false,
                                      "options": (int.parse(getTedadTedadi(ProductOfCartPage, i)) - quntityTextTedadi[i]).toString()
                                    }
                                  : {},
                            ]
                          };

                            editProductOrder(body,
                                getId(widget._productOfCartPage, i));
                        }

                        Future.delayed(Duration(milliseconds: 3000))
                            .whenComplete(() {
                          smsWithMassage(
                                  this.myList[0].phoneOfUser, mssageOfSMSbuyer)
                              .whenComplete(() {
                          });
                        });

                        _scaffoldKey.currentState
                            .showSnackBar(loading_SnackBar());
                        create_order(
                                first_name,
                                last_name,
                                address,
                                phone_number,
                                this.myList[0].boolCustomer == "true"
                                    ? this.myList[0].userId
                                    : "1",
                                list_of_product,
                                idOfOrder.toString())
                            .whenComplete(() {
                          _scaffoldKey.currentState.hideCurrentSnackBar();
                          _showErrorDialog("سفارش شما با موفقیت ثبت شد");
                          Future.delayed(Duration(seconds: 2)).whenComplete(() {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => BottomNavyBarMain()));
                            ProductOfCartPage = [];
                          });
                        });
                      }
                    },
                    child: Container(
                      width: 800.0,
                      height: 60.0,
                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                        color: Color(0xffc80058),
                      ),
                      child: Center(
                        child: Text(
                          "تکمیل خرید",
                          style: TextStyle(
                            fontSize: 21.0,
                            color: Color(0xfffafafa),
                            fontFamily: "iranSansMobile",
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        onWillPop: () {},
      ),
    );
  }

  void _showErrorDialog(String str) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(
          str,
          style: TextStyle(
            fontFamily: "IransansLight",
            fontSize: 16,
            color: Color(0xffffffff),
          ),
          textDirection: TextDirection.rtl,
        ),
        backgroundColor: Color(0xffc80058),
      ),
    );
  }
}

loading_SnackBar() {
  return SnackBar(
    behavior: SnackBarBehavior.floating,
    duration: Duration(seconds: 20),
    content: Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            backgroundColor: Colors.red,
          ),
          Text(
            "لطفا صبور باشید",
            style: TextStyle(
                color: Color(0xffffffff),
                fontFamily: "iranYekan",
                fontSize: 14),
          ),
        ],
      ),
    ),
    backgroundColor: Color(0xffc80058),
  );
}

String replaceEnglishNumber(String input) {
  const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

  for (int i = 0; i < english.length; i++) {
    input = input.replaceAll(farsi[i], english[i]);
  }

  return input;
}
