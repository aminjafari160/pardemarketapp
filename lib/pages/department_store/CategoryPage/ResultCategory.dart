import 'package:flutter/material.dart';
import 'package:pardemarketapp/function/get_offer.dart';
import 'package:pardemarketapp/pages/department_store/search/searchingDialog.dart';
import 'package:pardemarketapp/pages/department_store/singleProducts/singleProducts.dart';
import 'package:pardemarketapp/services/getProducts.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'CategoryPage.dart';
import '../StorePage/storePage.dart';
import '../URLS/urlS.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';

int idCategory;

class ResultCategory extends StatefulWidget {
  @override
  _ResultCategoryState createState() => _ResultCategoryState();
}

List ProductOfResultCategoryPage = new List();

class _ResultCategoryState extends State<ResultCategory> {
  bool amin = true;
  bool bottom = true;
  int a = 0;
  ScrollController _scrollController_ResultCateguru =
      new ScrollController(initialScrollOffset: 0.0);
  @override
  void initState() {
    ProductOfResultCategoryPage.clear();
    print("\n\ninitstatte\n\n");
    getProducts(baseUrl + "&category=$idCategory").whenComplete(() {
      setState(() {
        amin = false;
      });
    });
    _scrollController_ResultCateguru.addListener(() {
      get_date_created(ProductOfResultCategoryPage,
              ProductOfResultCategoryPage.length - 1
          );
      if (_scrollController_ResultCateguru.offset >=
              _scrollController_ResultCateguru.position.maxScrollExtent / 1.5 &&
          _scrollController_ResultCateguru.position.userScrollDirection.index ==
              2) {
        if (a == 0) {
          a = 1;
          setState(() {});
          bottom = false;
          getProducts(baseUrl +
                  "&category=$idCategory" +
                  "&per_page=100" +
                  "&before=${get_date_created(ProductOfResultCategoryPage, ProductOfResultCategoryPage.length - 2)}")
              .whenComplete(() {
            bottom = true;
            a = 0;
            setState(() {});
          });
        }
      }
    });

    super.initState();
    // "${ProductOfResultCategoryPage.length.toString()}");
    // getProducts(ProductOfResultCategoryPage,  baseUrl + "&category=$idCategory");
  }

  

  Color backgroundPages = Color(0xfffafafa);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double widthOfScreen = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: backgroundPages,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(90),
          child: Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Color(0xffD0D0D0), width: 2.0)),
                color: Color(0xfffafafa),
                boxShadow: [
                  BoxShadow(
                      color: Color(0xffd4d4d4),
                      blurRadius: 10.0,
                      offset: Offset(0, 3))
                ]),
            height: 80.0,
            width: 360.0,
            padding: EdgeInsets.only(top: 35.0),
            child: Column(
              children: <Widget>[
                // SizedBox(
                //   height: 10.0,
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: EdgeInsets.only(left: 20.0),
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.arrow_back_ios,
                          size: 26.0,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        'پرده مارکت',
                        style: TextStyle(
                          color: Color(0xffc80058),
                          fontFamily: 'sogand',
                          fontSize: 28.0,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        showDialog(
                          child: SearchingDialog(),
                          context: context,
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.only(right: 20.0),
                        alignment: Alignment.centerLeft,
                        child: Icon(
                          Icons.search,
                          size: 26.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        body:
            //  FutureBuilder(

            //   future: getProducts( baseUrl + "&category=$idCategory"),
            //   // getProducts(searchUrl),
            //   builder: (BuildContext context, AsyncSnapshot snapshot) {
/////////////////////////////      DATA ==  NULL   /////////////////////////////////////////////////////

            Column(
          children: <Widget>[
            Expanded(
              child: amin 
                  ? Center(
                      child: SpinKitCubeGrid(
                        color: Color(0xffc80058),
                      ),
                    )
                  :

/////////////////////////////      DATA  ==  0   /////////////////////////////////////////////////////

                  ProductOfResultCategoryPage.length == 0
                      ? Center(
                          child: Text(
                          "${ProductOfResultCategoryPage.length}محصولی در این دسته قرار نگرفته است",
                          style: TextStyle(
                              color: Color(0xffc80058),
                              fontFamily: "iranSansMobile"),
                        ),)
                      :

/////////////////////////////      DATA  !=  NULL   /////////////////////////////////////////////////////
                      ListView.builder(
                          controller: _scrollController_ResultCateguru,
                          itemCount: ProductOfResultCategoryPage.length,
                          itemBuilder: (BuildContext context, int index) {
                            return 
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () {
                                          itemIndex = index;
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => SingleProduct(
                                                      ProductOfResultCategoryPage)));
                                        },
                                        child: Stack(
                                          alignment: Alignment.centerRight,
                                          children: <Widget>[
                                            Positioned(
                                              left: 10.0,
                                              child: Container(
                                                width: widthOfScreen / 1.4,
                                                height: 111.0,
                                                decoration: BoxDecoration(
                                                  color: Color(0xfffafafa),
                                                  boxShadow: [
                                                    new BoxShadow(
                                                        color: Color(0xffCBCBCB),
                                                        blurRadius: 6)
                                                  ],
                                                  borderRadius: BorderRadius.all(
                                                    Radius.circular(19.0),
                                                  ),
                                                ),
                                                child: Stack(
                                                  alignment: Alignment.centerRight,
                                                  children: <Widget>[
                                                    Container(
                                                      margin:
                                                          EdgeInsets.only(right: 75.0),
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment.center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment.end,
                                                        children: <Widget>[
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets.only(
                                                                    left: 40.0),
                                                            child: Text(
                                                              getName(
                                                                  ProductOfResultCategoryPage,
                                                                  index),
                                                              overflow:
                                                                  TextOverflow.ellipsis,
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'iranSansMobile'),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 10.0,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets.only(
                                                                    left: 0.0),
                                                            child: Text(
                                                              "دسته : " +
                                                                  getCategorie(
                                                                      ProductOfResultCategoryPage,
                                                                      index),
                                                              textDirection:
                                                                  TextDirection.rtl,
                                                              overflow:
                                                                  TextOverflow.ellipsis,
                                                              style: TextStyle(
                                                                  color:
                                                                      Color(0xff787878),
                                                                  fontFamily:
                                                                      "IransansLight"),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Positioned(
                                                      left: 10.0,
                                                      bottom: 10.0,
                                                      child: Text(
                                                        getShowPrice(
                                                            ProductOfResultCategoryPage,
                                                            index),
                                                        textDirection:
                                                            TextDirection.rtl,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                          color: Color(0xffc80058),
                                                          fontFamily: "IransansLight",
                                                        ),
                                                      ),
                                                    ),
                                                    String_to_int(getRegularPrice(
                                                                ProductOfResultCategoryPage,
                                                                index)) >
                                                            String_to_int(getPrice(
                                                                ProductOfResultCategoryPage,
                                                                index))
                                                        ? Positioned(
                                                            top: 0,
                                                            left: 0,
                                                            child: Container(
                                                              alignment:
                                                                  Alignment.center,
                                                              width: 39.0,
                                                              height: 39.0,
                                                              child: Center(
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .only(
                                                                          top: 0.0),
                                                                  child: Text(
                                                                    "%" +
                                                                        offer(
                                                                                String_to_int(getRegularPrice(
                                                                                    ProductOfResultCategoryPage,
                                                                                    index)),
                                                                                String_to_int(getPrice(
                                                                                    ProductOfResultCategoryPage,
                                                                                    index)))
                                                                            .toString(),
                                                                    style: TextStyle(
                                                                        color: Color(
                                                                            0xfffafafa),
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        fontSize: 15.0,
                                                                        fontFamily:
                                                                            'IransansBold'),
                                                                  ),
                                                                ),
                                                              ),
                                                              decoration: BoxDecoration(
                                                                  color:
                                                                      Color(0xffc80058),
                                                                  borderRadius:
                                                                      BorderRadius.only(
                                                                          topLeft: Radius
                                                                              .circular(
                                                                                  9.0),
                                                                          bottomRight: Radius
                                                                              .circular(
                                                                                  20.0))),
                                                            ),
                                                          )
                                                        : Container(),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                // color: Colors.red,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(19.0)),
                                              ),
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(19.0),
                                                ),
                                                child: FadeInImage(
                                                  fit: BoxFit.cover,
                                                  image: NetworkImage(
                                                    getImage(
                                                        ProductOfResultCategoryPage,
                                                        index),
                                                  ),
                                                  placeholder: AssetImage(
                                                      'assets/imgs/zero_to_infinity_by_volorf.gif'),
                                                ),
                                              ),
                                              width: 131.0,
                                              height: 131.0,
                                            ),Padding(
                                              padding: const EdgeInsets.only(top:118.0 ,left: 15.0),
                                              child: bottom
                                ? Container()
                                : Container(
                                  
                                  alignment: Alignment.bottomCenter,
                                  // bottom: 0.0,
                                    child: index == ProductOfResultCategoryPage.length - 1
                                        ? Container(
                                              alignment: Alignment.center,
                                              width: widthOfScreen - 30,
                                              height: 50,
                                              color: Color(0xffc80058),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  CircularProgressIndicator(
                                                    backgroundColor: Colors.white,
                                                    valueColor:
                                                        AlwaysStoppedAnimation<
                                                                Color>(
                                                            Color(0xffc80058)),
                                                  ),
                                                  Text(
                                                    "درحال گرفتن اطلاعات",
                                                    style: TextStyle(
                                                        fontFamily: "iranSansD",
                                                        color: Colors.white),
                                                  )
                                                ],
                                              ),
                                          )
                                        : Container(),
                                  ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                          },
                        ),
            ),
          ],
        ));
  }

  Future<List> getProducts(String url) async {
    List a = new List();
    http.Response response = await http.get(
      (url),
      headers: {
        "Accept": "application/json",
      },
    );

    a = json.decode(response.body);
    for (var i = 1; i < a.length; i++) {
      ProductOfResultCategoryPage.add(a[i]);
    }

    return ProductOfResultCategoryPage;
  }
}




