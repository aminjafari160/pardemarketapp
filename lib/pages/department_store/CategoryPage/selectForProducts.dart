import 'package:flutter/material.dart';
import 'package:pardemarketapp/pages/department_store/addProducts/addProducts.dart';

class SelectCategories extends StatefulWidget {
  @override
  _SelectCategoriesState createState() => _SelectCategoriesState();
}

Color backgroundPages = Color(0xfffafafa);

int contentCard;

int idSubCategorie;
int idCategorie;
String subCategorie;
class _SelectCategoriesState extends State<SelectCategories> {
  @override
  Widget build(BuildContext context) {


    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(

      appBar: PreferredSize(
        
        preferredSize: Size.fromHeight(height / 5),
        child: Center(
          child: Text(
            "دسته بندی کالاها",
            style: TextStyle(
                fontFamily: 'iranYekan',
                color: Color(0xffc80058),
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
      body:   ListView.builder(
        itemCount: cards.length,
        itemBuilder: (context, i) {
          return Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
            child: Container(
              width: width / 9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xffE0E0E0),
                      blurRadius: 6,
                      offset: Offset(0, 1),
                    )
                  ]),
              child: ExpansionTile(
                  onExpansionChanged: (bool) {
                    setState(() {
                      contentCard = i;
                    });
                  },
                  title: new Text(
                    cards[i].title,
                    textDirection: TextDirection.rtl,
                    style: new TextStyle(
                        fontSize: 17.0, fontFamily: 'IransansLight'),
                  ),
                  children: <Widget>[
                    new Column(
                      children: _buildExpandableContent(cards[i],context),
                    ),
                  ],
                ),
              ),
          );
        },
      ),
    );
  }
  
}


_buildExpandableContent(Cards cards,BuildContext context) {
  List<Widget> columnContent = [];
  for (String content in cards.subtitle )
    columnContent.add(
      new ListTile(
        title: Row(
          textDirection: TextDirection.ltr,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(
              Icons.arrow_forward_ios,
              textDirection: TextDirection.rtl,
              size: 15,
              color: backgroundPages,
            ),
            Text(
              content,
              textDirection: TextDirection.rtl,
              style: TextStyle(fontSize: 14.0, fontFamily: 'IransansLight',),
            ),
          ],
        ),
        onTap: () {

          contentValue = [cards.title,content];
          Navigator.pop(context);
          idSubCategorie = int.parse(cards.idsubtitle[cards.subtitle.indexOf(content)]) ;
          idCategorie = cards.idTitle;
          subCategorie = cards.subtitle[cards.subtitle.indexOf(content)];
        },
      ),
    );

  return columnContent;
}
List<String> contentValue = [];

class Cards {
  final String title;
  List<String> subtitle ;
  List<String> idsubtitle ;
  int idTitle;

  Cards(this.title, this.subtitle,this.idsubtitle,this.idTitle);
}
List<String> subTitle1 = [
  "استر",
  "حریر",
  "صدفی",
  "قابی",
  "راشل",
  "کتان",
  "ابریشم",
  
];
List<String> subTitle2 = [
  "لب حاشیه",
  "ساتن",
  "کالیفرنیا",
  "شانل",
  "گونی بافت",
  "هازان",
];
List<String> subTitle3 = [
  "ریل",
  "نوار",
  "سرب",
  "گیره",
  "هوکس",
  "پشت دری",
  "پایه",
  "گونیا",
  "گلمیخ",
  "شرابه",
  "مدال",
  "ریشه",
  "سربند",
  "قرقره اتوماتیک",
  "پیچ",
  "رول پلاک",
];

///////////////////////    ID   ///////////////////



List<String> idsubTitle1 = [
  "85",
  "86",
  "87",
  "88",
  "222",
  "223",
  "224",
];
List<String> idsubTitle2 = [
  "89",
  "90",
  "91",
  "92",
  "225",
  "237",
];
List<String> idsubTitle3 = [
  "93",
  "94",
  "96",
  "95",
  "97",
  "226",
  "227",
  "228",
  "229",
  "230",
  "231",
  "232",
  "233",
  "234",
  "235",
  "236",
];

List<int> idTitle = [
  82,
  83,
  84,
];



List<Cards> cards = [
  new Cards(
    "کار زیر",
    subTitle1,
    idsubTitle1,
    idTitle[0],
  ),
  new Cards(
    "کار رو",
    subTitle2,
    idsubTitle2,
    idTitle[1],
  ),
  new Cards(
    "لوازم",
    subTitle3,
    idsubTitle3,
    idTitle[2],
  ),
];
