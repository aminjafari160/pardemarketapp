import 'package:flutter/material.dart';

import '../../HelpPage.dart';
import 'ResultCategory.dart';

Color backgroundPages = Color(0xfffafafa);

int contentCard;
int indexExpanded = 0;

class CategoryPage extends StatefulWidget {
  int index;
  CategoryPage([this.index]);
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  void initState() {

    if (widget.index !=null) {
      indexExpanded =widget.index;
    }
    // TODO: implement initState
    print(widget.index);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(height / 7 +1.6),
        child: Center(
          child: Column(
            children: <Widget>[
              Expanded(child: SizedBox()),
              Text(
                "دسته بندی کالاها",
                style: TextStyle(
                    fontFamily: 'iranYekan',
                    color: RedColorFont,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  titleCategory(cards[0].title, 0),
                  titleCategory(cards[1].title, 1),
                  titleCategory(cards[2].title, 2),
                ],
              ),
            ],
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: cards.length,
        itemBuilder: (context, i) {
          return Column(
            children: _buildExpandableContent(cards[indexExpanded], context),
          );
        },
      ),
    );
  }

  titleCategory(title, index) {
    return InkWell(
      onTap: () {
        setState(() {
          indexExpanded = index;
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 8),
        width: MediaQuery.of(context).size.width / 3.5,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xffc80058)),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          color: index == indexExpanded ? Color(0xffc80058) : Colors.white,
        ),
        child: Text(
          title,
          style: TextStyle(
            fontFamily: "iranSansMobile",
            color: index == indexExpanded ? Colors.white : Color(0xffc80058),
          ),
        ),
      ),
    );
  }
}

_buildExpandableContent(Cards cards, BuildContext context) {
  List<Widget> columnContent = [];
  for (String content in cards.subtitle)
    columnContent.add(
      new ListTile(
        title: Row(
          textDirection: TextDirection.ltr,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Icon(
              Icons.arrow_forward_ios,
              textDirection: TextDirection.rtl,
              size: 15,
              color: backgroundPages,
            ),
            Text(
              content,
              textDirection: TextDirection.rtl,
              style: TextStyle(
                fontSize: 14.0,
                fontFamily: 'IransansLight',
              ),
            ),
          ],
        ),
        onTap: () {
//           CategorySelect(cards.title.substring(0), content);
          contentValue = [cards.title, content];
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ResultCategory()),
          );
          idCategory =
              int.parse(cards.idsubtitle[cards.subtitle.indexOf(content)]);
          print(idCategory);
        },
      ),
    );

  return columnContent;
}

List<String> contentValue = [];

class Cards {
  final String title;
  List<String> subtitle;
  List<String> idsubtitle;

  Cards(this.title, this.subtitle, this.idsubtitle);
}

List<String> subTitleUnder = [
  "استر",
  "حریر",
  "صدفی",
  "قابی",
  "راشل",
  "کتان",
  "ابریشم",
];
List<String> subTitleFront = [
  "لب حاشیه",
  "ساتن",
  "کالیفرنیا",
  "شانل",
  "گونی بافت",
  "هازان",
];
List<String> subTitleTool = [
  "ریل",
  "نوار",
  "سرب",
  "گیره",
  "هوکس",
  "پشت دری",
  "پایه",
  "گونیا",
  "گلمیخ",
  "شرابه",
  "مدال",
  "ریشه",
  "سربند",
  "قرقره اتوماتیک",
  "پیچ",
  "رول پلاک",
];

///////////////////////    ID   ///////////////////

List<String> idsubTitleUnder = [
  "85",
  "86",
  "87",
  "88",
  "222",
  "223",
  "224",
];
List<String> idsubTitleFront = [
  "89",
  "90",
  "91",
  "92",
  "225",
  "237",
];
List<String> idsubTitleTool = [
  "93",
  "94",
  "96",
  "95",
  "97",
  "226",
  "227",
  "228",
  "229",
  "230",
  "231",
  "232",
  "233",
  "234",
  "235",
  "236",
];

List<Cards> cards = [
  new Cards(
    "کار زیر",
    subTitleUnder,
    idsubTitleUnder,
  ),
  new Cards(
    "کار رو",
    subTitleFront,
    idsubTitleFront,
  ),
  new Cards(
    "لوازم",
    subTitleTool,
    idsubTitleTool,
  ),
];
