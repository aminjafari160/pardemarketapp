import 'package:flutter/material.dart';
// import 'package:intro_slider/intro_slider.dart';
// import 'package:pardemarketapp/pages/department_store/BottomNavyBar.dart';

Color RedColorFont = Color(0xffc80058);
Color darkColorFont = Color(0xff212121);
Color LightColorFont = Color(0xff424242);

// String assetWeb = 'assets/imgs/Asset 1.png';

// String assetPrice = 'assets/imgs/Asset 2.jpg';

// String assetCard = 'assets/imgs/Asset 3.png';

// class HelpPage extends StatefulWidget {
//   @override
//   _HelpPageState createState() => _HelpPageState();
// }

// class _HelpPageState extends State<HelpPage> {
//   List<Slide> slides = new List();

//   @override
//   void initState() {
//     super.initState();

//     slides.add(
//       new Slide(
//         title: "راهنما",
//         styleTitle:
//         TextStyle(color: Color(0xff212121),
//             fontSize: 24.0,
//             fontWeight: FontWeight.bold,
//             fontFamily: 'iransansD'),
//         description: "در دسته بندی های مختلف بگردید"
//             + "\n" + " و محصولات موردنظر خود را انتخاب کنید",
//         styleDescription:
//         TextStyle(
//             color: Color(0xff424242), fontSize: 16.0, fontFamily: 'iranYekan'),
//         backgroundColor: Color(0xFFf5f5f5),
//         pathImage: assetPrice,
//       ),
//     );
//     slides.add(
//       new Slide(
//         title: "راهنما",
//         styleTitle:
//         TextStyle(color: Color(0xff212121),
//             fontSize: 24.0,
//             fontWeight: FontWeight.bold,
//             fontFamily: 'iransansD'),
//         description: "آنلاین بخرید و آنلاین پرداخت کنید و در محل تحویل بگیرید",
//         styleDescription:
//         TextStyle(
//             color: Color(0xff424242), fontSize: 16.0, fontFamily: 'iranYekan'),
//         backgroundColor: Color(0xFFf5f5f5),
//         pathImage: assetWeb,
//       ),
//     );
//     slides.add(
//       new Slide(
//         title: "راهنما",
//         styleTitle:
//         TextStyle(color: Color(0xff212121),
//             fontSize: 24.0,
//             fontWeight: FontWeight.bold,
//             fontFamily: 'iransansD'),
//         description:
//         "از تخفیف های شگفت انگیز اپلیکیشن ما بهره مند شوید",
//         styleDescription:
//         TextStyle(
//             color: Color(0xff424242), fontSize: 16.0, fontFamily: 'iranYekan'),
//         backgroundColor: Color(0xFFf5f5f5),
//         pathImage: assetCard,
//       ),
//     );
//   }

//   void onDonePress() {
//     Navigator.pop(
//       context,
//       MaterialPageRoute(builder: (context) => BottomNavyBarMain()),
//     );
//   }

//   void onSkipPress() {
//     Navigator.pop(
//       context,
//       MaterialPageRoute(builder: (context) => BottomNavyBarMain()),
//     );
//   }

//   Widget renderNextBtn() {
//     return Row(
//       mainAxisSize: MainAxisSize.min,
//       children: <Widget>[
//         Text('بعدی', style: TextStyle(fontSize:16.0,fontFamily: 'iranYekan',fontWeight: FontWeight.bold,color: RedColorFont),),
//         Icon(
//           Icons.navigate_next,
//           color: RedColorFont,
//           size: 22.0,
//         ),
//       ],
//     );
//   }

//   Widget renderDoneBtn() {
//     return Text(
//       "!شروع",
//       style: TextStyle(fontSize:16.0,fontFamily: 'iranYekan',fontWeight: FontWeight.bold,color: RedColorFont),
//     );
//   }

//   Widget renderSkipBtn() {
//     return Text(
//       "رد کردن",
//       style: TextStyle(color: Color(0xff424242),
//           fontWeight: FontWeight.bold,
//           fontFamily: 'iranYekan',
//           fontSize: 16.0),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new IntroSlider(
//       // List slides
//       slides: this.slides,

//       // Skip button
//       renderSkipBtn: this.renderSkipBtn(),
//       onSkipPress: this.onSkipPress,
//       colorSkipBtn: Color(0xFFf0f0f0),
//       highlightColorSkipBtn: Color(0xff000000),

//       // Next, Done button
//       onDonePress: this.onDonePress,
//       renderNextBtn: this.renderNextBtn(),
//       renderDoneBtn: this.renderDoneBtn(),
//       colorDoneBtn: Color(0xFFf0f0f0),
//       highlightColorDoneBtn: Color(0xff000000),

//       // Dot indicator
//       colorDot: Color(0xffdddddd),
//       colorActiveDot: Color(0xffc80058),
//       sizeDot: 8.0,
//     );
//   }
// }
