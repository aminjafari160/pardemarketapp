import 'dart:math';
import 'dart:ui';
import '../../function/snackBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pardemarketapp/database/theme_model.dart';
import 'package:pardemarketapp/model/model.dart';
import 'package:pardemarketapp/pages/department_store/MyStore/myStore.dart';
import 'package:pardemarketapp/pages/department_store/SMS_page/SmsPage.dart';
import 'package:pardemarketapp/pages/department_store/UserPanelPage/UserPanelPage.dart';
import 'package:pardemarketapp/pages/department_store/become_A_Vendor/becomeAVendor.dart';
import 'package:pardemarketapp/services/get_Token_forUser.dart';
import 'package:pardemarketapp/services/sms_service.dart';
import '../../services/login_services.dart';

bool amin = true;
String phone_Number = "";
String loginPassword,
    registerPassword,
    registerUsername,
    registerEmail =
        "pardemarket${Random().nextInt(2000000).toString()}@gmail${Random().nextInt(20000).toString()}.com";

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

class LogInAndRegister extends StatefulWidget {
  @override
  _LogInAndRegisterState createState() => _LogInAndRegisterState();
}

class _LogInAndRegisterState extends State<LogInAndRegister> {
  void setState_login() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Color(0xfff5f5f5),
          resizeToAvoidBottomPadding: false,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(220),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(53.5, 0.0, 53.5, 0.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 130, 0.0, 0.0),
                    child: Text(
                      "پرده مارکت",
                      style: TextStyle(
                          color: Color(0xffc80058),
                          fontFamily: "sogand",
                          fontSize: 30.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
          body: Vorood(setState_login),
        
      ),
    );
  }
}

class Vorood extends StatefulWidget {
  final Function _setSate;
  Vorood(this._setSate);
  @override
  _VoroodState createState() => _VoroodState();
}

class _VoroodState extends State<Vorood> {
  List ProductOfLoginPage;

  DbHelper dbHelper = new DbHelper();

  List<MyList> myList;

  MyList mylist;

  int count = 0;
  @override
  void initState() {
    super.initState();

    getData();
  }

  @override
  Widget build(BuildContext context) {
    if (myList == null) {
      myList = new List<MyList>();
      getData();
    }
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(53.5, 10.0, 53.5, 0.0),
              child: Row(
                textDirection: TextDirection.rtl,
                children: <Widget>[
                  Container(
                      width: 28,
                      height: 25,
                      child: Image.asset("assets/imgs/smartphone.png")),
                  Container(
                    width: 225.0,
                    height: 40.0,
                    child: TextField(
                      autofocus: true,
                      keyboardType: TextInputType.number,
                      style: TextStyle(fontFamily: "iranSansMobile"),
                      onChanged: (value) {
                        phone_Number = value;
                      },
                      cursorColor: Color(0xffc80058),
                      decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(0.0, 5.0, 10.0, 15.0),
                        hintText: "شماره همراه",
                        hintStyle: TextStyle(
                          fontFamily: "iranSansMobile",
                          fontSize: 15,
                          color: Color(0xffbfbfbf),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xffbdbdbd),
                            width: 2,
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xffc80058),
                            width: 2,
                          ),
                        ),
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(53.5, 23.5, 53.5, 0.0),
              child: MaterialButton(
                onPressed: () {
                  if (phone_Number.length == 11) {
                    FocusScope.of(context).requestFocus(FocusNode());
                    _scaffoldKey.currentState
                        .showSnackBar((loading_SnackBar()));
                    SMS_Register(phone_Number).whenComplete(() {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SmsPage(),
                        ),
                      );
                    });
                  } else {
                    showErrorDialog(
                        "شماره همراه به درستی وارد نشده است", _scaffoldKey);
                  }
                },
                child: Text(
                  "ارسال",
                  style: TextStyle(
                      color: Color(0xffffffff),
                      fontFamily: "iranYekan",
                      fontSize: 16),
                ),
                color: Color(0xffc80058),
                disabledColor: Color(0xffc80058),
                height: 48.0,
                minWidth: 255.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              ".ما کد تایید را برای شما پیامک می کنیم",
              style: TextStyle(
                fontFamily: "IransansLight",
                fontSize: 11,
                color: Color(0xff757575),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getData() {
    var dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      var listFuture = dbHelper.getMyList();
      listFuture.then((data) {
        List<MyList> myListData = new List<MyList>();
        count = data.length;

        for (var i = 0; i < count; i++) {
          myListData.add(MyList.fromObject(data[i]));
        }

        setState(() {
          myList = myListData;
          count = count;
        });
      });
    });
  }
}

List<String> contentValue = [];

void _showErrorDialog(String str) {
  _scaffoldKey.currentState.showSnackBar(
    new SnackBar(
      content: Text(
        str,
        style: TextStyle(
          fontFamily: "IransansLight",
          fontSize: 18,
          color: Color(0xffffffff),
        ),
        textDirection: TextDirection.rtl,
      ),
      backgroundColor: Color(0xffc80058),
    ),
  );
}
