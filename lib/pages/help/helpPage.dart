import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class HelpPage extends StatefulWidget {
  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
  List img = [
    "Help-Homepage.jpg",
    "Help-Account.jpg",
    "Help-Search.jpg",
    "Help-Cart.jpg"
  ];
  int _count = 0;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Column(
            children: <Widget>[
              CarouselSlider.builder(
                enableInfiniteScroll: false,
                height: height - 80,
                itemCount: img.length,
                viewportFraction: .9,
                onPageChanged: (value) {
                  setState(() {
                    _count = value;
                  });
                  print("\n\n   $_count  \n\n");
                },
                itemBuilder: (BuildContext context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: 30.0, left: 10.0, right: 10.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                      child: Image.asset(
                        "assets/imgs/${img[index]}",
                        fit: BoxFit.fill,
                      ),
                    ),
                  );
                },
              ),
              SizedBox(height: 20.0,),
              Container(
                width: 50.0,
                height: 30.0,
                child: ListView.builder(
                  itemCount: img.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      width: 8.0,
                      height: 8.0,
                      margin:
                          EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _count == index
                              ? Color(0xffc80058)
                              : Colors.grey),
                    );
                  },
                ),
              ),
            ],
          ),
          _count == img.length - 1
              ? Positioned(
                  top: height - 66.0,
                  right: 10.0,
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: 70.0,
                      height: 35.0,
                      child: Text(
                        "متوجه شدم",
                        style:
                            TextStyle(fontFamily: "aviny", color: Colors.white),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          color: Color(0xffc80058)),
                    ),
                  ),
                )
              : SizedBox()
        ],
      ),
    );
  }
}
